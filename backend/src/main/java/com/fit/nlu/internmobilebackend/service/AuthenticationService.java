package com.fit.nlu.internmobilebackend.service;

import com.fit.nlu.internmobilebackend.database.models.User;
import com.fit.nlu.internmobilebackend.requests.LoginRequest;
import com.fit.nlu.internmobilebackend.requests.RegisterRequest;
import com.fit.nlu.internmobilebackend.response.LoginResponse;
import com.fit.nlu.internmobilebackend.response.RegisterResponse;

public interface AuthenticationService {
    User login(LoginRequest request);

    RegisterResponse register(RegisterRequest request);

    Boolean resetPassword(String email);
}
