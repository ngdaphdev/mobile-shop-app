package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.PhotoArrayURL;
import com.fit.nlu.internmobilebackend.database.models.Specifications;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpecificationsRepository extends CrudRepository<Specifications, Integer> {
    // Tìm kiếm các bản ghi trong bảng photoarrayurls với điều kiện productId = :productId
    @Query("SELECT s.title, s.value FROM Specifications s WHERE s.productId = :productId")
    List<Object[]> findByProductId(@Param("productId") Long productId);
    @Transactional
    @Modifying
    @Query("DELETE FROM Specifications s WHERE s.productId = :productId")
    void deleteByProductId(@Param("productId") Long productId);
}
