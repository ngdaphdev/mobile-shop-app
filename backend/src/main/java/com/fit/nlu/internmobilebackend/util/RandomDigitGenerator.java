package com.fit.nlu.internmobilebackend.util;

import java.util.Random;

public class RandomDigitGenerator {

    public static String generateRandomDigits(int numberOfDigits) {
        if (numberOfDigits <= 0) {
            throw new IllegalArgumentException("Number of digits must be greater than 0");
        }

        Random random = new Random();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < numberOfDigits; i++) {
            int randomDigit = random.nextInt(10); // Generates a random number between 0 (inclusive) and 10 (exclusive)
            result.append(randomDigit);
        }

        return result.toString();
    }
}
