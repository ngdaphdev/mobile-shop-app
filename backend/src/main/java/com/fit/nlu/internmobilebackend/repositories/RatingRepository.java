package com.fit.nlu.internmobilebackend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.fit.nlu.internmobilebackend.database.models.Product;
import com.fit.nlu.internmobilebackend.database.models.Rating;
import com.fit.nlu.internmobilebackend.database.models.User;

public interface RatingRepository extends CrudRepository<Rating, Long> {
	List<Rating> findByProduct(Product product);
	List<Rating> findByUser(User user);
	
	@Query(value = "SELECT p FROM Rating p WHERE p.user = :user and p.product = :product")
	List<Rating> findByUserAndProduct(@Param(value = "product")Product product,@Param("user")User user);
	

}
