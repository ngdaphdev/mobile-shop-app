package com.fit.nlu.internmobilebackend.database.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "orderDetails")
public class OrderDetailDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name ="orderId",nullable = false)
    private Long orderId;
    @Column(name ="idProduct",nullable = false)
    private Long idProduct;
    @Column(name ="quantity",nullable = false)
    private int quantity;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "idProduct", insertable = false, updatable = false, referencedColumnName = "id")
    @JsonIgnore
    private Product product;

    @Column(name = "createdDate")
    private Date createdDate;

    // Sử dụng Transient để JPA bỏ qua khi lưu vào cơ sở dữ liệu
    @Transient
    public Product getProduct() {
        return product;
    }

}
