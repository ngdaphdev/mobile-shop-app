package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.DetailImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DetailImportCouponRepository extends CrudRepository<DetailImportCoupon, Integer> {
    @Query("SELECT p FROM DetailImportCoupon d join Product p on p.id = d.productId WHERE d.importCouponId = :importCouponId")
    List<Product> findProductByCouponId(@Param("importCouponId") Long importCouponId);

    @Query("SELECT d FROM DetailImportCoupon d WHERE d.importCouponId = :importCouponId")
    List<DetailImportCoupon> findDetailByCouponId(@Param("importCouponId") Long importCouponId);
}
