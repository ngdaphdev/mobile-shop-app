package com.fit.nlu.internmobilebackend.database.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "returncoupons")
public class ReturnCoupon implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "return person")
    private String returnPerson;

    @Basic(optional = false)
    @Column(name = "total_amount")
    private Long totalAmount;

    @Basic(optional = false)
    @Column(name = "quantity")
    private Long quantity;

    @Basic(optional = false)
    @Column(name = "date_time")
    private Timestamp dateTime;

    @Basic(optional = false)
    @Column(name = "supplier_id")
    private Long supplierId;

    public ReturnCoupon() {
    }
}
