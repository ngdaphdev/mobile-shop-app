package com.fit.nlu.internmobilebackend.service;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
