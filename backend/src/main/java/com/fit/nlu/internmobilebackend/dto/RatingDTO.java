package com.fit.nlu.internmobilebackend.dto;

import jakarta.persistence.Entity;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@Data
public class RatingDTO {
    private String descreption;
    
    private float rate;
    
    
    private int status;
    
    private int product_id;
    
    private Long user_id;
}
