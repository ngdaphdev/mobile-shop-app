package com.fit.nlu.internmobilebackend.dto;

import com.fit.nlu.internmobilebackend.database.models.Product;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class OrderDetailDto {

    private Long id;
    private Long orderId;
    private Long idProduct;
    private int quantity;
    private ProductDto product;
    private Date createdDate;
}
