package com.fit.nlu.internmobilebackend.repositories;


import com.fit.nlu.internmobilebackend.database.models.PhotoArrayURL;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PhotoArrayURLRepository extends CrudRepository<PhotoArrayURL, Integer> {
    // Tìm kiếm các bản ghi trong bảng photoarrayurls với điều kiện productId = :productId
    @Query("SELECT p.url FROM PhotoArrayURL p WHERE p.productId = :productId")
    List<String> findByProductId(@Param("productId") Long productId);
}
