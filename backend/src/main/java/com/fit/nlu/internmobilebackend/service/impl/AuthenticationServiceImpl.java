package com.fit.nlu.internmobilebackend.service.impl;

import com.fit.nlu.internmobilebackend.database.models.User;
import com.fit.nlu.internmobilebackend.repositories.UserRepository;
import com.fit.nlu.internmobilebackend.requests.LoginRequest;
import com.fit.nlu.internmobilebackend.requests.RegisterRequest;
import com.fit.nlu.internmobilebackend.response.LoginResponse;
import com.fit.nlu.internmobilebackend.response.RegisterResponse;
import com.fit.nlu.internmobilebackend.service.AuthenticationService;
import com.fit.nlu.internmobilebackend.service.EmailService;
import com.fit.nlu.internmobilebackend.util.RandomDigitGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    public final static int role=1;
    public final  static int statusEnable=1;

    private final UserRepository userRepository;
    private final EmailService emailService;
    @Override
    public User login(LoginRequest request) {
        Optional<User> userOptional = userRepository.findUserByEmail(request.getEmail());
        if(!userOptional.isPresent()){
            return null;
        }

        var storedUser = userOptional.get();
        if(!storedUser.getPassword().equals(request.getPassword())){
            User userReturn = userOptional.get();
            userReturn.setPassword("");
            return userReturn;
        }
        return userOptional.get();
    }

    @Override
    public RegisterResponse register(RegisterRequest request) {

        boolean emailExist = userRepository.findUserByEmail(request.getEmail()).isPresent();
        if(emailExist){
            return RegisterResponse.of("Email exists already!");
        }
        var newUser = new User();
        newUser.setUserName(request.getEmail());
        newUser.setEmail(request.getEmail());
        newUser.setPassword(request.getPassword());
        newUser.setName(request.getName());
        newUser.setRole(role);
        newUser.setStatus(statusEnable);

        userRepository.save(newUser);

        return RegisterResponse.of("Register success!");

    }

    @Override
    public Boolean resetPassword(String email) {
        Optional<User> userOptional = userRepository.findUserByEmail(email);
        if(!userOptional.isPresent()){
            return Boolean.FALSE;
        }
        var storedUser = userRepository.findUserByEmail(email).get();
        String newPassword = RandomDigitGenerator.generateRandomDigits(8);
        storedUser.setPassword(newPassword);
        userRepository.save(storedUser);

        emailService.sendSimpleMessage(email,"New Password Instaphone",newPassword);

        return Boolean.TRUE;
    }

}
