package com.fit.nlu.internmobilebackend.database.models;

public interface OrderDetailProjection {
    Long getId();
    Long getOrderId();
    Long getIdProduct();
    Integer getQuantity();  // Sử dụng Integer thay vì int
    Product getProduct();
}


