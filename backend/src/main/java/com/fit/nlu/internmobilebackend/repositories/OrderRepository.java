package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {
//    @Query("SELECT o FROM Order o WHERE o.status = :status")
//    List<Order> findOrdersByStatus(int status);
    @Query("SELECT o FROM Order o WHERE o.status = :status ORDER BY o.id DESC")
    List<Order> findOrdersByStatusOrderByDescId(int status);

//    @Query("SELECT o FROM Order o JOIN FETCH o.orderDetails od JOIN FETCH od.product p WHERE o.status = :status ORDER BY o.id DESC")
//    List<Order> findOrdersByStatusAndFetchOrderDetailsAndProduct(int status);
}

