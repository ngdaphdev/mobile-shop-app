package com.fit.nlu.internmobilebackend.controllers.rating;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fit.nlu.internmobilebackend.database.models.Product;
import com.fit.nlu.internmobilebackend.database.models.Rating;
import com.fit.nlu.internmobilebackend.database.models.User;
import com.fit.nlu.internmobilebackend.dto.RatingDTO;
import com.fit.nlu.internmobilebackend.dto.RatingReturnDTO;
import com.fit.nlu.internmobilebackend.repositories.ProductRepository;
import com.fit.nlu.internmobilebackend.repositories.RatingRepository;
import com.fit.nlu.internmobilebackend.repositories.ResponseObject;
import com.fit.nlu.internmobilebackend.repositories.UserRepository;

@Controller // This means that this class is a Controller
@RequestMapping(path="/rating") 
public class RatingController {
	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@GetMapping("/all")
	 public @ResponseBody ResponseEntity<ResponseObject> getAllByProductId(@RequestParam(value = "productId",required = false) Long id,@RequestParam(value = "userId",required = false) Long userId) {
		
		List<RatingReturnDTO> result = new ArrayList<>();
		List<Rating> rating;
		//Tim theo productId
		if(id != null && userId ==null) {
			Product product = new Product();
			product.setId(id);
			
			rating = ratingRepository.findByProduct(product);
			RatingReturnDTO ratingReturnDTO;
			
			for (Rating item : rating) {
				ratingReturnDTO = new RatingReturnDTO(item.getDescreption(), item.getRate(), item.getStatus(), item.getProduct().getId(), item.getUser().getUserName());
				ratingReturnDTO.setId(item.getId());
				ratingReturnDTO.setCreateAt(item.getCreateAt());
				result.add(ratingReturnDTO);
			}
			
	        // This returns a JSON or XML with the users
	        return ResponseEntity.status(HttpStatus.OK).body(
	        		new ResponseObject("ok", "Successful", result)
	        		);
		}
		//Tim theo UserId
		else if(userId != null && id ==null) {
			User user = new User();
			user.setId(userId);
			
			rating = ratingRepository.findByUser(user);
			RatingReturnDTO ratingReturnDTO;
			
			for (Rating item : rating) {
				ratingReturnDTO = new RatingReturnDTO(item.getDescreption(), item.getRate(), item.getStatus(), item.getProduct().getId(), item.getUser().getUserName());
				ratingReturnDTO.setId(item.getId());
				ratingReturnDTO.setCreateAt(item.getCreateAt());
				result.add(ratingReturnDTO);
			}
			 return ResponseEntity.status(HttpStatus.OK).body(
		        		new ResponseObject("ok", "Successful", result));
		} else if(id == null && userId ==null) {
			
			return ResponseEntity.status(HttpStatus.OK).body(
	        		new ResponseObject("Failed", "Required productId or userID", ""));
			
		}
		//Tim theo userId va productId
		else {
			Product product = new Product();
			product.setId(id);
			User user = new User();
			user.setId(userId);
			
			rating = ratingRepository.findByUserAndProduct(product,user);
			RatingReturnDTO ratingReturnDTO;
			
			System.out.println("Tìm kiếm theo UserId và ProductId");
			
			for (Rating item : rating) {
				ratingReturnDTO = new RatingReturnDTO(item.getDescreption(), item.getRate(), item.getStatus(), item.getProduct().getId(), item.getUser().getUserName());
				ratingReturnDTO.setId(item.getId());
				ratingReturnDTO.setCreateAt(item.getCreateAt());
				result.add(ratingReturnDTO);
			}
			
			return ResponseEntity.status(HttpStatus.OK).body(
	        		new ResponseObject("ok", "Successful", result));
		}
    }
	
	@PostMapping("/add")
	 public @ResponseBody ResponseEntity<ResponseObject> insertRating(@RequestBody RatingDTO ratingDto){
		
		System.out.println(ratingDto.getProduct_id());
		Product product = productRepository.findById(ratingDto.getProduct_id()).get();
		System.out.println("Pass");
		User user = userRepository.findById(ratingDto.getUser_id()).get();
		
		System.out.print(product);
		Rating rating = new Rating();
		
		rating.setDescreption(ratingDto.getDescreption());
		rating.setProduct(product);
		rating.setRate(ratingDto.getRate());
		rating.setStatus(ratingDto.getStatus());
		rating.setUser(user);
		
		Rating ratingSave = ratingRepository.save(rating);
		RatingReturnDTO ratingReturnDTO = new RatingReturnDTO(ratingSave.getDescreption(), ratingSave.getRate(), ratingSave.getStatus(), ratingSave.getProduct().getId(), ratingSave.getUser().getUserName());
		ratingReturnDTO.setId(ratingSave.getId());
		ratingReturnDTO.setCreateAt(ratingSave.getCreateAt());
		return ResponseEntity.status(HttpStatus.OK).body(
    			new ResponseObject("OK", " Successful", ratingReturnDTO)
    			);	
		
	}
	
	
	//Update Rating
	//Test API url: /update/{id} + json raw data
	
	@PutMapping("/update/{id}")
	 public @ResponseBody ResponseEntity<ResponseObject> insertRating(@RequestBody RatingDTO ratingDto,@PathVariable Long id){
		Rating rating = ratingRepository.findById(id).get();
		
		
		if(rating != null) {
			Product product = productRepository.findById(ratingDto.getProduct_id()).get();
			User user = userRepository.findById(ratingDto.getUser_id()).get();
			
			rating.setDescreption(ratingDto.getDescreption());
			rating.setProduct(product);
			rating.setRate(ratingDto.getRate());
			rating.setStatus(ratingDto.getStatus());
			rating.setUser(user);
			
			rating = ratingRepository.save(rating);
			RatingReturnDTO ratingReturnDTO = new RatingReturnDTO(rating.getDescreption(), rating.getRate(), rating.getStatus(), rating.getProduct().getId(), rating.getUser().getUserName());
			ratingReturnDTO.setId(rating.getId());
			ratingReturnDTO.setCreateAt(rating.getCreateAt());
			
			return ResponseEntity.status(HttpStatus.OK).body(
	    			new ResponseObject("OK", " Successful", ratingReturnDTO)
	    			);	
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
	    			new ResponseObject("Failed", " Update Failed. NOT FOUND RATING", ""));
		}
		
		
		
		
		
		
	}
	
	//Delete Rating
	
	@DeleteMapping("/delete/{id}")
	 public @ResponseBody ResponseEntity<ResponseObject> updateRating(@PathVariable Long id){
		
		Optional<Rating> rating = ratingRepository.findById(id);
		if(rating.isPresent()) {
			ratingRepository.delete(rating.get());
			return ResponseEntity.status(HttpStatus.OK).body(
	    			new ResponseObject("OK", " Delete successfull", rating.get()));
	    				
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
	    			new ResponseObject("Failed", " Delete Failed", ""));
		}
		

		
	}
	

}
