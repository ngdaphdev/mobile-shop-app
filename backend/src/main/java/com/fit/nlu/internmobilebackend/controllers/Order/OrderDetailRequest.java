package com.fit.nlu.internmobilebackend.controllers.Order;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetailRequest {

    private Long orderId;
    private Long idProduct;
    private int quantity;



}
