package com.fit.nlu.internmobilebackend.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.fit.nlu.internmobilebackend.database.models.User;
import org.springframework.data.repository.query.Param;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Long> {
	@Query(value = "SELECT p FROM User p WHERE p.email = :email")
	Optional<User> findUserByEmail(@Param(value = "email")String email);
	//==========================

	public User findByUserName(String userName);
	public List<User> findByRole(int role);

}