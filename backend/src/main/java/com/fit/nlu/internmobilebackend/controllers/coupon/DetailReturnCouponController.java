package com.fit.nlu.internmobilebackend.controllers.coupon;

import com.fit.nlu.internmobilebackend.database.models.DetailImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.DetailReturnCoupon;
import com.fit.nlu.internmobilebackend.database.models.Product;
import com.fit.nlu.internmobilebackend.repositories.DetailImportCouponRepository;
import com.fit.nlu.internmobilebackend.repositories.DetailReturnCouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller // This means that this class is a Controller
@RequestMapping(path = "/detailreturncoupons")
public class DetailReturnCouponController {
    @Autowired // This means to get the bean called userRepository
    private DetailReturnCouponRepository detailReturnCouponRepository;

    @PostMapping(path = "/add") // Map ONLY POST Requests
    public @ResponseBody Long addDetailImportCoupon(@RequestParam Long returnCouponId, Long productId, Long returnQuantity, Long returnPrice) {
        DetailReturnCoupon detailReturnCoupon = new DetailReturnCoupon();
        detailReturnCoupon.setReturnCouponId(returnCouponId);
        detailReturnCoupon.setReturnQuantity(returnQuantity);
        detailReturnCoupon.setProductId(productId);
        detailReturnCoupon.setReturnPrice(returnPrice);
        DetailReturnCoupon savedCoupon = detailReturnCouponRepository.save(detailReturnCoupon);
        return savedCoupon.getId();
    }

    @GetMapping(path = "/getProductByCouponId") // Map ONLY POST Requests
    public @ResponseBody List<Product> getProductByCouponId(@RequestParam Long returnCouponId) {
        return detailReturnCouponRepository.findProductByCouponId(returnCouponId);
    }

    @GetMapping(path = "/getDetailByCouponId") // Map ONLY POST Requests
    public @ResponseBody List<DetailReturnCoupon> getDetailByCouponId(@RequestParam Long returnCouponId) {
        return detailReturnCouponRepository.findDetailByCouponId(returnCouponId);
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<DetailReturnCoupon> getDetailReturnCoupon() {
        return detailReturnCouponRepository.findAll();
    }
}
