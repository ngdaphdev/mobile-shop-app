package com.fit.nlu.internmobilebackend.response;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor(staticName = "of")
public class LoginResponse {
    private String message;
}
