package com.fit.nlu.internmobilebackend.database.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "detailimportcoupons")
public class DetailImportCoupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "import_coupon_id")
    private Long importCouponId;

    @Basic(optional = false)
    @Column(name = "product_id")
    private Long productId;

    @Basic(optional = false)
    @Column(name = "import_quantity")
    private Long importQuantity;

    @Basic(optional = false)
    @Column(name = "import_price")
    private Long importPrice;
    public DetailImportCoupon() {

    }
}
