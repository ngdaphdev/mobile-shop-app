package com.fit.nlu.internmobilebackend.database.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "detailreturncoupons")
public class DetailReturnCoupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "return_coupon_id")
    private Long returnCouponId;

    @Basic(optional = false)
    @Column(name = "product_id")
    private Long productId;

    @Basic(optional = false)
    @Column(name = "return_quantity")
    private Long returnQuantity;

    @Basic(optional = false)
    @Column(name = "return_price")
    private Long returnPrice;

    public DetailReturnCoupon() {

    }
}
