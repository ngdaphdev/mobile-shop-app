package com.fit.nlu.internmobilebackend.controllers.coupon;

import com.fit.nlu.internmobilebackend.database.models.DetailImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.ImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.PhotoArrayURL;
import com.fit.nlu.internmobilebackend.database.models.ReturnCoupon;
import com.fit.nlu.internmobilebackend.repositories.DetailImportCouponRepository;
import com.fit.nlu.internmobilebackend.repositories.ImportCouponRepository;
import com.fit.nlu.internmobilebackend.repositories.PhotoArrayURLRepository;
import com.fit.nlu.internmobilebackend.repositories.ReturnCouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/returncoupons")
public class ReturnCouponController {
    @Autowired // This means to get the bean called userRepository
    // Which is auto-generated by Spring, we will use it to handle the data
    private ReturnCouponRepository returnCouponRepository;

    @PostMapping(path = "/add") // Map ONLY POST Requests
    public @ResponseBody Long addImReturnCoupon(@RequestParam String returnPerson, Long totalAmount, Long quantity, Long supplierId) {
        ReturnCoupon importCoupon = new ReturnCoupon();
        importCoupon.setReturnPerson(returnPerson);
        importCoupon.setTotalAmount(totalAmount);
        importCoupon.setQuantity(quantity);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        importCoupon.setDateTime(currentTimestamp);
        importCoupon.setSupplierId(supplierId);
        ReturnCoupon savedCoupon = returnCouponRepository.save(importCoupon);
        return savedCoupon.getId();
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<ReturnCoupon> getImportCoupon() {
        return returnCouponRepository.findAll();
    }
}
