package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.ImportCoupon;
import org.springframework.data.repository.CrudRepository;

public interface ImportCouponRepository extends CrudRepository<ImportCoupon, Integer> {
}
