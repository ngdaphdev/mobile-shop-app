package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.DetailImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.DetailReturnCoupon;
import com.fit.nlu.internmobilebackend.database.models.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DetailReturnCouponRepository extends CrudRepository<DetailReturnCoupon, Integer> {
    @Query("SELECT p FROM DetailReturnCoupon d join Product p on p.id = d.productId WHERE d.returnCouponId = :returnCouponId")
    List<Product> findProductByCouponId(@Param("returnCouponId") Long returnCouponId);

    @Query("SELECT d FROM DetailReturnCoupon d WHERE d.returnCouponId = :returnCouponId")
    List<DetailReturnCoupon> findDetailByCouponId(@Param("returnCouponId") Long returnCouponId);
}
