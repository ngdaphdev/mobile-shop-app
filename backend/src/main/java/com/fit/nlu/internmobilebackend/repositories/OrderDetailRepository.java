package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.OrderDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface  OrderDetailRepository extends CrudRepository<OrderDetail, Long> {
    @Query("SELECT o,p FROM OrderDetail o JOIN Product p ON o.idProduct=p.id WHERE o.orderId = :orderId")
    List<OrderDetail> findOrderDetailByOrderId(@Param("orderId") Long orderId);
}
