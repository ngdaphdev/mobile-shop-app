package com.fit.nlu.internmobilebackend.controllers.coupon;

import com.fit.nlu.internmobilebackend.database.models.DetailImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.Product;
import com.fit.nlu.internmobilebackend.repositories.DetailImportCouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/detailimportcoupons")
public class DetailCouponController {
    @Autowired // This means to get the bean called userRepository
    private DetailImportCouponRepository detailImportCouponRepository;

    @PostMapping(path = "/add") // Map ONLY POST Requests
    public @ResponseBody Long addDetailImportCoupon(@RequestParam Long importCouponId, Long productId, Long importQuantity, Long importPrice) {
        DetailImportCoupon detailImportCoupon = new DetailImportCoupon();
        detailImportCoupon.setImportCouponId(importCouponId);
        detailImportCoupon.setImportQuantity(importQuantity);
        detailImportCoupon.setProductId(productId);
        detailImportCoupon.setImportPrice(importPrice);
        DetailImportCoupon savedCoupon = detailImportCouponRepository.save(detailImportCoupon);
        return savedCoupon.getId();
    }

    @GetMapping(path = "/getProductByCouponId") // Map ONLY POST Requests
    public @ResponseBody List<Product> getProductByCouponId(@RequestParam Long importCouponId) {
        return detailImportCouponRepository.findProductByCouponId(importCouponId);
    }

    @GetMapping(path = "/getDetailByCouponId") // Map ONLY POST Requests
    public @ResponseBody List<DetailImportCoupon> getDetailByCouponId(@RequestParam Long importCouponId) {
        return detailImportCouponRepository.findDetailByCouponId(importCouponId);
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<DetailImportCoupon> getDetailImportCoupon() {
        return detailImportCouponRepository.findAll();
    }
}
