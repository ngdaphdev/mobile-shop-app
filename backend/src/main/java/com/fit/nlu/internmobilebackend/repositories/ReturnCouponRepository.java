package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.ImportCoupon;
import com.fit.nlu.internmobilebackend.database.models.ReturnCoupon;
import org.springframework.data.repository.CrudRepository;

public interface ReturnCouponRepository extends CrudRepository<ReturnCoupon, Integer> {
}
