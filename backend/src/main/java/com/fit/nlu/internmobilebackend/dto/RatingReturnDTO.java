package com.fit.nlu.internmobilebackend.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class RatingReturnDTO {
	 	
		private Long id;
	
		private String descreption;
	    
	    private float rate;
	    
	    
	    private int status;
	    
	    private LocalDateTime createAt;
	    
	    private Long product_id;
	    
	    private String userName;
	    
	    public RatingReturnDTO() {
	    	
	    }

		public RatingReturnDTO(String descreption, float rate, int status, Long product_id, String userName) {
			super();
			this.descreption = descreption;
			this.rate = rate;
			this.status = status;
			this.product_id = product_id;
			this.userName = userName;
		}

		public String getDescreption() {
			return descreption;
		}

		public void setDescreption(String descreption) {
			this.descreption = descreption;
		}

		public float getRate() {
			return rate;
		}

		public void setRate(float rate) {
			this.rate = rate;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public Long getProduct_id() {
			return product_id;
		}

		public void setProduct_id(Long product_id) {
			this.product_id = product_id;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public LocalDateTime getCreateAt() {
			return createAt;
		}

		public void setCreateAt(LocalDateTime createAt) {
			this.createAt = createAt;
		}
		
		
	    
	    

}
