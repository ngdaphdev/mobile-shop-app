package com.fit.nlu.internmobilebackend.repositories;

import com.fit.nlu.internmobilebackend.database.models.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    @Query("SELECT p FROM Product p join Category c on c.id = p.categoryId WHERE p.name LIKE %:nameProduct% OR c.name LIKE %:nameProduct%")
    List<Product> findByNameContainingIgnoreCase(@Param("nameProduct") String nameProduct);

    @Query("SELECT p FROM Product p WHERE p.quantity > 0 ")
    List<Product> findProductCurrently();

    @Query("SELECT p FROM Product p WHERE p.categoryId = :idCategory")
    List<Product> findByCategoryNameContainingIgnoreCase(@Param("idCategory") int idCategory);
}
