package com.fit.nlu.internmobilebackend.controllers.Order;

import com.fit.nlu.internmobilebackend.database.models.Category;
import com.fit.nlu.internmobilebackend.database.models.Order;
import com.fit.nlu.internmobilebackend.repositories.OrderRepository;
import com.fit.nlu.internmobilebackend.repositories.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @PostMapping
    public Order createOrder(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    @GetMapping("/unconfirmed")
    public List<Order> getUnconfirmedOrders() {
        return orderRepository.findOrdersByStatusOrderByDescId(0);
    }

    @GetMapping("/confirmed")
    public List<Order> getConfirmedOrders() {
        return orderRepository.findOrdersByStatusOrderByDescId(1);
    }

    @GetMapping("/dagiao")
    public List<Order> getDaGiaoOrders() {
        return orderRepository.findOrdersByStatusOrderByDescId(2);
    }
    @GetMapping("/danhan")
    public List<Order> getDaNhanOrders() {
        return orderRepository.findOrdersByStatusOrderByDescId(3);
    }
    @GetMapping("/dahuy")
    public List<Order> getCancelOrders() {
        return orderRepository.findOrdersByStatusOrderByDescId(-1);
    }
    @PutMapping("/updateStatus/{id}")
    public ResponseEntity<ResponseObject> update(@RequestBody Order order, @PathVariable int id) {
        Order existingOrder = orderRepository.findById((long) id).orElse(null);

        if (existingOrder != null) {
            existingOrder.setStatus(order.getStatus());
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Update Successful", orderRepository.save(existingOrder)));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Update Error", null));
    }

}

