package com.fit.nlu.internmobilebackend.service;

import com.fit.nlu.internmobilebackend.database.models.Order;
import com.fit.nlu.internmobilebackend.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order saveOrder(Order order) {
        // Set default values before saving to the database
        order.setUserId(1L); // Default user ID
        order.setStatus(0); // Default status (waiting for confirmation)
        Order savedOrder = orderRepository.save(order);

        // Manually commit the transaction if not using @Transactional
        // entityManager.flush();
        // entityManager.clear();

        return savedOrder;
    }
}
