package com.fit.nlu.internmobilebackend.controllers;

import com.fit.nlu.internmobilebackend.database.models.Category;
import com.fit.nlu.internmobilebackend.database.models.Payment;
import com.fit.nlu.internmobilebackend.repositories.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import utils.APIName;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

import static utils.APIName.PATH_ASSETS;

@Controller
@RequestMapping(path = "/common")
public class CommonController {
    @PostMapping("/upload")
    public ResponseEntity<ResponseObject> handleFileUpload(@RequestParam("image") MultipartFile file) {

        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Please upload a file", ""));
        }

        // Ensure the upload directory exists, create it if not
        Path uploadPath = Paths.get(PATH_ASSETS + "/products/logo");
        try {
            java.nio.file.Files.createDirectories(uploadPath);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Failed to create upload directory", ""));
        }

        String fileName = file.getOriginalFilename();

        Path filePath = uploadPath.resolve(fileName);

        try {
            java.nio.file.Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Failed to upload the file", ""));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "File uploaded successfully", ""));
    }

    @PostMapping("/getSecureHash")
    public ResponseEntity<ResponseObject> getKey(@RequestBody Payment payment) {
        System.out.println(payment.getAmount());
        System.out.println(payment.getOrderInfo());
        String vnp_Version = APIName.vnp_Version;
        String vnp_Command = APIName.vnp_Command;
        String vnp_OrderInfo = payment.getOrderInfo();
        String vnp_OrderType = APIName.vnp_OrderType;
        String vnp_TxnRef = new Random().nextInt(100000)+"";
        String vnp_IpAddr = APIName.vnp_IpAddr;
        String vnp_TmnCode = APIName.vnp_TmnCode;
        String vnp_BankCode = APIName.vnp_BankCode;
        String vnp_Locale = APIName.vnp_Locale;
        String vnp_ReturnUrl = APIName.vnp_ReturnUrl;

//        int amount = Integer.parseInt(payment.getAmount()) * 100;
//        System.out.println(amount);

        Map vnp_Params = new HashMap();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", payment.getAmount());
        vnp_Params.put("vnp_BankCode", vnp_BankCode);
        if (vnp_BankCode != null && !vnp_BankCode.isEmpty()) {
            vnp_Params.put("vnp_BankCode", vnp_BankCode);
        }
        vnp_Params.put("vnp_CurrCode", "VND");
        vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
        vnp_Params.put("vnp_OrderInfo", vnp_OrderInfo);
        vnp_Params.put("vnp_OrderType", vnp_OrderType);

        if (vnp_Locale != null && !vnp_Locale.isEmpty()) {
            vnp_Params.put("vnp_Locale", vnp_Locale);
        } else {
            vnp_Params.put("vnp_Locale", "vn");
        }
        vnp_Params.put("vnp_ReturnUrl", vnp_ReturnUrl);
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);
        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(cld.getTime());

        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
        cld.add(Calendar.MINUTE, 15);
        String vnp_ExpireDate = formatter.format(cld.getTime());
        //Add Params of 2.1.0 Version
//        vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
//        //Billing
//        vnp_Params.put("vnp_Bill_Mobile", req.getParameter("txt_billing_mobile"));
//        vnp_Params.put("vnp_Bill_Email", req.getParameter("txt_billing_email"));
//        String fullName = (req.getParameter("txt_billing_fullname")).trim();
//        if (fullName != null && !fullName.isEmpty()) {
//            int idx = fullName.indexOf(' ');
//            String firstName = fullName.substring(0, idx);
//            String lastName = fullName.substring(fullName.lastIndexOf(' ') + 1);
//            vnp_Params.put("vnp_Bill_FirstName", firstName);
//            vnp_Params.put("vnp_Bill_LastName", lastName);
//
//        }
//        vnp_Params.put("vnp_Bill_Address", req.getParameter("txt_inv_addr1"));
//        vnp_Params.put("vnp_Bill_City", req.getParameter("txt_bill_city"));
//        vnp_Params.put("vnp_Bill_Country", req.getParameter("txt_bill_country"));
//        if (req.getParameter("txt_bill_state") != null && !req.getParameter("txt_bill_state").isEmpty()) {
//            vnp_Params.put("vnp_Bill_State", req.getParameter("txt_bill_state"));
//        }
        // Invoice
//        vnp_Params.put("vnp_Inv_Phone", req.getParameter("txt_inv_mobile"));
//        vnp_Params.put("vnp_Inv_Email", req.getParameter("txt_inv_email"));
//        vnp_Params.put("vnp_Inv_Customer", req.getParameter("txt_inv_customer"));
//        vnp_Params.put("vnp_Inv_Address", req.getParameter("txt_inv_addr1"));
//        vnp_Params.put("vnp_Inv_Company", req.getParameter("txt_inv_company"));
//        vnp_Params.put("vnp_Inv_Taxcode", req.getParameter("txt_inv_taxcode"));
//        vnp_Params.put("vnp_Inv_Type", req.getParameter("cbo_inv_type"));
        //Build data to hash and querystring
        List fieldNames = new ArrayList<>(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) vnp_Params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                try {
                    hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                    query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                    query.append('=');
                    query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
                //Build query
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = calculateHMAC(APIName.vnp_HashSecret, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        String paymentUrl = APIName.vnp_PayUrl + "?" + queryUrl;
        System.out.println(paymentUrl);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Get hash secure uccessfully", paymentUrl));
    }

    private static final String HMAC_SHA512 = "HmacSHA512";

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public static String calculateHMAC(String key, String data) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), HMAC_SHA512);
        Mac mac = null;
        try {
            mac = Mac.getInstance(HMAC_SHA512);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        try {
            mac.init(secretKeySpec);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
        return toHexString(mac.doFinal(data.getBytes()));
    }

    @GetMapping(path = "/message")
    public ResponseEntity<ResponseObject> findById() {
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("OK", "Successful", ""));

    }

    @PostMapping("/uploadProduct")
    public ResponseEntity<ResponseObject> handleImageProductUpload(@RequestParam("image") MultipartFile file) {
        System.out.println(file.getOriginalFilename());

        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Please upload a file", ""));
        }

        // Ensure the upload directory exists, create it if not
//        Path currentPath = Paths.get(System.getProperty("user.dir"));
//        Path uploadPath = currentPath.resolve("../frontend/assets/product");
        Path uploadPath = Paths.get(PATH_ASSETS + "/products/logo");
        try {
            java.nio.file.Files.createDirectories(uploadPath);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Failed to create upload directory", ""));
        }

        String fileName = file.getOriginalFilename();

        Path filePath = uploadPath.resolve(fileName);

        try {
            java.nio.file.Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Failed to upload the file", ""));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "File uploaded successfully", ""));
    }

}