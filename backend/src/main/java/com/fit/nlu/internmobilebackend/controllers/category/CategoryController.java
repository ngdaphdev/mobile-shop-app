package com.fit.nlu.internmobilebackend.controllers.category;

import com.fit.nlu.internmobilebackend.database.models.Category;
import com.fit.nlu.internmobilebackend.database.models.User;
import com.fit.nlu.internmobilebackend.repositories.CategoryRepository;
import com.fit.nlu.internmobilebackend.repositories.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/categories")
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepository;


    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<ResponseObject> add(@RequestBody Category category) {
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("OK", "Add Successful", categoryRepository.save(category))
        );
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ResponseObject> deleteCategory(@PathVariable int id) {
        categoryRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("OK", "Delete Successful", "")
        );
    }

    @GetMapping(path = "/get/{id}")
    public ResponseEntity<ResponseObject> findById(@PathVariable int id) {
        Optional<Category> res = categoryRepository.findById(id);

        if (res.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("OK", "Successful", res)
            );

        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("error", "Not found category with id = " + id, null)
            );
        }
    }

    @DeleteMapping("/deleteAll")
    public ResponseEntity<ResponseObject> deleteAll(@RequestBody Category category) {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Delete all data successful", ""));
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<ResponseObject> update(@RequestBody Category category, @PathVariable int id) {
        Category existingCategory = categoryRepository.findById(id).orElse(null);

        if (existingCategory != null) {
            existingCategory.setName(category.getName());
            existingCategory.setUrl(category.getUrl());
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Update Successful", categoryRepository.save(existingCategory)));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseObject("error", "Update Error", null));
    }

}