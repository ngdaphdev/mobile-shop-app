package com.fit.nlu.internmobilebackend.controllers.Order;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fit.nlu.internmobilebackend.database.models.OrderDetail;
import com.fit.nlu.internmobilebackend.database.models.OrderDetailDto;
import com.fit.nlu.internmobilebackend.repositories.OrderDetailRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/orderDetails")
public class OrderDetailController {
    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping
    @Transactional
    public OrderDetail createOrderDetail(@RequestBody OrderDetailRequest orderRequest) {
        OrderDetail orderDetailNew = objectMapper.convertValue(orderRequest, OrderDetail.class);
        // Get the current date and time using LocalDateTime
        LocalDateTime currentDateTime = LocalDateTime.now();

        // Convert LocalDateTime to Date
        Date currentDate = Date.from(currentDateTime.atZone(ZoneId.systemDefault()).toInstant());
        orderDetailNew.setCreatedDate(currentDate);
        // Sử dụng persist thay vì save
        orderDetailRepository.save(orderDetailNew);
        return orderDetailNew;
    }


    @GetMapping("/loadListOrders/{orderId}")
    public List<OrderDetail> getOrderDetailsByOrderId(@PathVariable long orderId) {
        return orderDetailRepository.findOrderDetailByOrderId(orderId);
    }

}

