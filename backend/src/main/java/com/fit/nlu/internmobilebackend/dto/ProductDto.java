package com.fit.nlu.internmobilebackend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {

    private Long id;
    private Long categoryId;
    private String name;
    private String photoURL;
    private Long importPrice;
    private Long price;
    private String description;
    private int quantity;
    private Long supplierID;
}
