package com.fit.nlu.internmobilebackend.database.models;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name ="userId",nullable = false)
    private Long userId;
    @Column(name ="buyerName",nullable = false)
    private String buyerName;
    @Column(name ="phoneNumber",nullable = false)
    private String phoneNumber;
    @Column(name ="address",nullable = false)
    private String address;
    @Column(name ="paymentMethod",nullable = false)
    private String paymentMethod;
    @Column(name ="status",nullable = false)
    private Integer status;


}
