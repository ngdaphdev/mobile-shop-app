import React, { useState } from 'react';
import AppContainer from './src/navigations/AppNavigation';
import WalkthroughScreen from "./src/screens/WalkthroughScreen/WalkthroughScreen";
import WalkthroughAppConfig from "./src/screens/WalkthroughScreen/WalkthroughAppConfig";
import DynamicAppStyles from "./src/screens/WalkthroughScreen/DynamicAppStyles";
import Toast from 'react-native-toast-message';
export default function App() {
  const [skipIntroSlider, seSkipIntroSlider] = useState(false);
  const handleSkip = () => {
    seSkipIntroSlider(true)
  }

    return (
        <>
            {skipIntroSlider ? (
                <AppContainer />
            ) : (
                <WalkthroughScreen
                    handleSkip={handleSkip}
                    appConfig={WalkthroughAppConfig}
                    appStyles={DynamicAppStyles}
                />
            )}

            {/* Tích hợp ToastMessage */}
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </>
    );

}
