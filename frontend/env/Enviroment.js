const Enviroment = {
  URL_SERVER_API: "http://192.168.40.109:8080",
  PAYMENT: {
    url: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
    vnp_Version: "2.1.0",
    vnp_Command: "pay",
    vnp_TmnCode: "ZNDB0PJT",
    vnp_Amount: 180600000,
    vnp_BankCode: "NCB",
    vnp_CreateDate: '20240108021509',
    vnp_CurrCode: "VND",
    vnp_IpAddr: "127.0.0.1",
    vnp_Locale: "vn",
    vnp_OrderInfo: "Thanh toan don hang",
    vnp_OrderType: "other",
    vnp_ReturnUrl: "http://192.168.1.19:8080",
    vnp_TxnRef: "5",
    vnp_SecureHash: "01bad6f63725986b9e086ba5be1ba26d85d7caa26f2420b91d2e50ccedd8e8e865cbe50cba5153a0fd44508a61ec335d3194cca3791ab981abb1835f8a2ecb1c",
    vnp_HashSecret: "SWLHSEIISAZKWOJMZLVIGOLZYSKMYVNJ",
  }
};

export default Enviroment;
