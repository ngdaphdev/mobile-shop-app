import axios from "axios";
import Enviroment from "../../env/Enviroment";
import { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

// export function fetchDataProductAPI() {
//   const [products, setProducts] = useState([])
//   useEffect(() => {
//     axios.get(`${Enviroment.URL_SERVER_API}/products/all`)
//         .then( (response)=> {
//           if (response.status === 200) {
//             setProducts(response.data)
//           }
//         })
//         .catch(function (error) {
//           if (error.response) {
//              console.error('Server error:', error.response.data);
//           } else if (error.request) {
//              console.error('No response from server:', error.request);
//           } else {
//              console.error('Request error:', error.message);
//           }
//         })
//         .then(function () {
//      });
//   }, [])
//     return products;
// }
// export const product = fetchDataProductAPI();

export function fetchDataPhotoArrayProductAPI(productId) {
  const [photos, setPhotos] = useState(null)
  useEffect(() => {
    axios.get(`${Enviroment.URL_SERVER_API}/photoarrayurls/getPhotoURLByProductId?productId=${productId}`)
      .then((response) => {
        if (response.status === 200) {
          setPhotos(response.data)
        }
      })
      .catch(function (error) {
        if (error.response) {
          console.error('Server error:', error.response.data);
        } else if (error.request) {
          console.error('No response from server:', error.request);
        } else {
          console.error('Request error:', error.message);
        }
      })
      .then(function () {
      });
  }, [])
  return photos;
}

// START API CATEGORY
export async function getCategories() {
  try {
    const response = await axios.get(`${Enviroment.URL_SERVER_API}/categories/all`);
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}

export async function getItemCategories(item) {
  try {
    const response = await axios.get(`${Enviroment.URL_SERVER_API}/categories/get/${item.id}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}

export async function deleteCategories(item) {
  try {
    const response = await axios.delete(`${Enviroment.URL_SERVER_API}/categories/delete/${item.id}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}

export async function createCategories(item) {
  try {
    const response = await axios.post(`${Enviroment.URL_SERVER_API}/categories/add`, {
      name: item.name,
      url: item.url,
    });
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}

export async function updateCategories(item) {
  try {
    const response = await axios.put(`${Enviroment.URL_SERVER_API}/categories/update/${item.id}`, {
      name: item.name,
      url: item.url,
    });
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}

export async function getProductByCategory(item) {
  try {
    const response = await axios.get(`${Enviroment.URL_SERVER_API}/products/category/${item.id}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}
// END API CATEGORY

// UPLOAD IMAGE TO SERVER
export async function uploadImage(formData) {
  const response = await axios.post(`${Enviroment.URL_SERVER_API}/common/upload`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  return response;
}

// payment vnpay
export async function getSecureHash(item) {
  const response = await axios.post(`${Enviroment.URL_SERVER_API}/common/getSecureHash`, item);
  return response.data;
}

// order
export async function getOrderUnconfirmed() {
  const response = await axios.get(Enviroment.URL_SERVER_API + "/api/orders/unconfirmed")
  .then(()=>{
    return response.data;
  }
    )
  .catch(error => console.error("Error fetching unconfirmed orders:", error));
  return response.data;
}
// login
export async function login(item) {
  console.log(item);
  try {
    const response = await axios.post(`${Enviroment.URL_SERVER_API}/user/login`, { 
      email:item.email, 
      password:item.password, 
    });
    return response;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}
// register
export async function registerAPI(item) {
  try {
    const response = await axios.post(`${Enviroment.URL_SERVER_API}/user/register`, { 
      email:item.email, 
      password:item.password,
      name: item.name 
    });
    return response;
  } catch (error) {
    console.error('Error fetching categories:', error.message);
    return [];
  }
}
// login data user
export async function _retrieveData () {
  const value = await AsyncStorage.getItem('user');
  console.log("test");

  console.log(value);

  if (value !== null) {
    // We have data!!
    return value
  }
};
