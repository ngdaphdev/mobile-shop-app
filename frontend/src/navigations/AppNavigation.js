import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import {createDrawerNavigator} from '@react-navigation/drawer'
import HomeScreen from '../screens/Home/HomeScreen';
import CategoriesScreen from '../screens/Categories/CategoriesScreen';
import ProductScreen from '../screens/Product/ProductScreen'
import DrawerContainer from '../screens/DrawerContainer/DrawerContainer';
import SearchScreen from '../screens/Search/SearchScreen';
import StartScreen from '../screens/Login/StartScreen';
import RegisterScreen from '../screens/Login/RegisterScreen';
import LoginScreen from '../screens/Login/LoginScreen';
import ResetPasswordScreen from '../screens/Login/ResetPasswordScreen';
import InputInfoScreen from '../screens/Checkout/InputInfoScreen';
import CartScreen from '../screens/Cart/CartScreen';
import OrderConfirmationScreen from "../screens/Oder/OrderConfirmationScreen";

// import Dashboard from '../screens/Login/Dashboard';
import DashBoard from "../screens/Dashboard/DashBoard";
import ManagerUser from "../screens/Dashboard/Manager/ManagerUser";
import UserDetail from "../screens/Dashboard/Manager/UserDetail";
import ProfileDetail from "../screens/Dashboard/Manager/ProfileDetail";
import ManagerCategory from '../screens/Dashboard/Manager/category/ManagerCategory';
import AddCategory from '../screens/Dashboard/Manager/category/AddCategory';
import UpdateCategory from '../screens/Dashboard/Manager/category/UpdateCategory';
import RatingProduct from "../screens/Rating/RatingProduct";
import AllRatingOfProduct from "../screens/Rating/AllRatingOfProduct";
import FilterProductScreen from '../screens/Home/FilterProductScreen';
import OrderHistory from '../screens/OrderHistory/OrderHistory';
import DetailOrderHistory from '../screens/OrderHistory/DetailOrderHistory';
import ScanQRCode from '../components/QRCode/ScanQRCode';
import ZaloPay from '../screens/Checkout/ZaloPay';
import OrderManagement from "../screens/Dashboard/Manager/OrderManagement";
import ManagerProduct from "../screens/Dashboard/Manager/product/ManagerProduct";
import CreateProductScreen from "../screens/Dashboard/Manager/product/CreateProduct";
import CreateImportCoupon from "../screens/Dashboard/Manager/product/CreateImportCoupon";
import DetailImportCoupon from "../screens/Dashboard/Manager/product/DetailImportCoupon";
import CreateReturnCoupon from "../screens/Dashboard/Manager/product/CreateReturnCoupon";
import DetailReturnCoupon from "../screens/Dashboard/Manager/product/DetailReturnCoupon";
import DetailProductScreen from "../screens/Dashboard/Manager/product/DetailProduct";

const Stack = createStackNavigator();

function MainNavigator() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerTitleStyle: {
                    fontWeight: "bold",
                    alignSelf: "center",
                },
            }}
        >
            <Stack.Screen name="Home" component={HomeScreen}/>
            <Stack.Screen name="Categories" component={CategoriesScreen} options={{title: "Danh mục"}}/>
            <Stack.Screen name="Cart" component={CartScreen}/>
            <Stack.Screen name="FilterProduct" component={FilterProductScreen} options={{title: "Lọc sản phẩm"}}/>
            <Stack.Screen name='Product' component={ProductScreen}/>
            <Stack.Screen name="Search" component={SearchScreen}/>
            <Stack.Screen name="InputInfoScreen" component={InputInfoScreen}/>
            <Stack.Screen name="OrderHistory" component={OrderHistory} options={{title: "Lịch sử đặt hàng"}}/>
            <Stack.Screen name="DetailOrderHistory" component={DetailOrderHistory}
                          options={{title: "Chi tiết đơn hàng"}}/>
            <Stack.Screen name="ScanQRCode" component={ScanQRCode} options={{title: "Quét mã QR"}}/>
            <Stack.Screen name="ZaloPay" component={ZaloPay} options={{title: "Cổng VNPAY"}}/>
            <Stack.Screen name="OrderConfirmationScreen" component={OrderConfirmationScreen}/>
            <Stack.Screen name='CreateImportCoupon' component={CreateImportCoupon} options={{ title: "Tạo phiếu nhập hàng" }} />
            <Stack.Screen name='DetailImportCoupon' component={DetailImportCoupon} options={{ title: "Chi tiết phiếu nhập hàng" }}/>
            <Stack.Screen name='CreateProduct' component={CreateProductScreen} options={{ title: "Tạo hàng hóa" }} />
            <Stack.Screen name='CreateReturnCoupon' component={CreateReturnCoupon} options={{ title: "Tạo phiếu trả hàng" }} />
            <Stack.Screen name='DetailReturnCoupon' component={DetailReturnCoupon} options={{ title: "Chi tiết phiếu trả hàng" }}/>
            <Stack.Screen name='DetailProductScreen' component={DetailProductScreen} options={{ title: "Chi tiết sản phẩm" }}/>
            <Stack.Screen name='ManagerProduct' component={ManagerProduct} options={{ title: "Quản lý sản phẩm" }}/>

            {/* Auth */}
            <Stack.Screen name="StartScreen" component={StartScreen}/>
            <Stack.Screen name="LoginScreen" component={LoginScreen}/>
            <Stack.Screen name="RegisterScreen" component={RegisterScreen}/>
            {/* <Stack.Screen name="Dashboard" component={Dashboard} /> */}
            <Stack.Screen
                name="ResetPasswordScreen"
                component={ResetPasswordScreen}
            />
            <Stack.Screen name="OrderManagement" component={OrderManagement}/>

            <Stack.Screen
                name="DashBoard"
                component={DashBoard}
                options={{title: "Trang Quản Lý"}}
            />
            <Stack.Screen
                name="ManagerUser"
                component={ManagerUser}
                options={{title: "Quản Lý Người Dùng"}}
            />
            <Stack.Screen
                name="UserDetail"
                component={UserDetail}
                options={{title: "Chỉnh Sửa Thông Tin"}}
            />

            <Stack.Screen
                name="ProfileDetail"
                component={ProfileDetail}
                options={{title: "Thông Tin Người Dùng"}}
            />

            <Stack.Screen
                name="ManagerCategory"
                component={ManagerCategory}
                options={{title: "Quản Lý Danh Mục"}}
            />
            <Stack.Screen
                name="AddCategory"
                component={AddCategory}
                options={{title: "Thêm Danh Mục"}}
            />
            <Stack.Screen
                name="UpdateCategory"
                component={UpdateCategory}
                options={{title: "Chỉnh Sửa Danh Mục"}}
            />

            <Stack.Screen
                name="RatingProduct"
                component={RatingProduct}
                options={{title: "Đánh giá sản phẩm"}}
            />

            <Stack.Screen
                name="AllRatingOfProduct"
                component={AllRatingOfProduct}
                options={{title: "Tất cả đánh giá"}}
            />
        </Stack.Navigator>
    );
}

const Drawer = createDrawerNavigator();

function DrawerStack() {
    return (
        <Drawer.Navigator
            drawerPosition='right'
            initialRouteName='Main'
            drawerStyle={{
                width: 250,
                height: 100
            }}
            screenOptions={{headerShown: false}}
            drawerContent={({navigation}) => (
                <DrawerContainer navigation={navigation}/>
            )}
        >
            <Drawer.Screen name="Main" component={MainNavigator}/>
        </Drawer.Navigator>
    );
}

export default function AppContainer() {
    return (
        <NavigationContainer>
            <DrawerStack/>
        </NavigationContainer>
    );
}

console.disableYellowBox = true;
