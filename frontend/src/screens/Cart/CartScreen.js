import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    Image,
    Button,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CheckBox, Card } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';
import productStyle from '../Home/ProductStyle';

const CartScreen = () => {
    const [cartItems, setCartItems] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);
    const navigation = useNavigation();

    useEffect(() => {
        const getCartItemsFromAsyncStorage = async () => {
            try {
                const storedCartItems = await AsyncStorage.getItem('cartItems');
                if (storedCartItems) {
                    setCartItems(JSON.parse(storedCartItems));
                }
            } catch (error) {
                console.error('Error getting cart items:', error);
            }
        };

        getCartItemsFromAsyncStorage();
    }, []);

    const handleClearCart = async () => {
        try {
            await AsyncStorage.removeItem('cartItems');
            setCartItems([]);
            Toast.show({
                type: 'success',
                position: 'bottom',
                text1: 'Đã xóa tất cả sản phẩm trong giỏ hàng',
                visibilityTime: 3000,
                autoHide: true,
                topOffset: 30,
            });
        } catch (error) {
            console.error('Error clearing cart:', error);
        }
    };

    const handleCheckboxToggle = (itemId) => {
        setSelectedItems((prevSelectedItems) => {
            if (prevSelectedItems.includes(itemId)) {
                return prevSelectedItems.filter((id) => id !== itemId);
            } else {
                return [...prevSelectedItems, itemId];
            }
        });
    };

    const handlePlaceOrder = () => {
        const selectedCartItems = cartItems.filter((item) =>
            selectedItems.includes(item.id)
        );

        // Calculate total cost and total quantity for selected items
        const totalCost = selectedCartItems.reduce(
            (total, item) => total + item.quantity * item.price,
            0
        );
        const totalQuantity = selectedCartItems.reduce(
            (total, item) => total + item.quantity,
            0
        );

        navigation.navigate('InputInfoScreen', {
            selectedCartItems,
            totalCost,
            totalQuantity,
        });
        const updatedCartItems = cartItems.filter(
            (item) => !selectedItems.includes(item.id)
        );
        setCartItems(updatedCartItems);
        setSelectedItems([]);
    };

    const calculateTotalCost = () => {
        return cartItems.reduce(
            (total, item) => total + item.quantity * item.price,
            0
        );
    };

    const handleIncreaseQuantity = (itemId) => {
        setCartItems((prevCartItems) => {
            return prevCartItems.map((item) => {
                if (item.id === itemId) {
                    return { ...item, quantity: item.quantity + 1 };
                }
                return item;
            });
        });
    };

    const handleDecreaseQuantity = (itemId) => {
        setCartItems((prevCartItems) => {
            return prevCartItems.map((item) => {
                if (item.id === itemId && item.quantity > 1) {
                    return { ...item, quantity: item.quantity - 1 };
                }
                return item;
            });
        });
    };

    const renderCartItem = ({ item }) => (
        <Card containerStyle={styles.cardContainer}>
            <View style={styles.cardContent}>
                <CheckBox
                    checked={selectedItems.includes(item.id)}
                    onPress={() => handleCheckboxToggle(item.id)}
                    containerStyle={styles.checkboxContainer}
                />
                <View style={styles.itemDetails}>
                    <Image
                        style={styles.itemImage}
                        source={{ uri: item.photoURL }}
                    />
                    <View style={styles.itemText}>
                        <Text style={styles.itemName} numberOfLines={2}>
                            {item.name}
                        </Text>
                        <View style={styles.quantityContainer}>
                            <Text style={styles.quantityText}>Số lượng: </Text>
                            <View style={styles.quantityButtons}>
                                <TouchableOpacity
                                    style={styles.quantityButton}
                                    onPress={() => handleDecreaseQuantity(item.id)}
                                >
                                    <Text style={styles.buttonText}>-</Text>
                                </TouchableOpacity>
                                <Text style={styles.quantityValue}>{item.quantity}</Text>
                                <TouchableOpacity
                                    style={styles.quantityButton}
                                    onPress={() => handleIncreaseQuantity(item.id)}
                                >
                                    <Text style={styles.buttonText}>+</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Text style={styles.priceText}>Giá tiền: {item.price}đ</Text>
                    </View>
                </View>
            </View>
        </Card>
    );

    return (
        <View style={styles.container}>
            {cartItems.length === 0 ? (
                <Text style={styles.emptyCartText}>Giỏ hàng trống</Text>
            ) : (
                <>
                    <FlatList
                        data={cartItems}
                        keyExtractor={(cartItem) => cartItem.id.toString()}
                        renderItem={renderCartItem}
                    />

                    {selectedItems.length > 0 && (
                        <TouchableOpacity
                            style={styles.placeOrderButton}
                            onPress={handlePlaceOrder}
                        >
                            <Text style={styles.placeOrderButtonText}>Đặt hàng</Text>
                        </TouchableOpacity>
                    )}
                </>
            )}
            {cartItems.length > 0 && (
                <TouchableOpacity
                    style={styles.clearCartButton}
                    onPress={handleClearCart}
                >
                    <Text style={styles.clearCartButtonText}> Xóa tất cả</Text>
                </TouchableOpacity>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
    },
    emptyCartText: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 20,
    },
    cardContainer: {
        borderRadius: 10,
        marginBottom: 15,
    },

    itemImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        borderRadius: 5,
        marginRight: 10,
    },

    quantityContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
    },
    quantityText: {
        fontSize: 14,
        marginRight: 5,
    },

    quantityValue: {
        fontSize: 15,
    },
    priceText: {
        fontSize: 14,
        color: 'green',
    },
    placeOrderButton: {
        backgroundColor: '#3498db',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 10,
    },
    placeOrderButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },
    clearCartButton: {
        backgroundColor: 'red',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 10,
    },
    clearCartButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },

    itemName: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5,
    },

    cardContent: {
        flexDirection: 'row',
        alignItems: 'flex-start', // Align items to the top
        padding: 10,
    },
    checkboxContainer: {
        marginRight: 0, // Adjust the margin to your preference
        marginLeft: 0, // Adjust the margin to your preference
    },
    itemDetails: {
        flexDirection: 'row',
        flex: 1,
        marginLeft: 10, // Adjust the margin to your preference
    },
    itemText: {
        flex: 1,
        marginLeft: 10, // Adjust the margin to your preference
    },

    quantityButtons: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    quantityButton: {
        backgroundColor: 'lightgrey',
        borderRadius: 15, // Make it circular
        padding: 5, // Adjust the padding to your preference
        marginHorizontal: 5, // Add spacing between buttons
    },
    buttonText: {
        fontSize: 15,
        fontWeight: 'bold',
    },
});

export default CartScreen;
