import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#f5f5f5',
    },
    cartTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    emptyCartText: {
        fontSize: 16,
        textAlign: 'center',
    },
    cardContainer: {
        marginBottom: 15,
    },
    cardContent: {
        padding: 15,
        flexDirection: 'row', marginLeft: 10, alignItems: 'center', marginBottom: 10
    },
    itemName: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    quantityText: {
        fontSize: 16,
        color: 'gray',
    },



    placeOrderButton: {
        backgroundColor: '#4CAF50',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
    },
    placeOrderButtonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    clearCartButton: {
        backgroundColor: '#f44336',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
    },
    clearCartButtonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
    },
});
export default styles;
