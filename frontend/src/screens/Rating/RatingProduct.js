import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
} from "react-native";
import { AirbnbRating } from "@rneui/themed";
import Enviroment from "../../../env/Enviroment";
import { _retrieveData } from "../../data/dataArrays";

let Rating;
export default Rating = (props) => {
  const { navigation, route } = props;
  const item = route.params?.item;
  // var item = {};
  // if (route != undefined) {
  //   item = route.params?.item;
  // }

  console.log(item);

  const [rate, setRate] = useState(5);
  const [value, setValue] = useState("");
  const [user, setUser] = useState({});
  const [res, setRes] = useState({});

  console.log("Rate:" + rate);
  const Ratings = () => {
    const ratingCompleted = (rating) => {
      console.log("Rating is: " + rating);
    };

    const ratingProps = {};
    return (
      <View style={styles.rateComponent}>
        <ScrollView style={styles.viewContainer}>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 30,
            }}
          >
            <AirbnbRating
              count={5}
              reviews={["Rất Tệ", "Tệ", "Bình Thường", "Tốt", "Tuyệt vời"]}
              defaultRating={rate}
              size={20}
              reviewSize={20}
              onFinishRating={(number) => {
                setRate(number);
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  };

  const getUser = async () => {
    const userTemp = await _retrieveData();
    const data = JSON.parse(userTemp);
    setUser(data);
  };

  useEffect(() => {
    getUser();
  }, []);

  console.log("UserId:" + user.id);
  const onClickRate = (item) => {
    const submit = async (e) => {
      console.log("Log:" + value);
      const response = await fetch(`${Enviroment.URL_SERVER_API}/rating/add`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          descreption: value,
          rate: rate,
          status: "1",
          product_id: item.id,
          user_id: user.id,
        }),
      });

      const result = await response.json();
      if (result.status === "OK") {
        const returnData = result.data;
        var ratings = {
          id: returnData.id,
          descreption: returnData.descreption,
          rate: returnData.rate,
          status: 1,
          createAt: returnData.createAt,
          product_id: returnData.product_id,
          userName: returnData.userName,
        };
        console.log(returnData);

        navigation.navigate("Product", { item, ratings });
      }
    };

    submit();
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <View>
          <Text style={styles.title}>Đánh giá sản phẩm</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            backgroundColor: "white",
            borderColor: "#000",
            borderWidth: 1,
            marginHorizontal: 10,
          }}
        >
          <View
            style={{
              flex: 2,
              alignItems: "center",
              justifyContent: "center",
              paddingBottom: 10,
              borderRightWidth: 1,
              borderRightColor: "#000",
            }}
          >
            <Image
              style={styles.productImg}
              source={{
                uri: item.photoURL,
              }}
            />
          </View>
          <View style={{ flex: 8, flexDirection: "column", paddingBottom: 10 }}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.description}>{item.price} VNĐ</Text>
          </View>
        </View>

        <View style={styles.separator}>
          <Text style={styles.textRate}>Chất lượng sản phẩm:</Text>
          <Ratings></Ratings>
        </View>

        <View style={styles.containerInput}>
          <Text style={styles.textComment}>Nhận Xét:</Text>
          <TextInput
            editable
            multiline={true}
            row={4}
            maxLength={300}
            numberOfLines={6}
            onChangeText={(text) => setValue(text)}
            value={value}
            style={styles.textInput}
            placeholder="Nhập nhận xét của bạn tại đây"
          />
        </View>
        <View style={styles.addToCarContainer}>
          <TouchableOpacity
            style={styles.shareButton}
            onPress={() => {
              onClickRate(item);
            }}
          >
            <Text style={styles.shareButtonText}>Đánh giá</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "center",
  },
  productImg: {
    marginTop: 10,
    width: "100%",
    height: "100%",
  },
  name: {
    fontSize: 18,
    color: "#696969",
    fontWeight: "bold",
    flex: 1,
    marginLeft: 20,
  },
  description: {
    marginTop: 10,
    color: "#696969",
    flex: 1,
    marginLeft: 20,
  },
  btnColor: {
    height: 30,
    width: 30,
    borderRadius: 30,
    marginHorizontal: 3,
  },
  btnSize: {
    height: 40,
    width: 40,
    borderRadius: 40,
    borderColor: "#778899",
    borderWidth: 1,
    marginHorizontal: 3,
    backgroundColor: "white",

    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  starContainer: {
    justifyContent: "center",
    marginHorizontal: 30,
    flexDirection: "row",
    marginTop: 20,
  },
  contentColors: {
    justifyContent: "center",
    marginHorizontal: 30,
    flexDirection: "row",
    marginTop: 20,
  },
  contentSize: {
    justifyContent: "center",
    marginHorizontal: 30,
    flexDirection: "row",
    marginTop: 20,
  },
  separator: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  shareButton: {
    marginTop: 10,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    backgroundColor: "#00BFFF",
  },
  shareButtonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  addToCarContainer: {
    marginHorizontal: 30,
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 20,
    textAlign: "center",
    alignItems: "center",
  },
  textRate: {
    flex: 2,
    marginBottom: 30,
    marginLeft: 15,
    fontWeight: "bold",
  },
  rateComponent: {
    marginTop: 10,
    flex: 4,
    marginRight: "20%",
  },
  containerInput: {
    flex: 3,
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 8,
    padding: 8,
    margin: 10,
  },
  textComment: {
    marginLeft: 15,
    fontWeight: "bold",
  },
});
