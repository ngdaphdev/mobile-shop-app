import { ButtonGroup, Icon } from "@rneui/themed";
import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import { useRoute, useNavigation } from "@react-navigation/native";
import { Rating } from "@kolking/react-native-rating";
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import BackButton from "../../components/BackButton/BackButton";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";

let AllRatingProdiuct;
export default AllRatingProdiuct = (props) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  //   const { navigation, route } = props;

  //   console.log("Route:" + route.params.toString());
  //   console.log("Navigation:" + navigation);
  //   //   const data = route.params.data;
  const navigation = useNavigation();
  const route = useRoute();
  // console.log("Route:" + route.params);
  // console.log(navigation);

  var data = {};
  if (route != undefined) {
    data = route.params?.data;
    data.sort((a, b) => {
      return b.id - a.id;
    });
  }

  //   useEffect(() => {
  //     setData(item);
  //   }, []);
  console.log(selectedIndex);

  function convertDate(ngay) {
    const date = new Date(ngay);
    return (
      date.getDate() +
      "/" +
      date.getMonth() +
      1 +
      "/" +
      date.getFullYear() +
      "   " +
      date.getHours() +
      ":" +
      date.getMinutes()
    );
  }
  return (
    <View style={styles.wrapContainer}>
      <ButtonGroup
        buttonStyle={{ padding: 10 }}
        selectedButtonStyle={{ backgroundColor: "#e2e2e2" }}
        selectedIndex={selectedIndex}
        onPress={(value) => {
          setSelectedIndex(value);
        }}
        buttons={[
          <Text>Tất cả</Text>,
          <Text>
            1 <FontAwesomeIcon name="star" color="#fb6d2c" />
          </Text>,
          <Text>
            2 <FontAwesomeIcon name="star" color="#fb6d2c" />
          </Text>,
          <Text>
            3 <FontAwesomeIcon name="star" color="#fb6d2c" />
          </Text>,
          <Text>
            4 <FontAwesomeIcon name="star" color="#fb6d2c" />
          </Text>,
          <Text>
            5 <FontAwesomeIcon name="star" color="#fb6d2c" />,
          </Text>,
          // <TouchableOpacity>
          //   <Text>Tất cả</Text>
          // </TouchableOpacity>,
          // <TouchableOpacity>
          //   <Text>
          //     5 <FontAwesomeIcon name="star" color="#fb6d2c" />
          //   </Text>
          // </TouchableOpacity>,
          // <TouchableOpacity>
          //   <Text>
          //     4 <FontAwesomeIcon name="star" color="#fb6d2c" />
          //   </Text>
          // </TouchableOpacity>,
          // <TouchableOpacity>
          //   <Text>
          //     3 <FontAwesomeIcon name="star" color="#fb6d2c" />
          //   </Text>
          // </TouchableOpacity>,
          // <TouchableOpacity>
          //   <Text>
          //     2 <FontAwesomeIcon name="star" color="#fb6d2c" />
          //   </Text>
          // </TouchableOpacity>,
          // <TouchableOpacity>
          //   <Text>
          //     1 <FontAwesomeIcon name="star" color="#fb6d2c" />
          //   </Text>
          // </TouchableOpacity>,
        ]}
        // selectedIndex={selectedIndex}
        // onPress={setSelectedIndex}
      />
      {/* <ScrollView> */}
      {route != undefined && (
        <View style={styles.container}>
          <FlatList
            data={
              data == {}
                ? {}
                : selectedIndex != 0
                ? data.filter((obj) => {
                    // console.log("Rate:" + obj.rate);
                    // console.log("Select:" + selectedIndex);
                    // console.log(Math.floor(obj.rate) == selectedIndex);
                    return Math.floor(obj.rate) == selectedIndex;
                  })
                : data
            }
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <View style={styles.commentContainer}>
                <View style={styles.avatarContainer}>
                  <Text style={styles.avatar}>{item.userName.slice(0, 1)}</Text>
                </View>
                <View style={styles.commentTextContainer}>
                  <Text style={styles.commentAuthor}>
                    {item.userName}
                    {"        "}
                    <Text
                      style={{
                        fontWeight: "normal",
                        marginLeft: "10%",
                        color: "#000",
                        opacity: 0.5,
                        fontSize: 12,
                      }}
                    >
                      {convertDate(item.createAt)}
                    </Text>
                  </Text>
                  <Rating
                    isDisabled={true}
                    size={10}
                    rating={item.rate}
                    style={{
                      marginLeft: "1%",
                      marginVertical: "2%",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  />
                  <View style={{ width: "100%", flexWrap: "wrap" }}>
                    <Text style={styles.commentText}>{item.descreption}</Text>
                  </View>
                </View>
              </View>
            )}
          />
        </View>
      )}
      {/* </ScrollView> */}
    </View>
  );
};
const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    backgroundColor: "white",
  },
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "white",
  },
  warpTitleComment: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  content: {
    fontSize: 16,
    lineHeight: 24,
  },
  sectionTitle: {
    fontSize: 15,
    fontWeight: "bold",
    marginBottom: 20,
  },
  commentContainer: {
    flexDirection: "row",
    marginTop: 10,
    paddingBottom: 13,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },
  commentAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  commentTextContainer: {
    marginLeft: 10,
    justifyContent: "center",
    alignItems: "baseline",
  },
  commentAuthor: {
    fontSize: 16,
    fontWeight: "bold",
  },
  commentText: {
    fontSize: 14,
    maxWidth: "90%",
    textAlign: "justify",
  },
  rateCore: {
    marginLeft: "1%",
  },
  avatar: {
    fontSize: 30,
    fontWeight: "700",
  },
  avatarContainer: {
    width: 50,
    height: 50,
    borderRadius: 70,
    backgroundColor: "#ccc",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 6,
    shadowOpacity: 0.16,
  },
});
