import React, { useState, useEffect, useRef } from "react";

import { useRoute, useNavigation } from "@react-navigation/native";
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  ScrollView,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AirbnbRating, Icon, ActivityIndicator } from "@rneui/themed";
import { Rating } from "@kolking/react-native-rating";
import Enviroment from "../../../env/Enviroment";

let ReviewComponent;
export default ReviewComponent = (props) => {
  const item = props.item;
  var rating = props.newRating;
  // const navigation = props.navigate;
  // const route = props.route;
  const navigation = useNavigation();
  const route = useRoute();
  console.log("Item:" + item.id);

  const [isLoading, setLoading] = useState(true);
  const [rate, setRate] = useState({});
  const sentMessage = useRef(0);

  // const data = [
  //   {
  //     id: 1,
  //     author: "Jane Doe",
  //     authorAvatar: "https://www.bootdey.com/img/Content/avatar/avatar2.png",
  //     text: "Great post!",
  //   },
  //   {
  //     id: 2,
  //     author: "John Smith",
  //     authorAvatar: "https://www.bootdey.com/img/Content/avatar/avatar3.png",
  //     text: "I agree, this was a very informative and well-written post.",
  //   },
  // ];

  const getRating = async () => {
    console.log("Get ID:" + item.id);
    // const encodedValue = encodeURIComponent(item.id);
    // console.log("Encode:" + encodedValue);
    try {
      const response = await fetch(
        `${Enviroment.URL_SERVER_API}/rating/all?productId=${item.id}`,
        {
          method: "GET",
        }
      );
      const json = await response.json();
      console.log(json);
      setRate(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getRating();
  }, []);

  const dataSplit = Array.from(rate)
    .sort((a, b) => b.id - a.id)
    .slice(0, 3);
  console.log(dataSplit);

  const avg = (
    Array.from(rate).reduce((acc, currentValue) => acc + currentValue.rate, 0) /
    Array.from(rate).length
  ).toFixed(1);

  console.log("Avg:" + isNaN(avg));

  if (rating != undefined) {
    sentMessage.current += 1;
  }

  function convertDate(ngay) {
    const date = new Date(ngay);
    return (
      date.getDate() +
      "/" +
      date.getMonth() +
      1 +
      "/" +
      date.getFullYear() +
      "   " +
      date.getHours() +
      ":" +
      date.getMinutes()
    );
  }

  console.log("Day:" + convertDate("2024-01-03T17:46:00.4245658"));
  return (
    <View style={styles.wrapContainer}>
      <View style={styles.warpTitleComment}>
        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
          onPress={() => {
            navigation.navigate("AllRatingOfProduct", {
              data: rate,
            });
          }}
        >
          <View>
            <Text style={styles.sectionTitle}>Đánh Giá Sản Phẩm </Text>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Rating
                style={{ marginBottom: "10%", marginTop: "-8%" }}
                size={10}
                rating={isNaN(avg) ? 5 : avg}
              />
              <Text
                style={{
                  marginBottom: "10%",
                  marginLeft: "5%",
                  marginTop: "-8%",
                }}
              >
                {isNaN(avg) ? 0 : avg}/5 (
                {rate.length > 1000
                  ? rate.length / 1000 + "k"
                  : rate.length + sentMessage.current + ""}{" "}
                đánh giá )
              </Text>
            </View>
          </View>

          <View style={{ marginRight: 2, right: 0, flexDirection: "row" }}>
            <Text
              style={{
                fontWeight: "bold",
                color: "red",
                marginTop: "10%",
                marginRight: "2%",
              }}
            >
              Xem Tất Cả
            </Text>
            <Text style={{ marginTop: "7%" }}>
              {" "}
              <Icon name="angle-right" type="font-awesome" color="red" />
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        {rating != undefined && (
          <View style={styles.commentContainer}>
            <View style={styles.avatarContainer}>
              <Text style={styles.avatar}>{rating.userName.slice(0, 1)}</Text>
            </View>
            <View style={styles.commentTextContainer}>
              <Text style={styles.commentAuthor}>
                {rating.userName}
                {"        "}
                <Text
                  style={{
                    fontWeight: "normal",
                    marginLeft: "10%",
                    color: "#000",
                    opacity: 0.5,
                    fontSize: 12,
                  }}
                >
                  {convertDate(rating.createAt)}
                </Text>
              </Text>
              <Rating
                size={10}
                isDisabled={true}
                rating={rating.rate}
                style={{ marginLeft: "1%", marginVertical: "2%" }}
              />
              <Text style={styles.commentText}>{rating.descreption}</Text>
            </View>
          </View>
        )}

        <FlatList
          data={
            dataSplit
            // rate
            // == null || rate == {} || rate == undefined
            //   ? {}
            //   : rate.slice(0, 3)
          }
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.commentContainer}>
              {/* <Image
                source={{ uri: item.authorAvatar }}
                style={styles.commentAvatar}
              /> */}
              <View style={styles.avatarContainer}>
                <Text style={styles.avatar}>{item.userName.slice(0, 1)}</Text>
              </View>
              <View style={styles.commentTextContainer}>
                <Text style={styles.commentAuthor}>
                  {item.userName}
                  {"        "}
                  <Text
                    style={{
                      fontWeight: "normal",
                      marginLeft: "10%",
                      color: "#000",
                      opacity: 0.5,
                      fontSize: 12,
                    }}
                  >
                    {convertDate(item.createAt)}
                  </Text>
                </Text>
                <Rating
                  isDisabled={true}
                  size={10}
                  rating={item.rate}
                  style={{
                    marginLeft: "1%",
                    marginVertical: "2%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                />
                <View style={{ width: "100%", flexWrap: "wrap" }}>
                  <Text style={styles.commentText}>{item.descreption}</Text>
                </View>
              </View>
            </View>
          )}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    backgroundColor: "white",
  },
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "white",
  },
  warpTitleComment: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  content: {
    fontSize: 16,
    lineHeight: 24,
  },
  sectionTitle: {
    fontSize: 15,
    fontWeight: "bold",
    marginBottom: 20,
  },
  commentContainer: {
    flexDirection: "row",
    marginTop: 10,
    paddingBottom: 13,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },
  commentAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  commentTextContainer: {
    marginLeft: 10,
    justifyContent: "center",
    alignItems: "baseline",
  },
  commentAuthor: {
    fontSize: 16,
    fontWeight: "bold",
  },
  commentText: {
    fontSize: 14,
    maxWidth: "90%",
    textAlign: "justify",
  },
  rateCore: {
    marginLeft: "1%",
  },
  avatar: {
    fontSize: 30,
    fontWeight: "700",
  },
  avatarContainer: {
    width: 50,
    height: 50,
    borderRadius: 70,
    backgroundColor: "#ccc",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 6,
    shadowOpacity: 0.16,
  },
});
