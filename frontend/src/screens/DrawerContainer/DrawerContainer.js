import React, { useState, useEffect } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";
import MenuButton from "../../components/MenuButton/MenuButton";
import CartScreen from "../Cart/CartScreen";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { _retrieveData } from "../../data/dataArrays";

export default function DrawerContainer(props) {
  const { navigation } = props;

  const [isLogin, setIsLogin] = useState(false);
  const [user, setUser] = useState({});

  const Logout = async () => {
    await AsyncStorage.removeItem("user");
  };

  return (
    <View style={styles.content}>
      <View style={styles.container}>
        <MenuButton
          title="HOME"
          source={require("../../../assets/icons/home.png")}
          onPress={() => {
            navigation.navigate("Home");
            navigation.closeDrawer();
          }}
        />
        <MenuButton
          title="Danh mục"
          source={require("../../../assets/icons/category.png")}
          onPress={() => {
            navigation.navigate("Categories");
            navigation.closeDrawer();
          }}
        />
        <MenuButton
          title="Giỏ hàng"
          source={require("../../../assets/icons/cart.png")}
          onPress={() => {
            navigation.navigate("Cart");
            navigation.closeDrawer();
          }}
        />
        <MenuButton
          title="Đơn hàng"
          source={require("../../../assets/icons/category.png")}
          onPress={() => {
            // navigation.navigate("OrderConfirmationScreen");
            navigation.navigate("OrderManagement");
            navigation.closeDrawer();
          }}
        />
        <MenuButton
          title="Lịch sử đặt hàng"
          source={require("../../../assets/icons/history_order.png")}
          onPress={() => {
            navigation.navigate("OrderHistory");
            navigation.closeDrawer();
          }}
        />
        <MenuButton
          title="Quét mã QR"
          source={require("../../../assets/icons/qrcode.png")}
          onPress={() => {
            navigation.navigate("ScanQRCode");
            navigation.closeDrawer();
          }}
        />
        {!isLogin && (
          <MenuButton
            title="Login"
            source={require("../../../assets/icons/login.png")}
            onPress={() => {
              navigation.navigate("StartScreen");
              navigation.closeDrawer();
            }}
          />
        )}
        {isLogin && (
          <MenuButton
            title="Logout"
            source={require("../../../assets/icons/logout.png")}
            onPress={() => {
              Logout();
              navigation.navigate("LoginScreen");
              navigation.closeDrawer();
            }}
          />
        )}

        {isLogin && user.role == 0 && (
          <MenuButton
            title="Trang Quản Lí"
            source={require("../../../assets/icons/admin.png")}
            onPress={() => {
              navigation.navigate("DashBoard");
              navigation.closeDrawer();
            }}
          />
        )}
      </View>
    </View>
  );
}

DrawerContainer.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};
