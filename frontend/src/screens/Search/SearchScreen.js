import React, { useEffect, useLayoutEffect, useState } from "react";
import {FlatList, Text, View, Image, TouchableHighlight, Pressable, ActivityIndicator} from "react-native";
import styles from "./styles";
import MenuImage from "../../components/MenuImage/MenuImage";
import {
    getCategoryName,
    getRecipesByRecipeName,
    getRecipesByCategoryName,
    getRecipesByIngredientName,
    getProductByName
} from "../../data/MockDataAPI";
import { TextInput } from "react-native-gesture-handler";
import productStyle from "../Home/ProductStyle";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import axios from "axios";
import Enviroment from "../../../env/Enviroment";
import numeral from "numeral";
import {useDebounce} from '../../hooks/useDebounce'

export default function SearchScreen(props) {
  const { navigation } = props;
  const [searchValue, setSearchValue] = useState("");
  const [searchResult, setSearchResult] = useState([]);
  let [loading, setLoading] = useState(true);
  const debouncedSearchValue = useDebounce(searchValue,300);

  useEffect( () => {
      fetchFindDataProduct().then(() => {
          setLoading(false)
      })
  }, [debouncedSearchValue]);


  useLayoutEffect( () => {
    navigation.setOptions({
      headerLeft: () => (
        <MenuImage
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ),
      headerTitle: () => (
        <View style={styles.searchContainer}>
          <Image style={styles.searchIcon} source={require("../../../assets/icons/search.png")} />
          <TextInput
            style={styles.searchInput}
            onChangeText={handleSearch}
            value={searchValue}
          />
          <Pressable onPress={() => handleSearch("")}>
          {loading && <FontAwesomeIcon name="spinner"/>}
          {!!searchValue && !loading && <Image style={styles.searchIcon} source={require("../../../assets/icons/close.png")} />}
          </Pressable>
        </View>
      ),
      headerRight: () => <View />,
    });
  }, [searchValue, loading]);

    let fetchFindDataProduct = async () => {
        let path = ``;
        if(!debouncedSearchValue.trim()) {
            setSearchValue("")
            path = `/products/all`;
        }else{
            path =`/products/searchProduct?nameProduct=${encodeURIComponent(debouncedSearchValue)}`;
        }
        await axios.get(`${Enviroment.URL_SERVER_API}${path}`)
            .then( (response)=> {
                if (response.status === 200) {
                    setSearchResult(response.data)
                }
            })
            .catch(function (error) {
                if (error.response) {
                    console.error('Server error:', error.response.data);
                } else if (error.request) {
                    console.error('No response from server:', error.request);
                } else {
                    console.error('Request error:', error.message);
                }
            })
    }
    const handleSearch = (text) => {
        setSearchValue(text);
        setLoading(true);
    };
    const onPressProduct = (item) => {
        navigation.navigate("Product", { item });
    };
    const renderProducts = ({ item }) => (
        <TouchableHighlight underlayColor="rgba(84, 23, 137, 0.8)" onPress={() => onPressProduct(item)}>
            <View style={productStyle.container}>
                <Image style={productStyle.photo} source={{ uri: item.photoURL}} />
                <Text style={productStyle.name}>{item.name}</Text>
                <Text style={productStyle.price}>{numeral(item.price).format('0,0').replace(/,/g, '.')}₫</Text>
                <Text>
                    <FontAwesomeIcon name="star" color="#fb6d2c"/>
                    <FontAwesomeIcon name="star" color="#fb6d2c"/>
                    <FontAwesomeIcon name="star" color="#fb6d2c"/>
                    <FontAwesomeIcon name="star" color="#fb6d2c"/>
                    <FontAwesomeIcon name="star" color="#fb6d2c"/>
                    <Text style={{fontSize:12}}> 50</Text>
                </Text>
            </View>
        </TouchableHighlight>
    );
  return (
    <View>
        {!loading && searchResult.length === 0 &&
         <View><Text style={{fontSize: 18, marginTop: 10, marginLeft: 15}}>Không có kết quả tìm kiếm phù hợp</Text></View>
        }
        {!loading && searchResult.length > 0 && <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            numColumns={2}
            data={searchResult}
            renderItem={renderProducts}
            keyExtractor={(item) => `${item.id}`}
        />}
        {loading && <View style={{marginTop:20}}><ActivityIndicator size="large" /></View> }

    </View>
  );
}