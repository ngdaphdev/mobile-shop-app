const WalkthroughAppConfig = {
  onboardingConfig: {
    walkthroughScreens: [
      {
        icon: require("../../../assets/react-native.png"),
        title: "Chào mừng",
        description: "Chào mừng bạn đến trang mua sắm PhoneCare",
      },
      {
        icon: require("../../../assets/educate.png"),
        title: "Educate",
        description:
          "Showcase features to new users so that they get to love your app.",
      },
      {
        icon: require("../../../assets/bell.png"),
        title: "Khám phá ngay!",
        description: "Đăng ký ngay để mua sắm thỏa thích",
      },
    ],
  },
};

export default WalkthroughAppConfig;
