import React, {useEffect} from 'react';
import {View, Text, Image, Button} from 'react-native';
import PropTypes from "prop-types";
import {useNavigation} from '@react-navigation/native';
const OrderConfirmationScreen = ({ route }) => {
    const navigation = useNavigation();
    // Lấy thông tin đơn hàng từ tham số đường dẫn
    const { order } = route.params || { order: null };

    useEffect(() => {
        navigation.setOptions({
            title: 'Quay lại',
        });
    }, []);
    // Kiểm tra xem order có tồn tại không
    if (!order) {
        return (
            <View>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 20 }}>
                    Không tìm thấy thông tin đơn hàng.
                </Text>
            </View>
        );
    }


    return (
        <View>
            <Text style={{ fontSize: 20, fontWeight: 'bold', marginBottom: 10 }}> Đơn hàng của bạn</Text>

            <Text style={{ fontWeight: 'bold' }}> Thông tin người mua:</Text>
            <Text> Tên: {order.buyerName}</Text>
            <Text> Số điện thoại: {order.phoneNumber}</Text>
            <Text> Địa chỉ: {order.address}</Text>

            <Text style={{ marginTop: 10, fontWeight: 'bold' }}> Danh sách sản phẩm:</Text>
            {Array.isArray(order.items) && order.items.length > 0 ? (
                order.items.map((item) => (
                    <View key={item.id}>
                        {/*<Image style={{width: 50, height: 50, resizeMode: "contain", borderRadius: 0}}*/}
                        {/*       source={{uri: item.photoURL}}/>*/}
                        <Text> {item.name}</Text>
                        <Text> {item.price}đ</Text>

                    </View>
                ))
            ) : (
                <Text> Không có sản phẩm trong đơn hàng.</Text>
            )}

            <Text style={{ marginTop: 10, fontWeight: 'bold' }}> Phương thức thanh toán:</Text>
            <Text>
                {order.paymentMethod === '0' ? ' Thanh toán khi nhận hàng' : ' Thanh toán qua VNPay'}
            </Text>


            <Text style={{ marginTop: 10, fontWeight: 'bold' }}> Tổng giá trị đơn hàng:</Text>
            {Array.isArray(order.items) && order.items.length > 0 ? (
                <Text> {calculateTotal(order.items)}đ</Text>
            ) : (
                <Text> Không có sản phẩm trong đơn hàng.</Text>
            )}
            <Button title={"Tiếp tục mua hàng"}  onPress={() => {
                navigation.navigate("Home");
            }}></Button>
            {/*<Text>{calculateTotal(order.items)}đ</Text>*/}
        </View>
    );
};

const calculateTotal = (items) => {
    // Kiểm tra xem items có tồn tại và là một mảng không
    if (Array.isArray(items) && items.length > 0) {
        // Nếu là mảng và có ít nhất một phần tử, thì thực hiện reduce
        return items.reduce((total, item) => total + item.price, 0);
    } else {
        // Nếu không phải mảng hoặc không có phần tử, trả về 0 hoặc một giá trị mặc định khác
        return 0;
    }
};


export default OrderConfirmationScreen;
