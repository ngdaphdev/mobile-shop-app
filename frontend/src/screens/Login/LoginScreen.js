import React, { useState } from "react";
import { TouchableOpacity, StyleSheet, View, ToastAndroid } from "react-native";
import { Text } from "react-native-paper";
import Background from "../../components/login/Background";
import Logo from "../../components/login/Logo";
import Header from "../../components/login/Header";
import Button from "../../components/login/Button";
import TextInput from "../../components/login/TextInput";
import BackButton from "../../components/login/BackButton";
import { theme } from "./core/theme";
import { emailValidator } from "./helpers/emailValidator";
import { passwordValidator } from "./helpers/passwordValidator";
import { log } from "react-native-reanimated";
import Enviroment from "../../../env/Enviroment";
import HomeScreen from "../Home/HomeScreen";
import { login } from "../../data/dataArrays";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const Login = async () => {
    const body = {
      email: email.value,
      password: password.value,
    };
    try {
      const token = await login(body);

      if (token.status == 200) {
        // const  token  = await response.json();

        const user = token.data.data;

        if (user.status == 0) {
          ToastAndroid.show(" Tài Khoản đã bị khóa ", ToastAndroid.SHORT);
          return;
        } else {
          try {
            await AsyncStorage.setItem("user", JSON.stringify(user));
          } catch (error) {
            console.log("Lỗi Lưu:" + error);
          }
        }

        // if (user.role == 1) {
        //   navigation.navigate("Home");
        // } else {
        //   navigation.navigate("DashBoard");
        // }

        navigation.navigate("Home", { user: user });

        //Xử lí đk đúng
      } else if (response.status == 404) {
        console.log("Khong tim thay");
        ToastAndroid.show(" tai khoan khong ton tai", ToastAndroid.SHORT);
        //Xử lí điều kiện k tìm thấy
      } else if (response.status == 501) {
        //Xử lí sai password
        console.log("Sai password");
        ToastAndroid.show(" password khong dung", ToastAndroid.SHORT);
      } else {
        console.log("Không được để trống");
      }
    } catch (error) {
      // Handle login failure
      console.log("Login Failed", error.message);
    }
  };

  return (
    <Background>
      <Logo />
      <Header>Welcome back.</Header>
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: "" })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => navigation.navigate("ResetPasswordScreen")}
        >
          <Text style={styles.forgot}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>
      <Button mode="contained" onPress={Login}>
        Login
      </Button>
      <View style={styles.row}>
        <Text>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
});
