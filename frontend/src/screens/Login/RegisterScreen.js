import React, { useState } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Text } from 'react-native-paper'
import Background from '../../components/login/Background'
import Logo from '../../components/login/Logo'
import Header from '../../components/login/Header'
import Button from '../../components/login/Button'
import TextInput from '../../components/login/TextInput'
import BackButton from '../../components/login/BackButton'
import { theme } from './core/theme'
import { registerAPI } from '../../data/dataArrays'

export default function RegisterScreen({ navigation }) {
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })

  const handleRegister=async () => {
    const body = { 
      email:email.value, 
      password:password.value
    }
    try {

      const token = await registerAPI({
        email:email.value, 
        password:password.value,
        name:name.value
      })
  
      if (token.status == 200) {
          navigation.replace('LoginScreen')
      }else if(response.status == 404){
        console.log("Khong tim thay")
        ToastAndroid.show(" tai khoan da ton tai" , ToastAndroid.SHORT);
        //Xử lí điều kiện k tìm thấy
      }else if(response.status == 501){
        //Xử lí sai password
        console.log("Sai password")
        ToastAndroid.show(" password khong dung" , ToastAndroid.SHORT);
      }else {
        console.log("Không được để trống")
      }
    } catch (error) {
      // Handle login failure
     console.log('Login Failed', error.message);
    }
  } 

  return (
    <Background>
      <Logo />
      <Header>Create Account</Header>
      <TextInput
        label="Name"
        returnKeyType="next"
        value={name.value}
        onChangeText={(text) => setName({ value: text, error: '' })}
        error={!!name.error}
        errorText={name.error}
      />
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button
        mode="contained"
        onPress={handleRegister}
        style={{ marginTop: 24 }}
      >
        Sign Up
      </Button>
      <View style={styles.row}>
        <Text>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.replace('LoginScreen')}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
