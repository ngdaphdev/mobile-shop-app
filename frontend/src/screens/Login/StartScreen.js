import React from 'react'
import Background from '../../components/login/Background'
import Logo from '../../components/login/Logo'
import Header from '../../components/login/Header'
import Button from '../../components/login/Button'
import Paragraph from '../../components/login/Paragraph'
import BackButton from '../../components/login/BackButton'

export default function StartScreen({ navigation }) {
  return (
    <Background>
      <Logo />
      <Header>Login Template</Header>
      <Paragraph>
        The easiest way to start with your amazing application.
      </Paragraph>
      <Button
        mode="contained"
        onPress={() => navigation.navigate('LoginScreen')}
      >
        Login
      </Button>
      <Button
        mode="outlined"
        onPress={() => navigation.navigate('RegisterScreen')}
      >
        Sign Up
      </Button>
    </Background>
  )
}
