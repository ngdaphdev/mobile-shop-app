import React, { useState } from 'react';
import WebView from 'react-native-webview';

const ZaloPay = ({ route }) => {
    const url = route.params?.url;
    return <WebView source={{ uri: `${url}` }} style={{ flex: 1 }} />;
};
export default ZaloPay;