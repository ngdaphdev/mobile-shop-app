// InputInfoScreen.js

import React, {useRef, useState, useEffect} from 'react';
import {
    Alert,
    ScrollView,
    View,
    TextInput,
    TouchableOpacity,
    Text,
    TouchableHighlight,
    Image,
    Dimensions,
    FlatList
} from 'react-native';
import Carousel, {Pagination} from "react-native-snap-carousel";
import styles from "../Product/style";
import BuyButton from "../../components/BuyButton/BuyButton";
import AddCartButton from "../../components/AddCartButton/AddCartButton";
import {useNavigation} from '@react-navigation/native';
import {RadioButton} from 'react-native-paper';
import axios from "axios";
import {Button} from "@rneui/themed";
import Enviroment from '../../../env/Enviroment';
import {Linking} from 'react-native';
import WebView from 'react-native-webview';
import {getSecureHash} from '../../data/dataArrays';
import OrderConfirmationScreen from "../Oder/OrderConfirmationScreen";
import {CheckBox, Card} from 'react-native-elements';

const {width: viewportWidth} = Dimensions.get("window");
const InputInfoScreen = ({route}) => {
    const navigation = useNavigation();
    const {item, selectedCartItems} = route.params || {item: null, selectedCartItems: []};
    const [activeSlide, setActiveSlide] = useState(0);
    const slider1Ref = useRef();
    const [totalPrice, setTotalPrice] = useState(0);
    useEffect(() => {
        navigation.setOptions({
            title: 'Quay lại Mua Hàng',
        });
    }, []);
    const renderImage = ({item}) => (
        <TouchableHighlight>
            <View style={{backgroundColor: '#fff'}}>
                <Image style={{width: '100%', height: 260, resizeMode: 'contain'}} source={{uri: item}}/>
            </View>
        </TouchableHighlight>
    );


    const [buyerName, setBuyerName] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');
    const [paymentMethod, setPaymentMethod] = useState('');
    const [orderDetails, setOrderDetails] = useState(null);

    const handleOrderPress = async () => {
        if (!buyerName || !phoneNumber || !address || !paymentMethod) {
            Alert.alert('Thông báo', 'Vui lòng điền đầy đủ thông tin.');
            return;
        }

        let order;

        if (selectedCartItems && selectedCartItems.length > 0) {
            // Use selectedCartItems if it is not null and has items
            order = {
                buyerName: buyerName,
                phoneNumber: phoneNumber,
                address: address,
                paymentMethod: paymentMethod,
                userId: 1, // Default user ID
                status: 0, // Default status (waiting for confirmation)
                items: selectedCartItems.map(item => ({
                    id: item.id,
                    userId: 1,
                    name: item.name,
                    price: item.price,
                    quantity: item.quantity,
                    // Add other relevant properties from item
                }


                )),
            };
            if (paymentMethod == "1") {
                handleVNPay()
            } else {
                const response = await fetch(Enviroment.URL_SERVER_API + '/api/orders', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(order),

                });
                const result = await response.json();

                console.log("Test")
                console.log()

                if (!response.ok) {
                    // remove cart
                    console.error('Response not okay:', response.status, response.statusText);
                    throw new Error('Failed to create order');
                }else{
                    const idOrderNew = result.id;
                    selectedCartItems.map(async (item) => {
                        const orderDetailData = {
                            orderId: idOrderNew,
                            idProduct: item.id,
                            quantity: item.quantity,
                        };

                        console.log(orderDetailData)
                        await axios.post(Enviroment.URL_SERVER_API + `/api/orderDetails`, orderDetailData)
                            .then(response => {
                                console.log("Order confirmed successfully:", response.data);
                                // handleReloadOrders();
                            })
                            .catch(error => console.error("Error confirming order:", error))
                    })

                    console.log('Order created successfully:', result);
                    // Now you can navigate or perform other actions based on the result
                    Alert.alert('Success', 'Order placed successfully!', [
                        {
                            text: 'OK',
                            onPress: () => {
                                // Navigate to the order confirmation screen
                                navigation.navigate('OrderConfirmationScreen', {order: order});
                            },
                        },
                    ]);
                }


            }

        } else if (item) {
            // Use item if selectedCartItems is null or empty
            order = {
                buyerName: buyerName,
                phoneNumber: phoneNumber,
                address: address,
                paymentMethod: paymentMethod,
                userId: 1,
                status: 0,

                items: [{
                    id: item.id,
                    userId: 1,
                    name: item.name,
                    price: item.price,
                    quantity: 1,
                }],
            };
            if (paymentMethod == "1") {
                handleVNPay()
            } else {
                const response = await fetch(Enviroment.URL_SERVER_API + '/api/orders', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(order),

                });
                const result = await response.json();


                if (!response.ok) {

                    // remove cart
                    console.error('Response not okay:', response.status, response.statusText);
                    throw new Error('Failed to create order');
                }else{
                    const idOrderNew = result.id;
                    const orderDetailData = {
                        orderId: idOrderNew,
                        idProduct: item.id,
                        quantity: item.quantity,
                    };

                    console.log(orderDetailData);

                    try {
                        const response = await axios.post(Enviroment.URL_SERVER_API + `/api/orderDetails`, orderDetailData);
                        console.log("Order confirmed successfully:", response.data);
                        // handleReloadOrders();
                    } catch (error) {
                        console.error("Error confirming order:", error);
                    }
                    console.log('Order created successfully:', result);
                    // Now you can navigate or perform other actions based on the result
                    Alert.alert('Success', 'Order placed successfully!', [
                        {
                            text: 'OK',
                            onPress: () => {
                                // Navigate to the order confirmation screen
                                navigation.navigate('OrderConfirmationScreen', {order: order});
                            },
                        },
                    ]);
                }


            }
        } else {
            // Handle the case when both selectedCartItems and item are null
            Alert.alert('Thông báo', 'Không có sản phẩm nào để đặt hàng.');
            return;
        }


    };
    // Hàm tính tổng tiền từ mảng các sản phẩm trong giỏ hàng
    const calculateTotalPrice = (cartItems, item) => {
        console.log("cartItems:" + cartItems)
        if (cartItems) {
            let totalPrice = 0;
            cartItems.forEach((item) => {
                totalPrice += item.price * item.quantity;
            });
            return totalPrice;
        } else if (item) {
            return item;
        }
    };
    // Hiệu ứng sẽ chạy mỗi khi selectedCartItems thay đổi để cập nhật tổng tiền
    useEffect(() => {
        const newTotalPrice = calculateTotalPrice(selectedCartItems, item);
        setTotalPrice(newTotalPrice);
    }, [selectedCartItems]);
    const renderCartItem = ({item}) => (
        <View style={{flexDirection: 'row', marginLeft: 10, alignItems: 'center', marginBottom: 10}}>
            <Image
                style={{width: 50, height: 50, resizeMode: "contain", borderRadius: 0}}
                source={{uri: item.photoURL}}
            />
            <View style={{marginLeft: 10}}>
                <Text style={{marginRight: 5}}>{item.name}</Text>
                <Text>{item.price}đ</Text>
                <Text>{item.quantity}</Text>
            </View>
        </View>
    );

    const handleVNPay = async () => {
        const totalMoney = selectedCartItems.reduce((acc, item) => acc + item.price, 0);

        const response = await getSecureHash({
            amount: totalMoney,
            orderInfo: `Thanh toan don hang tai Phonecare shop`,
        })
        if (response.status === 'OK') {
            const url = response.data
            navigation.navigate("ZaloPay", {url});
        }
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                {item && (
                    <View style={{flexDirection: 'row', marginLeft: 10, alignItems: 'center', marginBottom: 10}}>
                        <Image
                            style={{width: 50, height: 50, resizeMode: "contain", borderRadius: 0}}
                            source={{uri: item.photoURL}}
                        />
                        <View style={{marginLeft: 10}}>
                            <Text style={{marginRight: 5}}>{item.name}</Text>
                            <Text>{item.price}đ</Text>
                        </View>
                    </View>
                )}
                {selectedCartItems && selectedCartItems.length > 0 && (

                    <FlatList
                        data={selectedCartItems}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={renderCartItem}
                    />

                )}
                {item && (
                    <Card>
                        <View style={{flexDirection: 'row', marginLeft: 10, alignItems: 'center'}}>
                            <Text> Tổng tiền:</Text>
                            <Text> {item.price}đ</Text>
                        </View>
                    </Card>
                )}
                {selectedCartItems && selectedCartItems.length > 0 && (

                    <Card>
                        <View style={{flexDirection: 'row', marginLeft: 10, alignItems: 'center'}}>
                            <Text> Tổng tiền:</Text>
                            <Text>  {totalPrice}đ</Text>
                        </View>
                    </Card>

                )}

                <Card>
                    <TextInput
                        style={styles.input}
                        placeholder="Tên người mua"
                        value={buyerName}
                        onChangeText={(text) => setBuyerName(text)}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Số điện thoại"
                        value={phoneNumber}
                        onChangeText={(text) => setPhoneNumber(text)}
                        keyboardType="phone-pad"
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Địa chỉ"
                        value={address}
                        onChangeText={(text) => setAddress(text)}
                    />
                    <View style={styles.radioButtonContainer}>
                        <RadioButton.Group onValueChange={value => setPaymentMethod(value)} value={paymentMethod}>
                            <View style={styles.radioButton}>
                                <RadioButton value="0"/>
                                <Text>Thanh toán khi nhận hàng</Text>

                            </View>
                            <View style={styles.radioButton}>
                                <RadioButton value="1"/>
                                <Text>Thanh toán qua VNPay</Text>

                            </View>
                        </RadioButton.Group>
                    </View>

                    <TouchableOpacity style={styles.button} onPress={handleOrderPress}>
                        <Text style={styles.buttonText}>Đặt hàng ngay</Text>
                    </TouchableOpacity>
                </Card>
                {/*<View>*/}
                {/*    <Text style={{flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginBottom: 0 ,marginTop: 10}}>Hoặc</Text>*/}
                {/*</View>*/}
                {/*<Card>*/}
                {/*<Button*/}
                {/*    title={'Thanh toán với VNPay'}*/}
                {/*    onPress={handleVNPay}*/}
                {/*/>*/}
                {/*</Card>*/}
            </View>
        </ScrollView>
    );
};

export default InputInfoScreen;
