import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  image: {
    width: 100,
    height: 100,
  },
  box: {
    padding: 20,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: "white",
    flexDirection: "row",
  },
  boxContent: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    marginLeft: 10,
  },
  title: {
    fontSize: 18,
    color: "#151515",
  },
  description: {
    fontSize: 15,
    color: "#646464",
  },
  buttons: {
    flexDirection: "row",
  },
  button: {
    height: 35,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    width: 50,
    marginRight: 5,
    marginTop: 5,
  },
  icon: {
    width: 20,
    height: 20,
  },
  view: {
    backgroundColor: "#eee",
  },
  profile: {
    backgroundColor: "#1E90FF",
  },
  danger: {
    backgroundColor: "rgba(214, 61, 57, 1)",
  },
  lock: {
    backgroundColor: "red",
  },

  lock_open: {
    backgroundColor: "#228B22",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  formContent: {
    flexDirection: "row",
    // marginTop: 10,
  },
  inputContainer: {
    borderBottomColor: "#000",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderWidth: 1,
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    margin: 10,
  },
  iconBtnSearch: {
    alignSelf: "center",
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    flex: 1,
    borderColor: "#000",
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: "center",
  },

  textId: {
    justifyContent: "center",
    alignItems: "center",
    fontSize: 20,
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
});

export default styles;
