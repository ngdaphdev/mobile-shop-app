import {Dimensions, FlatList, ScrollView, Text, TouchableOpacity, View} from "react-native";
import React, {useEffect, useState} from "react";
import {ProductItem} from "../../../../components/Product/ProductItem";
import {Icon} from "@rneui/themed";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
import {useFocusEffect} from "@react-navigation/native";
const { width, height } = Dimensions.get('window');
export default function ProductScreen(props){
    const {navigation} = props;
    const [products, setProducts] = useState([])
    let fetchDataProduct = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/products/all`);
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useFocusEffect(
        React.useCallback(() => {
            fetchDataProduct();
        }, [])
    );

    const handleDetailProduct = (item) => {
        navigation.navigate('DetailProductScreen',{item})
    }

    const renderItem = ({ item, index }) => (
        <TouchableOpacity onPress={()=> handleDetailProduct(item)}>
            <ProductItem key={item.id} item={item} index={index} />
        </TouchableOpacity>
    );

    const handleAddItem = () => {
        navigation.navigate("CreateProduct");
        console.log("Click add product")
    };
    const [total, setTotal] = useState(0)

    useEffect(() => {
        let totalProduct = 0;
        products.forEach((product) => {
            totalProduct += product.quantity;
        });
        setTotal(totalProduct);
    }, [products])

    return (
        <>
            <ScrollView style={{position: 'relative', width: width}}>
                <View style={{padding: 5, backgroundColor: '#e0e0e0'}}></View>
                <View style={{borderWidth: 0.5, borderColor: '#a2a2a2', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15,display:'flex', flexDirection:'row',justifyContent:'space-between'}}>
                    <Text>{products.length} sản phẩm</Text>
                    <Text>Tổng kho có <Text style={{color: '#006fff'}}>{total}</Text> sản phẩm</Text>
                </View>
                <FlatList
                    data={products}
                    renderItem={renderItem}
                    numColumns={1}
                    keyExtractor={(item) => item.id}
                />
            </ScrollView>
            <View style={{ position: 'absolute',top: height*(75/100),bottom: 80, right: width*(10/100)}}>
                <TouchableOpacity style={{backgroundColor: '#3f94e8',width: 50,height: 50,borderRadius: 30, display: 'flex',justifyContent: 'center',alignItems: 'center',elevation: 3,}} onPress={handleAddItem}>
                    <Text style={{textAlign: 'center'}}><FontAwesomeIcon name="plus" size={25} color="white" /></Text>
                </TouchableOpacity>
            </View>
        </>
    )
}
