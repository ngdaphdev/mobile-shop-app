import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Alert,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView, Button, Modal, Pressable, ImageBackground
} from "react-native";

import {Dropdown} from 'react-native-element-dropdown';
import BottomSheet from '@gorhom/bottom-sheet';
import * as ImagePicker from 'expo-image-picker';
import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import {ButtonGroup} from "@rneui/themed";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
const { width, height } = Dimensions.get('window');
export default function DetailProductScreen(props){
    const { navigation, route } = props;
    const item = route.params?.item;
    const [inputNameProduct, setInputNameProduct] = useState('');
    const [inputPriceImportProduct, setInputPriceImportProduct] = useState('');
    const [inputPriceSaleProduct, setInputPriceSaleProduct] = useState('');
    const [selectedImage, setSelectedImage] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [categories, setCategories] = useState([]);
    //Thuong hieu
    const [isTrademarkFocus, setTrademarkIsFocus] = useState(false);
    const [trademarkData, setTrademarkData] = useState([]);
    const [trademark, setTrademark] = useState(null);
    //Nha cung cap
    const [isSupplierFocus, setSupplierIsFocus] = useState(false);
    const [supplierData, setSupplierData] = useState([{label:'Nhà cung cấp 1'.toLocaleUpperCase(), value: 0},{label:'Nhà cung cấp 2'.toLocaleUpperCase(), value: 1},{label:'Nhà cung cấp 3'.toLocaleUpperCase(), value: 2}]);
    const [supplier, setSupplier] = useState(null);
    const [specifications, setSpecifications] = useState([]); // State lưu trữ thông số kỹ thuật được thêm vào
    const [specificationsProduct, setSpecificationsProduct] = useState([]);

    let fetchDataCategories = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/categories/all`)
            setCategories(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };


    let fetchDataSpecificationsProduct = async () => {
        try {
            const response = await axios.get(
                `${Enviroment.URL_SERVER_API}/specifications/getSpecificationsByProductId?productId=${item.id}`
            );
            setSpecificationsProduct(response.data)
            setSpecifications(() => response.data.map(specification => specification.title));
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };
    useEffect(() => {
        fetchDataSpecificationsProduct();
    }, [item.id]);

    useEffect(() => {
        specificationsProduct.forEach((spec) => {
            setSpecValues({ ...specValues, [spec.title]: spec.value });
            console.log('specValue-----: '+specValues)
        });
    }, [specificationsProduct]);

    useEffect(() => {
        setInputNameProduct(item?.name)
        setInputPriceImportProduct(parseInt(item?.importPrice))
        setInputPriceSaleProduct(parseInt(item?.price))
        setTrademark(item?.categoryId)
        setSupplier(item?.supplierID)
        setDescription(item?.description)
    }, [])

    useEffect(() => {
        fetchDataCategories().then(()=>{
        })
    }, [])

    useEffect(() => {
        setTrademarkData(transformedData);
    }, [categories]);

    const transformedData = categories.map(item => ({
        label: item.name.toUpperCase(),
        value: item.id
    }));

    //Update Image
    const openImagePicker = async () => {
        // No permissions request is necessary for launching the image library
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.canceled) {
            setSelectedImage(result.assets[0].uri);
        }
        setModalVisible(false)
    };

    const handleCameraLaunch = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.canceled) {
            setSelectedImage(result.assets[0].uri);
        }
        setModalVisible(false)
    }
    //Name product
    function handleChangeNameProduct(text) {
        setInputNameProduct(text)
    }

    function handleChangePriceImportProduct(text) {
        setInputPriceImportProduct(text)
    }

    function handleChangePriceSaleProduct(text) {
        setInputPriceSaleProduct(text)
    }

    //Specification
    const specificationsButtons = ['Màn hình', 'Hệ điều hành', 'Camera sau', 'Camera trước', 'Chip', 'RAM', 'ROM', 'Pin, Sạc'];
    //Bottom sheet
    // ref
    const bottomSheetRef = useRef(null);
    // variables
    const snapPoints = useMemo(() => ['70%', '70%'], []);
    // callbacks
    const handleSheetChanges = useCallback((index: number) => {
        console.log('handleSheetChanges', index);
    }, []);
    const openAddSpecBottomSheet = () =>{
        bottomSheetRef.current?.expand()
    }
    const [selectedIndexes, setSelectedIndexes] = useState([]);
    // Handle focus specification select
    const addSpecification = (index) => {
        setSelectedIndexes(index);
        const keys = Object.keys(index);
        let specSelect = []
        for (let i = 0; i < keys.length; i++) {
            specSelect = [...specSelect, specificationsButtons[index[keys[i]]]]
        }
        setSpecifications(specSelect)
    }
    const handleAddSpecification = () => {
        bottomSheetRef.current?.close()
    }
    const [specValues, setSpecValues] = useState({});
    function handleInputSpecChange(text, spec) {
        setSpecValues({ ...specValues, [spec]: text });
    }
    //Description
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (text) =>{
        setDescription(text);
    }
    function resetValue(){
        setInputNameProduct('')
        setInputPriceSaleProduct('')
        setInputPriceImportProduct('')
        setSupplier(null)
    }

    let uploadImage = async (uriFile, nameFile) => {
        console.log(uriFile, nameFile)
        const formData = new FormData()
        formData.append("image", {
            uri: uriFile,
            name: nameFile,
            type: 'image/png'
        })

        const response = await axios.post(`${Enviroment.URL_SERVER_API}/common/upload`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        }).then((r) => {
            // Xử lý dữ liệu khi yêu cầu thành công
            console.log(r.data);
        })
            .catch((error) => {
                // Xử lý lỗi từ máy chủ hoặc từ yêu cầu Axios
                console.error('Error:', error.message);
                if (error.response) {
                    // Nếu có response từ máy chủ, in ra status và data (nếu có)
                    console.error('Response Status:', error.response.status);
                    console.error('Response Data:', error.response.data);
                }
            });
        return response;
    }

    let updateProductAPI = async (productId, categoryId, nameProduct, photoURL, importPrice, price, supplierID, description) => {
        try {
            const response = await axios.put(`${Enviroment.URL_SERVER_API}/products/updateProduct?productId=${productId}&categoryId=${categoryId}&nameProduct=${nameProduct}&photoURL=${photoURL}&importPrice=${importPrice}&price=${price}&description=${description}&supplierID=${supplierID}`);
            return response.data;
        } catch (error) {
            return 0;
        }
    };

    let resetSpecAPI = async (productId) => {
        try {
            const response = await axios.delete(`${Enviroment.URL_SERVER_API}/specifications/deleteByProductId?productId=${productId}`);
            return response.data;
        } catch (error) {
            return error;
        }
    };
    let createSpecsAPI = async (productId, titleSpec, valueSpec) => {
        try {
            const response = await axios.post(`${Enviroment.URL_SERVER_API}/specifications/add?productId=${productId}&titleSpec=${titleSpec}&valueSpec=${valueSpec}`);
            return response.data;
        } catch (error) {
            return 0;
        }
    };
    function handleUpdateProduct() {
        console.log(selectedImage)
        let filename = selectedImage !== null ? selectedImage.substring(selectedImage.lastIndexOf('/') + 1, selectedImage.length) : item.photoURL;
        if (inputNameProduct.trim() === '' || trademark === null ||  supplier === null || description.trim() === '') {
            Alert.alert('Thông báo', 'Vui lòng điền đầy đủ các thông tin hàng hóa', [
                {text: 'OK', onPress: () => {}},
            ]);
        }else{
            updateProductAPI(item.id, trademark, inputNameProduct, filename, inputPriceImportProduct, inputPriceSaleProduct, supplier, description).then((r)=>{
                console.log(r)
                if(r === 'Product updated successfully'){
                    if(selectedImage !== null){
                        uploadImage(selectedImage, filename)
                    }
                    resetSpecAPI(item.id).then((r)=>{
                        console.log('r------: '+r)
                        if(r === 'Deleted'){
                            for (const key in specValues) {
                                if (Object.hasOwnProperty.call(specValues, key)) {
                                    const title = key;
                                    const value = specValues[key];
                                    console.log("Title la:", title);
                                    console.log("Value la:", value);
                                    let rs = createSpecsAPI(item.id, title, value)
                                    console.log('rs----: '+rs)
                                }else{
                                    console.log(null);
                                }
                            }
                        }
                    })

                    Alert.alert('Thông báo', 'Đã sửa hàng hóa thành công', [
                        {text: 'OK', onPress: () => {}},
                    ]);
                }else{
                    Alert.alert('Thông báo', 'Sửa hàng hóa thất bại! Vui lòng kiểm tra và thực hiện lại', [
                        {text: 'OK', onPress: () => {}},
                    ]);
                }
            })
        }
    }

    return (
        <>
            <ScrollView>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        setModalVisible(!modalVisible);
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{display: 'flex', flexDirection: 'row'}}>
                                <View style={{ marginTop: 20, marginRight: 20}}>
                                    <TouchableOpacity style={{borderWidth: 1, borderRadius: 10, padding: 10, width: width*(40/100),display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}} onPress={openImagePicker}>
                                        <Image  source={require('../../../../../assets/cam-icon.png')} style={{width: width*(15/100), margin: 10, height: width*(20/100), resizeMode: 'contain'}} />
                                        <Text style={{textAlign: 'center'}}>Chọn ảnh từ thiết bị</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ marginTop: 20, marginBottom: 50 , backgroundColor: 'none'}}>
                                    <TouchableOpacity style={{borderWidth: 1, borderRadius: 10, padding: 10, width: width*(40/100),display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}} onPress={handleCameraLaunch}>
                                        <Image  source={require('../../../../../assets/gallery-icon.jpg')} style={{width: width*(20/100),margin: 10, height: width*(20/100), resizeMode: 'contain'}} />
                                        <Text style={{textAlign: 'center'}}>Chụp từ Camera</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Pressable
                                style={[{width: width*(85/100), padding: 10, borderRadius: 10}, styles.buttonClose]}
                                onPress={() => setModalVisible(!modalVisible)}>
                                <Text style={styles.textStyle}>Huỷ</Text>
                            </Pressable>
                        </View>
                    </View>
                </Modal>
                <View>
                    <View style={{}}>
                        {item.photoURL.includes("http") ? (
                            <ImageBackground source={{ uri: item.photoURL }} resizeMode="cover" style={{width: width, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: height*(30/100)}}>
                                <TouchableOpacity onPress={() => setModalVisible(true)}>
                                    <Image  source={require('../../../../../assets/camera.png')} style={{width: width*(20/100), height: height*(20/100), resizeMode: 'contain'}} />
                                </TouchableOpacity>
                            </ImageBackground>
                        ) : (
                            <ImageBackground source={{uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,}} resizeMode="cover" style={{width: width, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: height*(30/100)}}>
                                <TouchableOpacity onPress={() => setModalVisible(true)}>
                                    <Image  source={require('../../../../../assets/camera.png')} style={{width: width*(20/100), height: height*(20/100), resizeMode: 'contain'}} />
                                </TouchableOpacity>
                             </ImageBackground>
                        )}

                    </View>
                    <View style={styles.box}>
                        <View style={{display: 'flex', flexDirection: 'row', margin: 10}}>
                            <View style={{display: 'flex', flexBasis: '30%'}}>
                                <Text style={styles.row}>Mã hàng</Text>
                                <Text style={styles.row}>Tên hàng</Text>
                                <Text style={styles.row}>Giá nhập</Text>
                                <Text style={styles.row}>Giá bán</Text>
                            </View>
                            <View style={{display: 'flex', flexBasis: '65%'}}>
                                <TextInput editable={false} placeholder='Auto' style={[styles.row, styles.underText]}/>
                                <TextInput value={inputNameProduct} onChangeText={handleChangeNameProduct} placeholder='Tên hàng' style={[styles.row, styles.underText]}/>
                                <TextInput value={inputPriceImportProduct} onChangeText={handleChangePriceImportProduct} placeholder={'' + inputPriceImportProduct} keyboardType="numeric" style={[styles.row, styles.underText]}/>
                                <TextInput value={inputPriceSaleProduct} onChangeText={handleChangePriceSaleProduct} placeholder={''+inputPriceSaleProduct} keyboardType="numeric" style={[styles.row, styles.underText]}/>
                            </View>
                        </View>
                    </View>

                    <View style={styles.box}>
                        <View style={{display: 'flex', flexDirection: 'row', margin: 10}}>
                            <View style={{display: 'flex', flexBasis: '30%'}}>
                                <Text style={[styles.row]}>Thương hiệu</Text>
                                <Text style={[styles.row, {marginTop: 20}]}>Nhà cung cấp</Text>
                            </View>
                            <View style={{display: 'flex', flexBasis: '65%'}}>
                                <View style={[styles.row]}>
                                    <Dropdown
                                        style={[styles.dropdown, isTrademarkFocus && {borderColor: 'blue'}]}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        inputSearchStyle={styles.inputSearchStyle}
                                        iconStyle={styles.iconStyle}
                                        data={trademarkData}
                                        search
                                        maxHeight={300}
                                        labelField="label"
                                        valueField="value"
                                        placeholder={!isTrademarkFocus ? 'Chọn thương hiệu' : '...'}
                                        searchPlaceholder="Search..."
                                        value={trademark}
                                        onFocus={() => setTrademarkIsFocus(true)}
                                        onBlur={() => setTrademarkIsFocus(false)}
                                        onChange={item => {
                                            setTrademark(item.value);
                                        }}
                                    />
                                </View>

                                <View style={[styles.row, {marginBottom: 10, marginTop: 20}]}>
                                    <Dropdown
                                        style={[styles.dropdown, isSupplierFocus && {borderColor: 'blue'}]}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        inputSearchStyle={styles.inputSearchStyle}
                                        iconStyle={styles.iconStyle}
                                        data={supplierData}
                                        search
                                        maxHeight={300}
                                        labelField="label"
                                        valueField="value"
                                        placeholder={!isSupplierFocus ? 'Chọn nhà cung cấp' : '...'}
                                        searchPlaceholder="Search..."
                                        value={supplier}
                                        onFocus={() => setSupplierIsFocus(true)}
                                        onBlur={() => setSupplierIsFocus(false)}
                                        onChange={item => {
                                            setSupplier(item.value);
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    {/*Specification*/}
                    <View style={styles.box}>
                        <View style={{display: 'flex', flexDirection: 'column', margin: 10}}>
                            <TouchableOpacity style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}} onPress={openAddSpecBottomSheet}>
                                <View style={{backgroundColor: '#3f94e8',width: 20,height: 20,borderRadius: 30, display: 'flex',justifyContent: 'center',alignItems: 'center',elevation: 3,}}>
                                    <FontAwesomeIcon name="plus" size={10} color="white" />
                                </View>
                                <Text style={{marginLeft: 5}}>Thêm thông số kỹ thuật</Text>
                            </TouchableOpacity>
                        </View>
                        {specifications.map((spec, index) => (
                            <View style={{display: 'flex', flexDirection: 'row', margin: 10}} key={index} >
                                <View style={{display: 'flex', flexBasis: '30%'}}>
                                    <Text style={[styles.row, {backgroundColor: 'white'}]}>{spec}</Text>
                                </View>
                                <View style={{display: 'flex', flexBasis: '65%'}}>
                                    <TextInput onChangeText={(text) => handleInputSpecChange(text, spec)} placeholder={specificationsProduct[index]?.value !== undefined ? specificationsProduct[index]?.value + '' : 'Nhập dữ liệu'} style={[styles.row, styles.underText]}/>
                                </View>
                            </View>
                        ))}
                    </View>
                    {/*Description*/}
                    <View style={styles.box}>
                        <View style={{display: 'flex', flexDirection: 'column', margin: 10}}>
                            <Text style={{marginLeft: 5}}>Mô tả sản phẩm</Text>
                            <TextInput
                                style={{marginLeft: 5, minHeight: 100}}
                                multiline={true}
                                numberOfLines={4}
                                placeholder="Nhập mô tả sản phẩm..."
                                value={description}
                                onChangeText={handleDescriptionChange}
                            />
                        </View>
                    </View>

                </View>
                <TouchableOpacity style={styles.button} onPress={() => handleUpdateProduct()}>
                    <Text style={{textAlign:'center', width: '100%', color: 'white', fontSize: 16}}>Chỉnh sửa hàng hóa</Text>
                </TouchableOpacity>
            </ScrollView>
            <BottomSheet
                ref={bottomSheetRef}
                index={-1}
                snapPoints={snapPoints}
                onChange={handleSheetChanges}
            >
                <View style={styles.contentContainer}>
                    <Text style={{fontSize: 16, marginLeft: 20, fontWeight: 'bold'}}>Danh sách thông số kỹ thuật</Text>
                </View>
                <ButtonGroup
                    vertical={true}
                    buttons={['Màn hình', 'Hệ điều hành', 'Camera sau', 'Camera trước', 'Chip', 'RAM', 'ROM', 'Pin, Sạc']}
                    selectMultiple
                    selectedIndexes={selectedIndexes}
                    onPress={addSpecification}
                    containerStyle={{marginBottom: 20, color: '#000'}}
                    buttonStyle={{textAlign: 'left'}}
                    textStyle={{color: '#000', textAlign: 'left'}}
                />
                <TouchableOpacity onPress={handleAddSpecification} style={styles.button}>
                    <Text style={{textAlign: 'center', width: '100%', color: 'white', fontSize: 16}}>Xong</Text>
                </TouchableOpacity>
            </BottomSheet>
        </>
    )
}

const styles = StyleSheet.create({
    box: {
        backgroundColor:'white',
        margin: 10
    },
    row:{
        lineHeight: 40,
        height: 40,
        textAlign: 'left',
        fontSize: 15,
        maxHeight: 40,
        marginTop: 10,
    },
    underText:{
        borderWidth: 1,
        borderTopColor: '#fff',
        borderLeftColor: '#fff',
        borderRightColor: '#fff',
        borderBottomColor: '#b2b2b2'
    },
    button: {
        borderRadius: 10,
        width: width*(94/100),
        backgroundColor: 'blue',
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginLeft: width*(3/100),
        marginBottom: 20,
        paddingTop: 15,
        paddingBottom: 15
    },
    dropdown: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
        marginBottom: 10,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
    containerSheetBottom: {
        flex: 1,
        padding: 24,
        backgroundColor: 'red',
        height: 500
    },
    contentContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 1,
            height: 5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: width*(95/100)
    },
    buttonOpen: {
        backgroundColor: '#F194FF',
    },
    buttonClose: {
        backgroundColor: '#2196F3',
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
})