import {View, Text, FlatList, TouchableOpacity, Dimensions, StyleSheet} from "react-native";
import React, {useEffect, useState} from "react";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import {AntDesign, FontAwesome5, Ionicons} from "@expo/vector-icons";
import numeral from "numeral";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
import {ImportCouponItem} from "../../../../components/Product/ImportCouponItem";
const { width, height } = Dimensions.get('window');
export default function ReturnProductScreen(props){
    const {navigation} = props;
    const handleAddItem = () => {
        navigation.navigate("CreateReturnCoupon");
    }
    const [returnCoupons, setReturnCoupons] = useState([])
    const [totalPriceReturn, setTotalPriceReturn] = useState(0)
    let fetchDataReturnCoupon = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/returncoupons/all`);
            setReturnCoupons(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    useEffect(() => {
        fetchDataReturnCoupon().then(() => {
        });
    }, [])

    useEffect(() => {
        if(totalPriceReturn === 0){
            let totalPrice = 0;
            returnCoupons.forEach((returnCoupon) => {
                totalPrice += returnCoupon.totalAmount;
            });
            setTotalPriceReturn(totalPrice);
        }
    }, [returnCoupons])
    const detailCoupon = (item) => {
        navigation.navigate("DetailReturnCoupon", {item})
    }

    const renderItem = ({ item, index }) => {
        const date = new Date(item.dateTime);
        const formattedDateTime =
            `${date.getDate().toString().padStart(2, '0')}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getFullYear()} ` +
            `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
        return (
            <TouchableOpacity onPress={() => detailCoupon(item)}>
                <View style={styles.container}>
                    <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'flex-start'}}>
                        <FontAwesome5 style={{paddingBottom: 25, paddingLeft: 15, paddingRight: 10}} name="money-bill-alt" size={24} color="black" />
                        <View style={styles.text}>
                            <Text style={styles.price}>{numeral(item.totalAmount).format('0,0').replace(/,/g, '.')}₫</Text>
                            <Text style={styles.id}>Mã phiếu: {item.id}</Text>
                            <Text style={styles.id}>Người lập: {item.returnPerson}</Text>
                        </View>
                    </View>
                    <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                        <Text>{formattedDateTime}</Text>
                        <Text></Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };
    return(
        <View>
            <View style={{borderWidth: 0.5, borderColor: '#a2a2a2', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white'}}>
                <Text><AntDesign name="calendar" size={16} color="black" /> Tháng này <AntDesign name="down" size={10} color="black" /></Text>
            </View>
            <View style={{display: 'flex',flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white'}}>
                <Text><Text style={{color: '#006fff'}}>{returnCoupons.length}</Text> phiếu</Text>
                <Text>Tổng tiền trả hàng: <Text style={{color: '#006fff'}}>{numeral(totalPriceReturn).format('0,0').replace(/,/g, '.')}đ</Text></Text>
            </View>
            <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{margin: 8, marginTop: 18}}>Tất cả phiếu trả hàng</Text>
                <TouchableOpacity onPress={() => fetchDataReturnCoupon()}>
                    <Ionicons style={{marginRight: 20, padding: 8, backgroundColor:'#c9c9c9', marginBottom: 10, marginTop: 10, borderRadius: 10}} name="reload" size={16} color="black" />
                </TouchableOpacity>
            </View>
            <View style={{backgroundColor: 'white'}}>
                <View>
                    <FlatList
                        data={returnCoupons}
                        renderItem={renderItem}
                        numColumns={1}
                        keyExtractor={(item) => item.id}
                    />
                </View>
            </View>
            <View style={{ position: 'absolute',top: height*(75/100),bottom: 80,  right: width*(10/100)}}>
                <TouchableOpacity style={{backgroundColor: '#3f94e8',width: 50,height: 50,borderRadius: 30, display: 'flex',justifyContent: 'center',alignItems: 'center',elevation: 3,}} onPress={handleAddItem}>
                    <Text style={{textAlign: 'center'}}><FontAwesomeIcon name="plus" size={25} color="white" /></Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 10
    },
})