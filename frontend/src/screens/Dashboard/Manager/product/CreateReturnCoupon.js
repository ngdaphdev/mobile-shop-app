import React, {useEffect, useState} from "react";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
import {
    Alert,
    Dimensions,
    FlatList, Image,
    Modal,
    Pressable, ScrollView,
    StyleSheet,
    Text,
    TextInput, TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";
import {ProductItem} from "../../../../components/Product/ProductItem";
import numeral from "numeral";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import {AntDesign, EvilIcons} from "@expo/vector-icons";
const { width, height } = Dimensions.get('window');
export default function CreateReturnCoupon(){
    const [products, setProducts] = useState([])
    const [searchValue, setSearchValue]= useState("")
    const [searchResult, setSearchResult]= useState([])
    const [listProductSelected, setListProductSelected] = useState([])
    const [productSelected, setProductSelected] = useState({})
    const [modalVisible, setModalVisible] = useState(false);
    const [quantity:number, setQuantity] = useState(1);
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalQuantityCoupon:number, setTotalQuantityCoupon] = useState(0);
    const [totalPriceCoupon, setTotalPriceCoupon] = useState(0);
    let [loading, setLoading] = useState(false);
    let fetchDataProductCurrently = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/products/getProductCurrently`);
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    function handleSearch(text) {
        setLoading(true)
        setSearchValue(text);
    }

    const filteredProducts = products.filter(product => {
        return product?.name.toLowerCase().includes(searchValue.toLowerCase());
    });

    useEffect(()=>{
        // Xu ly khi nhap value
        if(searchValue.length > 0){
            setSearchResult(filteredProducts)
        }
        setLoading(false)
    },[searchValue])
    const onPressProductSearch = (item) => {
        //Xu ly khi bam chon item da search
        setSearchValue('')
        setTotalPrice(item.price)
        setProductSelected(item)
        setModalVisible(!modalVisible)
    };
    const renderProductSearch = ({ item, index }) => (
        <TouchableHighlight underlayColor="rgba(84, 23, 137, 0.8)" onPress={() => onPressProductSearch(item)}>
            <View style={styles.container}>
                <View style={{flexDirection: 'row'}}>
                    {item?.photoURL?.includes("http") ? (
                        <Image style={styles.photo} source={{ uri: item.photoURL }} />
                    ) : (
                        <Image
                            style={styles.photo}
                            source={{
                                uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
                            }}
                        />
                    )}
                    <View style={styles.text}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                        <Text style={styles.id}>Mã: {item.id}</Text>
                        <Text>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                    </View>
                </View>
                <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                    <Text></Text>
                    <Text>Trong kho: {item.quantity}</Text>
                </View>
            </View>
        </TouchableHighlight>
    );
    useEffect(() => {
        fetchDataProductCurrently();
    }, [])
    function handleChangeQuantity(text) {
        setQuantity(text)
        if(productSelected!=null){
            setTotalPrice(text * productSelected.price)
        }else{
            setTotalPrice(0)
        }
    }
    const handleOpenModalReturnProduct = (item) => {
        setProductSelected(item)
        setModalVisible(!modalVisible);
    }
    function handleReturnProductSelected() {
        productSelected.quantity = quantity
        productSelected.totalPrice = totalPrice
        setTotalPriceCoupon(totalPriceCoupon + totalPrice)
        setTotalQuantityCoupon(parseInt(totalQuantityCoupon) + parseInt(quantity))
        setListProductSelected([...listProductSelected, productSelected])
        setModalVisible(!modalVisible)
    }

    const renderItem = ({ item, index }) => (
        <TouchableOpacity onPress={() => handleOpenModalReturnProduct(item)}>
            <ProductItem key={item.id} item={item} index={index} />
        </TouchableOpacity>
    );

    const renderProduct = ({ item, index }) => (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                {item?.photoURL?.includes("http") ? (
                    <Image style={styles.photo} source={{ uri: item.photoURL }} />
                ) : (
                    <Image
                        style={styles.photo}
                        source={{
                            uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
                        }}
                    />
                )}
                <View style={styles.text}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                    <Text style={styles.id}>Mã: {item.id}</Text>
                    <Text style={styles.id}>SL: {item.quantity}</Text>
                </View>
            </View>
            <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                <Text style={{fontSize: 13}}>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                <Text>{numeral(item.totalPrice).format('0,0').replace(/,/g, '.')}đ</Text>
                <EvilIcons name="trash" size={24} color="red"/>
            </View>
        </View>
    );
    let createDetailReturnCoupon = async (returnCouponId, productId, returnQuantity, returnPrice) => {
        try {
            const response = await axios.post(`${Enviroment.URL_SERVER_API}/detailreturncoupons/add?returnCouponId=${returnCouponId}&productId=${productId}&returnQuantity=${returnQuantity}&returnPrice=${returnPrice}`);
            return response.data;
        } catch (error) {
            console.log('error: '+error)
            return 'Error';
        }
    };
    const resetImportCoupon = () => {
        setTotalQuantityCoupon(0)
        setTotalPriceCoupon(0)
        setListProductSelected([])
        setQuantity(0)
    }
    let createReturnCoupon = async () => {
        try {
            const response = await axios.post(`${Enviroment.URL_SERVER_API}/returncoupons/add?returnPerson=${'Admin'}&totalAmount=${totalPriceCoupon}&quantity=${totalQuantityCoupon}&supplierId=1`);
            return response.data;
        } catch (error) {
            return 'Error';
        }
    };
    let importQuantityProduct = async (productId, quantity) => {
        try {
            const response = await axios.put(`${Enviroment.URL_SERVER_API}/products/import?idProduct=${productId}&quantity=${quantity*(-1)}`);
            return response.data;
        } catch (error) {
            console.log('error: '+error)
            return 'Error';
        }
    };
    function handleReturnProduct() {
        if(totalQuantityCoupon > 0){
            let idCoupon = createReturnCoupon().then((id) => {
                console.log("kq: ", id);
                listProductSelected.forEach((product) => {
                    createDetailReturnCoupon(id, product.id, product.quantity, product.totalPrice).then(r => {console.log(r)})
                    importQuantityProduct(product.id, product.quantity).then(r => {console.log(r)})
                });
                Alert.alert('Thông báo', 'Đã trả hàng thành công', [
                    {text: 'OK', onPress: () => {resetImportCoupon()}},
                ]);
            }).catch((error) => {
                console.error("Error: ", error);
            });
        }else{
            Alert.alert('Thông báo', 'Vui lòng thêm nội dung trả hàng', [
                {text: 'OK', onPress: () => {}},
            ]);
        }
    }

    return (
        <View style={{borderWidth: 0.5, borderColor: '#a2a2a2'}}>
            <View style={{backgroundColor: '#fff', paddingTop: 10, paddingBottom: 10, width: width, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <View style={styles.searchContainer}>
                    <Image style={styles.searchIcon} source={require("../../../../../assets/icons/search.png")} />
                    <TextInput
                        style={styles.searchInput}
                        onChangeText={handleSearch}
                        placeholder='Chọn hàng cần trả trong kho'
                        value={searchValue}
                    />
                    <Pressable onPress={() => handleSearch("")}>
                        {loading && <FontAwesomeIcon name="spinner"/>}
                        {searchValue.length > 0 && !loading && <Image style={styles.searchIcon} source={require("../../../../../assets/icons/close.png")}/>}
                    </Pressable>
                    <View style={{backgroundColor: 'white',position: 'absolute', top: 50, zIndex: 999, left: 0, right: 0, shadowColor: '#000', shadowOffset: {width: 1, height: 5,}, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5,borderRadius: 10, width: width*(90/100)}}>
                        {searchResult.length > 0 && searchValue.length > 0 &&
                            <View>
                                <Text style={{margin: 8}}>Các sản phẩm hiện có trong kho: </Text>
                                <FlatList
                                    data={searchResult}
                                    renderItem={renderProductSearch}
                                    numColumns={1}
                                    keyExtractor={(item) => item.id}
                                />
                            </View>
                        }
                    </View>
                </View>
            </View>
            <Text style={{margin: 8, zIndex: -1, marginLeft: 20}}>Danh sách sản phẩm cần trả hàng: </Text>
            <ScrollView style={{backgroundColor: '#fff', minHeight: height*(70/100), zIndex: -1}}>
                <View style={{paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15}}>
                    {listProductSelected.length === 0 && <View style={{display: 'flex', justifyContent: 'center',  alignItems: 'center', marginTop: height*(15/100)}}>
                        <Image source={require('../../../../../assets/coupon-icon.png')} style={{width: width*(30/100),margin: 10, height: width*(30/100), resizeMode: 'contain'}} />
                        <Text>Chưa có hàng hóa nào trong phiếu trả hàng</Text>
                    </View>}
                    {listProductSelected.length > 0 && <FlatList
                        data={listProductSelected}
                        renderItem={renderProduct}
                        numColumns={1}
                        keyExtractor={(item) => item.id}
                    />}
                </View>
            </ScrollView>
            <TouchableHighlight onPress={handleReturnProduct}>
                <View style={{backgroundColor: '#2196F3', display: 'flex', flexDirection: 'row', justifyContent:'space-between', alignItems: 'center', padding: 16, margin:20, borderRadius: 10 }}>
                    <Text style={{color: 'white', fontSize: 16}}>Tổng SL: {totalQuantityCoupon} - {numeral(totalPriceCoupon).format('0,0').replace(/,/g, '.')}đ </Text>
                    <Text style={{color: 'white', fontSize: 16}}>Trả hàng <AntDesign name="right" size={16} color="white" /></Text>
                </View>
            </TouchableHighlight>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {productSelected?.photoURL?.includes("http") ? (
                            <Image style={{width: width, height: height*(50/100), resizeMode: 'contain'}} source={{ uri: productSelected?.photoURL }} />
                        ) : (
                            <Image
                                style={{width: width, height: height*(50/100), resizeMode: 'contain'}}
                                source={{
                                    uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${productSelected?.photoURL}`,
                                }}
                            />
                        )}
                        <View style={styles.box}>
                            <View style={{display: 'flex', flexDirection: 'row', margin: 10}}>
                                <View style={{display: 'flex', flexBasis: '25%'}}>
                                    <Text style={styles.row}>Số lượng</Text>
                                    <Text style={styles.row}>Giá</Text>
                                    <Text style={styles.row}>Tổng tiền</Text>
                                </View>
                                <View style={{display: 'flex', flexBasis: '70%'}}>
                                    <TextInput onChangeText={handleChangeQuantity} placeholder="1" keyboardType="numeric" style={[styles.row, styles.underText]}/>
                                    <Text style={[styles.row, styles.underText]}>{numeral(productSelected?.price).format('0,0').replace(/,/g, '.')}</Text>
                                    <Text style={[styles.row, styles.underText]}>{numeral(totalPrice).format('0,0').replace(/,/g, '.')}đ</Text>
                                </View>
                            </View>
                        </View>

                        <Pressable
                            style={[{width: width*(85/100), padding: 10, borderRadius: 10}, styles.buttonClose]}
                            onPress={handleReturnProductSelected}>
                            <Text style={styles.textStyle}>Thêm vào trả hàng</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>

    )
}

const styles = StyleSheet.create({
    searchContainer: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#EDEDED",
        borderRadius: 10,
        width: width*(90/100),
        justifyContent: "space-around"
    },
    searchIcon: {
        width: 20,
        height: 20,
        tintColor: 'grey'
    },
    searchInput: {
        backgroundColor: "#EDEDED",
        color: "black",
        width:  width*(65/100),
        height: 50,
    },
    box: {
        margin: 10
    },
    row:{
        lineHeight: 40,
        height: 40,
        textAlign: 'left',
        fontSize: 15,
        maxHeight: 40,
        marginTop: 10,
    },
    underText:{
        borderWidth: 1,
        borderTopColor: '#fff',
        borderLeftColor: '#fff',
        borderRightColor: '#fff',
        borderBottomColor: '#b2b2b2'
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 20,
        alignItems: "center",
        shadowColor: '#000',
        shadowOffset: {
            width: 1,
            height: 5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: width*(100/100),
        paddingBottom: height*(38/100)
    },
    buttonOpen: {
        backgroundColor: '#F194FF',
    },
    buttonClose: {
        backgroundColor: '#2196F3',
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
    contentContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },centeredView: {
        flex: 1,
    },imageContainer: {
        flex: 1,
        justifyContent: 'center',
        width: width,
        height: height
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'flex-end',
        paddingVertical: 8,
        marginTop: 200,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 0
    },
    container: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between',
        marginTop: 10,
        paddingBottom: 10
    },
    photo: {
        width: width*(15/100),
        height: width*(15/100),
        resizeMode: "contain",
        margin: 5
    },
    text:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    name:{
        color: '#000',
        fontSize: 15,
        width: width*(45/100)
    },
    id: {
        color: '#656565',
        fontSize: 13
    }
});