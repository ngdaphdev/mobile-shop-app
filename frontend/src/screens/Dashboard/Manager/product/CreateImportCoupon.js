import {
    Text,
    View,
    TextInput,
    Image,
    StyleSheet,
    Pressable,
    Dimensions,
    ScrollView,
    FlatList,
    TouchableHighlight, Alert, TouchableOpacity, Modal
} from "react-native";
import React, {useEffect, useRef, useState} from "react";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import {ProductItem} from "../../../../components/Product/ProductItem";
import Carousel, {Pagination} from "react-native-snap-carousel";
import { EvilIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import numeral from "numeral";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
const { width, height } = Dimensions.get('window');

export default function CreateImportCoupon(props){
    const { navigation, route } = props;
    const products = route.params?.products;
    const [searchValue, setSearchValue]= useState("")
    const [searchResult, setSearchResult]= useState([])
    const [listProductSelected, setListProductSelected] = useState([])
    const [productSelected, setProductSelected] = useState({})
    const [modalVisible, setModalVisible] = useState(false);
    const [quantity:number, setQuantity] = useState(1);
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalQuantityCoupon:number, setTotalQuantityCoupon] = useState(0);
    const [totalPriceCoupon, setTotalPriceCoupon] = useState(0);
    let [loading, setLoading] = useState(false);
    function handleSearch(text) {
        setLoading(true)
        setSearchValue(text);
    }

    const dataListFake = [
        {id: 1, name: 'iphone 11', photoURL: 'https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/Products/Images/42/289700/TimerThumb/iphone-14-pro-max-256gb-(56).jpg', price: 2000000},
        {id: 2, name: 'iphone 12', photoURL: 'https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/Products/Images/42/289700/TimerThumb/iphone-14-pro-max-256gb-(56).jpg', price: 3000000},
        {id: 3, name: 'iphone 13', photoURL: 'https://img.tgdd.vn/imgt/f_webp,fit_outside,quality_100/https://cdn.tgdd.vn/Products/Images/42/289700/TimerThumb/iphone-14-pro-max-256gb-(56).jpg', price: 3000000},
    ]

    const filteredProducts = products.filter(product => {
        return product?.name.toLowerCase().includes(searchValue.toLowerCase());
    });

    useEffect(()=>{
        // Xu ly khi nhap value
        if(searchValue.length > 0){
            setSearchResult(filteredProducts)
        }
        setLoading(false)
    },[searchValue])

    useEffect(() => {
        if (productSelected !== null) {
            // Nếu productSelected không null, cập nhật giá trị priceValue
        }
    }, [productSelected]);

    const onPressProductSearch = (item) => {
        //Xu ly khi bam chon item da search
        setSearchValue('')
        setTotalPrice(item.price)
        setProductSelected(item)
        setModalVisible(!modalVisible)
    };

    function handleChangeQuantity(text) {
        setQuantity(text)
        if(productSelected!=null){
            setTotalPrice(text * productSelected.price)
        }else{
            setTotalPrice(0)
        }
    }

    function handleAddProductSelected() {
        productSelected.quantity = quantity
        productSelected.totalPrice = totalPrice
        setTotalPriceCoupon(totalPriceCoupon + totalPrice)
        setTotalQuantityCoupon(parseInt(totalQuantityCoupon) + parseInt(quantity))
        setListProductSelected([...listProductSelected, productSelected])
        setModalVisible(!modalVisible)
    }
    let createImportCoupon = async () => {
        try {
            const response = await axios.post(`${Enviroment.URL_SERVER_API}/importcoupons/add?importer=${'Admin'}&totalAmount=${totalPriceCoupon}&quantity=${totalQuantityCoupon}&supplierId=1`);
            return response.data;
        } catch (error) {
            return 'Error';
        }
    };

    let createDetailImportCoupon = async (importCouponId, productId, importQuantity, importPrice) => {
        try {
            const response = await axios.post(`${Enviroment.URL_SERVER_API}/detailimportcoupons/add?importCouponId=${importCouponId}&productId=${productId}&importQuantity=${importQuantity}&importPrice=${importPrice}`);
            return response.data;
        } catch (error) {
            console.log('error: '+error)
            return 'Error';
        }
    };

    let importQuantityProduct = async (productId, quantity) => {
        try {
            const response = await axios.put(`${Enviroment.URL_SERVER_API}/products/import?idProduct=${productId}&quantity=${quantity}`);
            return response.data;
        } catch (error) {
            console.log('error: '+error)
            return 'Error';
        }
    };
     function handleImportProduct() {
         if(totalQuantityCoupon > 0){
             let idCoupon = createImportCoupon().then((id) => {
                 console.log("kq: ", id);
                 listProductSelected.forEach((product) => {
                     console.log("sản phẩm: " + product.id)
                     createDetailImportCoupon(id, product.id, product.quantity, product.totalPrice).then(r => {console.log(r)})
                     importQuantityProduct(product.id, product.quantity).then(r => {console.log(r)})
                 });
                 Alert.alert('Thông báo', 'Đã nhập hàng thành công', [
                     {text: 'OK', onPress: () => {resetImportCoupon()}},
                 ]);
             }).catch((error) => {
                 console.error("Error: ", error);
             });
         }else{
             Alert.alert('Thông báo', 'Vui lòng thêm nội dung nhập hàng', [
                 {text: 'OK', onPress: () => {}},
             ]);
         }
    }
    const resetImportCoupon = () => {
         setTotalQuantityCoupon(0)
         setTotalPriceCoupon(0)
         setListProductSelected([])
         setQuantity(0)
    }
    const renderProductSearch = ({ item, index }) => (
        <TouchableHighlight underlayColor="rgba(84, 23, 137, 0.8)" onPress={() => onPressProductSearch(item)}>
            <View style={styles.container}>
                <View style={{flexDirection: 'row'}}>
                    {item?.photoURL?.includes("http") ? (
                        <Image style={styles.photo} source={{ uri: item.photoURL }} />
                    ) : (
                        <Image
                            style={styles.photo}
                            source={{
                                uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
                            }}
                        />
                    )}
                    <View style={styles.text}>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                        <Text style={styles.id}>Mã: {item.id}</Text>
                        <Text>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                    </View>
                </View>
                <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 20}}>
                    <Text></Text>
                    <Text>Trong kho: {item.quantity}</Text>
                </View>
            </View>
        </TouchableHighlight>
    );
    const renderProduct = ({ item, index }) => (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                {item?.photoURL?.includes("http") ? (
                    <Image style={styles.photo} source={{ uri: item.photoURL }} />
                ) : (
                    <Image
                        style={styles.photo}
                        source={{
                            uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
                        }}
                    />
                )}
                <View style={styles.text}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                    <Text style={styles.id}>Mã: {item.id}</Text>
                    <Text style={styles.id}>SL: {item.quantity}</Text>
                </View>
            </View>
            <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                <Text style={{fontSize: 13}}>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                <Text>{numeral(item.totalPrice).format('0,0').replace(/,/g, '.')}đ</Text>
                <EvilIcons name="trash" size={24} color="red"/>
            </View>
        </View>
    );
    const renderImage = ({ item }) => (
        <TouchableHighlight>
            <View style={{ backgroundColor: "white" }}>
                <Image
                    style={{ width: "100%", height: 260, resizeMode: "contain" }}
                    source={{ uri: item }}
                />
            </View>
        </TouchableHighlight>
    );
    return (
        <View>
            <View style={{borderWidth: 0.5, borderColor: '#a2a2a2', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white'}}>
                <View style={styles.searchContainer}>
                    <Image style={styles.searchIcon} source={require("../../../../../assets/icons/search.png")} />
                    <TextInput
                        style={styles.searchInput}
                        onChangeText={handleSearch}
                        placeholder='Chọn hàng cần nhập'
                        value={searchValue}
                    />
                    <Pressable onPress={() => handleSearch("")}>
                        {loading && <FontAwesomeIcon name="spinner"/>}
                        {searchValue.length > 0 && !loading && <Image style={styles.searchIcon} source={require("../../../../../assets/icons/close.png")}/>}
                    </Pressable>
                    <View style={{backgroundColor: 'white',position: 'absolute', top: 50, zIndex: 999, left: 0, right: 0, shadowColor: '#000', shadowOffset: {width: 1, height: 5,}, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5,borderRadius: 10, width: width*(90/100)}}>
                        {searchResult.length > 0 && searchValue.length > 0 && <FlatList
                            data={searchResult}
                            renderItem={renderProductSearch}
                            numColumns={1}
                            keyExtractor={(item) => item.id}
                        />}
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            setModalVisible(!modalVisible);
                        }}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                {productSelected?.photoURL?.includes("http") ? (
                                    <Image style={{width: width, height: height*(50/100), resizeMode: 'contain'}} source={{ uri: productSelected?.photoURL }} />
                                ) : (
                                    <Image
                                        style={{width: width, height: height*(50/100), resizeMode: 'contain'}}
                                        source={{
                                            uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${productSelected?.photoURL}`,
                                        }}
                                    />
                                )}
                                <View style={styles.box}>
                                    <View style={{display: 'flex', flexDirection: 'row', margin: 10}}>
                                        <View style={{display: 'flex', flexBasis: '25%'}}>
                                            <Text style={styles.row}>Số lượng</Text>
                                            <Text style={styles.row}>Đơn giá</Text>
                                            <Text style={styles.row}>Giảm giá</Text>
                                            <Text style={styles.row}>Tổng tiền</Text>
                                        </View>
                                        <View style={{display: 'flex', flexBasis: '70%'}}>
                                            <TextInput onChangeText={handleChangeQuantity} placeholder="1" keyboardType="numeric" style={[styles.row, styles.underText]}/>
                                            <Text style={[styles.row, styles.underText]}>{numeral(productSelected?.price).format('0,0').replace(/,/g, '.')}</Text>
                                            <TextInput placeholder='0%' keyboardType="numeric" style={[styles.row, styles.underText]}/>
                                            <Text placeholder='0' style={[styles.row, styles.underText]}>{numeral(totalPrice).format('0,0').replace(/,/g, '.')}đ</Text>
                                        </View>
                                    </View>
                                </View>

                                <Pressable
                                    style={[{width: width*(85/100), padding: 10, borderRadius: 10}, styles.buttonClose]}
                                    onPress={handleAddProductSelected}>
                                    <Text style={styles.textStyle}>Thêm</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                </View>

            </View>

            <View style={{paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15,zIndex: -1, display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text>Nội dung nhập hàng</Text>
                <Text>Người lập: <Text style={{color: '#2196F3'}}>Admin</Text></Text>
            </View>

            <ScrollView style={{backgroundColor: '#fff', minHeight: height*(70/100), zIndex: -1}}>
                <View style={{paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15}}>
                    {listProductSelected.length === 0 && <View style={{display: 'flex', justifyContent: 'center',  alignItems: 'center', marginTop: height*(15/100)}}>
                        <Image  source={require('../../../../../assets/coupon-icon.png')} style={{width: width*(30/100),margin: 10, height: width*(30/100), resizeMode: 'contain'}} />
                        <Text>Chưa có hàng hóa nào trong phiếu nhập</Text>
                    </View>}
                    {listProductSelected.length > 0 && <FlatList
                        data={listProductSelected}
                        renderItem={renderProduct}
                        numColumns={1}
                        keyExtractor={(item) => item.id}
                    />}
                </View>
            </ScrollView>

            <TouchableHighlight onPress={handleImportProduct}>
                <View style={{backgroundColor: '#2196F3', display: 'flex', flexDirection: 'row', justifyContent:'space-between', alignItems: 'center', padding: 16, margin:20, borderRadius: 10 }}>
                    <Text style={{color: 'white', fontSize: 16}}>Tổng SL: {totalQuantityCoupon} - {numeral(totalPriceCoupon).format('0,0').replace(/,/g, '.')}đ </Text>
                    <Text style={{color: 'white', fontSize: 16}}>Nhập hàng <AntDesign name="right" size={16} color="white" /></Text>
                </View>
            </TouchableHighlight>
        </View>
    )
}


const styles = StyleSheet.create({

    searchContainer: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#EDEDED",
        borderRadius: 10,
        width: width*(90/100),
        justifyContent: "space-around"
    },
    searchIcon: {
        width: 20,
        height: 20,
        tintColor: 'grey'
    },
    searchInput: {
        backgroundColor: "#EDEDED",
        color: "black",
        width:  width*(65/100),
        height: 50,
    },
    box: {
        margin: 10
    },
    row:{
        lineHeight: 40,
        height: 40,
        textAlign: 'left',
        fontSize: 15,
        maxHeight: 40,
        marginTop: 10,
    },
    underText:{
        borderWidth: 1,
        borderTopColor: '#fff',
        borderLeftColor: '#fff',
        borderRightColor: '#fff',
        borderBottomColor: '#b2b2b2'
    },modalView: {
        backgroundColor: 'white',
        borderRadius: 20,
        alignItems: "center",
        shadowColor: '#000',
        shadowOffset: {
            width: 1,
            height: 5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: width*(100/100),
        paddingBottom: height*(38/100)
    },
    buttonOpen: {
        backgroundColor: '#F194FF',
    },
    buttonClose: {
        backgroundColor: '#2196F3',
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
    contentContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },centeredView: {
        flex: 1,
    },imageContainer: {
        flex: 1,
        justifyContent: 'center',
        width: width,
        height: height
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'flex-end',
        paddingVertical: 8,
        marginTop: 200,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 0
    },
    container: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between',
        paddingTop: 5,
        paddingBottom: 5
    },
    photo: {
        width: width*(15/100),
        height: width*(15/100),
        resizeMode: "contain",
        margin: 5
    },
    text:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    name:{
        color: '#000',
        fontSize: 15,
        width: width*(45/100)
    },
    id: {
        color: '#656565',
        fontSize: 13
    }
});