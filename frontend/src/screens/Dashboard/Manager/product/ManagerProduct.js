import React, {useEffect, useState} from "react";
import {View, Text} from "react-native";
// Import các màn hình
import ProductScreen from './ProductScreen';
import ImportProductScreen from './ImportProductScreen';
import ReturnProductScreen from './ReturnProductScreen';
import {Icon, TabView, Tab} from "@rneui/themed";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";

export default function ManagerProduct(props){
    const { navigation } = props;
    const [loading, setLoading] = useState(true);
    const [index, setIndex] = useState(0);
    const [products, setProducts] = useState([])
    let fetchDataProduct = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/products/all`);
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchDataProduct();
    }, [])

    const dataFakeImportCoupon = [
        {id: 1, date: '07/01/2024', price: 2000000, importer:'Pham A'},
        {id: 2, date: '08/01/2024',price: 1500000, importer:'Le B'},
        {id: 3, date: '08/01/2024', price: 3000000, importer:'Huynh C'},
        {id: 4, date: '09/01/2024', price: 4000000, importer:'Nguyen D'},
        {id: 5, date: '010/01/2024', price: 5000000, importer:'Tran G'},
        {id: 6, date: '08/01/2024',price: 1500000, importer:'Le B'},
        {id: 7, date: '08/01/2024', price: 3000000, importer:'Huynh C'},
        {id: 8, date: '09/01/2024', price: 4000000, importer:'Nguyen D'},
        {id: 9, date: '010/01/2024', price: 5000000, importer:'Tran G'},
        {id: 10, date: '08/01/2024',price: 1500000, importer:'Le B'},
        {id: 11, date: '08/01/2024', price: 3000000, importer:'Huynh C'},
        {id: 12, date: '09/01/2024', price: 4000000, importer:'Nguyen D'},
        {id: 13, date: '010/01/2024', price: 5000000, importer:'Tran G'},
    ]
    return (
        <>
            <TabView value={index} onChange={setIndex} animationType="spring">
                <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
                    <View><ProductScreen navigation={navigation} data={products}></ProductScreen></View>
                </TabView.Item>
                <TabView.Item style={{ width: "100%" }}>
                    <View><ImportProductScreen navigation={navigation} data={products}></ImportProductScreen></View>
                </TabView.Item>
                <TabView.Item style={{ width: "100%" }}>
                    <View><ReturnProductScreen navigation={navigation} ></ReturnProductScreen></View>
                </TabView.Item>
            </TabView>

            <Tab
                value={index}
                onChange={(e) => setIndex(e)}
                indicatorStyle={{
                    backgroundColor: "white",
                    height: 3,
                }}
                variant="primary"
            >
                <Tab.Item
                    title="Hàng hóa"
                    titleStyle={{ fontSize: 12 }}
                    icon={{ name: "timer", type: "ionicon", color: "white" }}
                />
                <Tab.Item
                    title="Nhập hàng"
                    titleStyle={{ fontSize: 12 }}
                    icon={{ name: "reader-outline", type: "ionicon", color: "white" }}
                />
                <Tab.Item
                    title="Trả hàng"
                    titleStyle={{ fontSize: 12 }}
                    icon={{
                        name: "body-outline",
                        type: "ionicon",
                        color: "white",
                    }}
                />
            </Tab>
        </>
    );
}
