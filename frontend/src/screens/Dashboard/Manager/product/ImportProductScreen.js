import {View, Text, StyleSheet, FlatList, TouchableOpacity, Dimensions, TouchableHighlight} from "react-native";
import React, {useEffect, useState} from "react";
import {ImportCouponItem} from "../../../../components/Product/ImportCouponItem";
import numeral from "numeral";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
const { width, height } = Dimensions.get('window');
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
export default function ImportProductScreen(props){
    const {navigation} = props;
    const products = props.data

    const [importCoupons, setImportCoupons] = useState([])
    const [totalPriceImport, setTotalPriceImport] = useState(0)
    let fetchDataImportCoupon = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/importcoupons/all`);
            setImportCoupons(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    useEffect(() => {
        fetchDataImportCoupon().then(() => {
        });
    }, [])

    useEffect(() => {
        let totalPrice = 0;
        importCoupons.forEach((importCoupon) => {
            totalPrice += importCoupon.totalAmount;
        });
        setTotalPriceImport(totalPrice);
    }, [importCoupons])

    const detailCoupon = (item) => {
        navigation.navigate("DetailImportCoupon", {item})
    }
    const renderItem = ({ item, index }) => (
        <TouchableOpacity onPress={() => detailCoupon(item)}>
            <ImportCouponItem key={item.id} item={item} index={index} />
        </TouchableOpacity>
    );
    const handleAddItem = () => {
        navigation.navigate("CreateImportCoupon", { products });
    };
    return (
        <>
            <View>
                <View style={{borderWidth: 0.5, borderColor: '#a2a2a2', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white'}}>
                    <Text><AntDesign name="calendar" size={16} color="black" /> Tháng này <AntDesign name="down" size={10} color="black" /></Text>
                </View>
                <View style={{display: 'flex',flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white'}}>
                    <Text><Text style={{color: '#006fff'}}>{importCoupons.length}</Text> phiếu</Text>
                    <Text>Tổng tiền nhập hàng: <Text style={{color: '#006fff'}}>{numeral(totalPriceImport).format('0,0').replace(/,/g, '.')}đ</Text></Text>
                </View>
                <View style={styles.importBox}>
                    <View>
                        <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{margin: 8}}>Tất cả</Text>
                            <TouchableOpacity onPress={() => fetchDataImportCoupon()}>
                                <Ionicons style={{marginRight: 20, padding: 8, backgroundColor:'#c9c9c9', marginBottom: 10, borderRadius: 10}} name="reload" size={16} color="black" />
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={importCoupons}
                            renderItem={renderItem}
                            numColumns={1}
                            keyExtractor={(item) => item.id}
                        />
                    </View>
                </View>
            </View>
            <View style={{ position: 'absolute',top: height*(75/100),bottom: 80,  right: width*(10/100)}}>
                <TouchableOpacity style={{backgroundColor: '#3f94e8',width: 50,height: 50,borderRadius: 30, display: 'flex',justifyContent: 'center',alignItems: 'center',elevation: 3,}} onPress={handleAddItem}>
                    <Text style={{textAlign: 'center'}}><FontAwesomeIcon name="plus" size={25} color="white" /></Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    importBox: {
        marginTop: 10
    }
})