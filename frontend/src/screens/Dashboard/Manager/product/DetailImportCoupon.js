import {Dimensions, FlatList, Image, StyleSheet, Text, View} from "react-native";
import numeral from "numeral";
import React, {useEffect, useState} from "react";
import axios from "axios";
import Enviroment from "../../../../../env/Enviroment";
const { width, height } = Dimensions.get('window');
import {EvilIcons} from "@expo/vector-icons";
import Carousel, {Pagination} from "react-native-snap-carousel";

export default function DetailImportCouponScreen(props){
    const { navigation, route } = props;
    const item = route.params?.item;
    const date = new Date(item.dateTime);
    const [coupons, setCoupons] = useState([])
    const [products, setProducts] = useState([])

    let fetchCoupons = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/detailimportcoupons/getDetailByCouponId?importCouponId=${item.id}`);
            setCoupons(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };
    let fetchProductsOfImportCoupon = async () => {
        try {
            const response = await axios.get(`${Enviroment.URL_SERVER_API}/detailimportcoupons/getProductByCouponId?importCouponId=${item.id}`);
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchProductsOfImportCoupon();
    }, [])

    useEffect(() => {
        fetchCoupons();
    }, [])
    const renderQuantityItem = (id) =>{
        let quantity = 0
        coupons.map((item) =>{
            if(item.productId === id){
                quantity = item.importQuantity
            }
        })
        return quantity
    }
    const renderTotalPriceItem = (id) =>{
        let quantity = 0
        coupons.map((item) =>{
            if(item.productId === id){
                quantity = item.importPrice
            }
        })
        return quantity
    }
    const renderProduct = ({ item, index }) => (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <Image style={styles.photo} source={{ uri: item.photoURL}} />
                <View style={styles.text}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                    <Text style={styles.id}>Mã: {item.id}</Text>
                    <Text style={styles.id}>SL: {renderQuantityItem(item.id)}</Text>
                </View>
            </View>
            <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                <Text style={{fontSize: 13}}>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                <Text style={{fontSize: 13}}>Thành tiền: {numeral(renderTotalPriceItem(item.id)).format('0,0').replace(/,/g, '.')}đ</Text>
            </View>
        </View>
    );
    const formattedDateTime =
        `${date.getDate().toString().padStart(2, '0')}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getFullYear()} ` +
        `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    return (
        <View>
            <View style={{display: 'flex',flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white', borderWidth:0.5, borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0}}>
                <Text>Mã phiếu: <Text style={{color: '#006fff'}}>{item.id}</Text></Text>
                <Text>Người lập: <Text style={{color: '#006fff'}}>{item.importer}</Text></Text>
            </View>
            <View style={{display: 'flex',flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white',}}>
                <Text>Số lượng: <Text style={{color: '#006fff'}}>{item.quantity}</Text></Text>
                <Text>Tổng: <Text style={{color: '#006fff'}}>{numeral(item.totalAmount).format('0,0').replace(/,/g, '.')}đ</Text></Text>
            </View>
            <Text style={{margin: 8}}>Nội dung nhập hàng ngày: {formattedDateTime}</Text>
            <View style={{backgroundColor: 'white'}}>
                <FlatList
                    data={products}
                    renderItem={renderProduct}
                    numColumns={1}
                    keyExtractor={(item) => item.id}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between',
        paddingTop:10,
        paddingBottom: 10
    },
    photo: {
        width: width*(15/100),
        height: width*(15/100),
        resizeMode: "contain",
        margin: 5
    },
    text:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    name:{
        color: '#000',
        fontSize: 15,
        width: width*(45/100)
    },
    id: {
        color: '#656565',
        fontSize: 13
    }
})