import React, { useState, useLayoutEffect } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";
import BackButton from "../../../components/BackButton/BackButton";
import { ButtonGroup } from "@rneui/themed";
import Enviroment from "../../../../env/Enviroment";

let SignUpScreen;
export default SignUpScreen = (props) => {
  console.log(props);
  const { navigation, route } = props;

  const item = route.params?.item;

  const [email, setEmail] = useState(item.email);
  const [name, setName] = useState(item.name);
  const [phone, setPhone] = useState(item.phone);
  const [address, setAddress] = useState(item.address);
  const [selectedIndex, setSelectedIndex] = useState(item.status);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTransparent: "true",
      headerLeft: () => (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      headerRight: () => <View />,
    });
  }, []);

  const update = (item) => {
    var error = false;
    const numberRegex = /^[0-9]+$/;

    // if (!email.includes("@")) {
    //   ToastAndroid.show("Email không đúng định dạng !", ToastAndroid.SHORT);
    //   error = true;
    // } else
    if (phone != undefined) {
      if (phone.length < 10 || !numberRegex.test(phone)) {
        ToastAndroid.show(
          "Số điện thoại phải từ 10 kí tự và phải là số !",
          ToastAndroid.SHORT
        );
        error = true;
      }
    }

    if (!error) {
      item.name = name;
      item.email = email;
      item.phone = phone;
      item.address = address;
      item.status = selectedIndex;
      fetch(`${Enviroment.URL_SERVER_API}/user/update/` + item.id, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          password: item.password,
          name: item.name,
          email: item.email,
          address: item.address,
          phone: item.phone,
          role: item.role,
          status: item.status,
        }),
      });
      ToastAndroid.show("Chỉnh sửa thành công !", ToastAndroid.SHORT);
      navigation.navigate("ManagerUser", item);
    }
  };

  return (
    <View style={styles.container}>
      {/* <Image
        source={{ uri: "https://www.bootdey.com/image/280x280/00BFFF/000000" }}
        style={styles.background}
      /> */}
      <View style={styles.logoContainer}>
        {/* <Image
          source={{
            uri: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
          }}
          style={styles.logo}
        /> */}
      </View>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Chỉnh sửa thông tin</Text>
        <View style={styles.card}>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>
              User Name:{" "}
              <Text style={{ fontWeight: "bold" }}>{item.userName}</Text>
            </Text>
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>Name</Text>
            <TextInput
              style={styles.input}
              value={name}
              onChangeText={(name) => {
                setName(name);
              }}
              placeholder="Name"
              placeholderTextColor="#999"
            />
          </View>
          {/* <View style={styles.inputContainer}>
            <Text style={styles.label}>Email</Text>
            <TextInput
              style={styles.input}
              value={email}
              onChangeText={(email) => {
                setEmail(email);
              }}
              placeholder="Email"
              placeholderTextColor="#999"
            />
          </View> */}
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Số Điện Thoại</Text>
            <TextInput
              style={styles.input}
              value={phone}
              onChangeText={(phone) => {
                setPhone(phone);
              }}
              placeholder="Số Điện Thoại"
              placeholderTextColor="#999"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Địa chỉ</Text>
            <TextInput
              style={styles.input}
              value={address}
              onChangeText={(address) => setAddress(address)}
              placeholder="Địa Chỉ"
              placeholderTextColor="#999"
            />
          </View>

          <ButtonGroup
            buttons={["Khóa", "Hoạt Động"]}
            selectedIndex={selectedIndex}
            onPress={(value) => {
              setSelectedIndex(value);
            }}
            containerStyle={{ marginBottom: 20 }}
          />
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                update(item);
              }}
            >
              <Text style={styles.buttonText}>Lưu lại</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Quay về</Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#B0E0E6",
  },
  background: {
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  logoContainer: {
    alignItems: "center",
    marginTop: 120,
  },
  logo: {
    width: 120,
    height: 120,
    borderRadius: 60,
    resizeMode: "contain",
  },

  formContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    color: "#fff",
    marginBottom: 20,
    marginTop: 20,
    fontWeight: "bold",
  },
  card: {
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 4,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    padding: 20,
    marginBottom: 20,
  },
  inputContainer: {
    marginBottom: 20,
  },
  label: {
    fontSize: 16,
    color: "#333",
  },
  input: {
    height: 40,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#ddd",
    color: "#333",
    paddingLeft: 10,
  },
  button: {
    width: "100%",
    height: 40,
    backgroundColor: "#333",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
  },
};
