import React, { useState, useEffect, useLayoutEffect } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Alert,
  Image,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
} from "react-native";

import { Icon } from "@rneui/themed";
import { deleteCategories, getCategories } from "../../../../data/dataArrays";
import styles from "../styles";

export default ManagerCategory = (props) => {
  const [message, setMessage] = useState('');
  const [categories, setCategories] = useState([]);
  const [query, setQuery] = useState("");

  const [isLoading, setLoading] = useState(true);

  const { navigation, route } = props;

  let showAlert;
  showAlert = () => Alert.alert("Alert", "Button pressed ");

  useEffect(() => {
    async function fetchData() {
      const data = await getCategories();
      if (data.length > 0)
        setLoading(false)
      setCategories(data);
    }

    fetchData();
  }, []);

  if (isLoading) {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  const onPressEdit = (item) => {
    navigation.navigate("UpdateCategory", { item });
  };

  const onPressDelete = (item) => {
    Alert.alert(
      'Xác nhận xóa',
      'Bạn có chắc chắn muốn xóa?',
      [
        {
          text: 'Hủy',
          style: 'cancel',
        },
        {
          text: 'Xóa',
          onPress: () => handleDeleteConfirmed(item),
        },
      ],
      { cancelable: true }
    );
  };

  const handleDeleteConfirmed = async (item) => {
    const data = await deleteCategories(item);
    if (data.status === 'OK') {
      setMessage(data.message);
  } else {
    setMessage('Không thể xóa danh mục');
  }
    setTimeout(() => {
      setMessage('');
    }, 1000)
  };
  const onPressAdd = () => {
    navigation.navigate("AddCategory");
  }
  const item = route.params?.item;
  console.log("Item:" + item);
  if (item != undefined) {
    const newData = users.map((obj) => {
      if (item.id === obj.id) {
        return item;
      }
      return obj;
    });
    setUsers(newData);
  }

  return (
    <View style={styles.container}>
      <View style={styles.formContent}>
        <View style={styles.inputContainer}>
          <Image
            style={[styles.icon, styles.inputIcon]}
            source={{
              uri: "https://img.icons8.com/color/70/000000/search.png",
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Tìm kiếm..."
            underlineColorAndroid="transparent"
            onChangeText={(query) => {
              setQuery(query);
            }}
          />
        </View>
      </View>

      <FlatList
        enableEmptySections={true}
        // data={categories}
        data={
          categories == {}
            ? {}
            : categories.filter(
                (obj) =>
                  obj.name.toLowerCase().includes(query.toLowerCase())
              )
        }
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <View style={styles.box}>
              <View>
                <Text style={styles.textId}>{item.id}</Text>
              </View>
              <View style={styles.boxContent}>
                <Text style={styles.title}>{item.name}</Text>
                <Text style={styles.description}>
                  {item.url == "" ? "" : item.url}
                </Text>
                <View style={styles.buttons}>
                  <TouchableOpacity
                    style={[styles.button, styles.profile]}
                    onPress={() => onPressEdit(item)}
                  >
                    <Icon name="pencil" type="font-awesome" color="white" />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[styles.button, styles.danger]}
                    onPress={() => onPressDelete(item)}
                  >
                    <Icon name="trash" type="font-awesome" color="white" />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          );
        }}
      />
      <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', margin: 16 }}>
        <TouchableOpacity
          style={{
            backgroundColor: '#3498db',
            width: 50,
            height: 50,
            borderRadius: 25,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 8,
          }}
          onPress={() => {
            onPressAdd()
          }}
        >
          <Text style={{ fontSize: 30, color: "#fff" }}>+</Text>
        </TouchableOpacity>
      </View>
      {message !== '' && (
        <Text style={{ color: 'green' }}>{message}</Text>
      )}
    </View>
  );
};
