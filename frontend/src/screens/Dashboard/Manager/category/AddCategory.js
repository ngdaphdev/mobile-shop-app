import React, { useLayoutEffect, useState } from 'react';
import { View, TextInput, Button, Alert, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { createCategories, uploadImage } from '../../../../data/dataArrays';
import { Card, Dialog, Icon, Image, Text } from '@rneui/themed';
import * as ImagePicker from 'expo-image-picker';
import styles from "./styles";

const AddCategory = (props) => {
    const { navigation } = props;
    const [category, setCategory] = useState({
        name: null,
        url: null,
    });
    const [visible, setVisible] = useState(false);
    const [selectedImage, setSelectedImage] = useState(null);

    const openImagePicker = async () => {
        // No permissions request is necessary for launching the image library
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.canceled) {
            setSelectedImage(result.assets[0].uri);
            setCategory({ ...category, url: result.assets[0].uri });
            setVisible(false)
        }
    };

    const handleCameraLaunch = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.canceled) {
            setSelectedImage(result.assets[0].uri);
            setCategory({ ...category, url: result.assets[0].uri });
            setVisible(false)
        }
    }

    const handleCreateCategory = async () => {
        // Perform validation
        if (!category.name || !selectedImage) {
            Alert.alert('Error', 'Vui lòng nhập đầy đủ thông tin!');
            return;
        }
        else {
            var filename = selectedImage.substring(selectedImage.lastIndexOf('/') + 1, selectedImage.length);
            // setCategory({ ...category, url: filename })
            category.url = filename

            const data = await createCategories(category);
            if (data.status === 'OK') {
                try {
                    const formData = new FormData()
                    formData.append("image", {
                        uri: selectedImage,
                        name: `${filename}`,
                        type: 'image/png'
                    })

                    const response = uploadImage(formData)
                    if (response.status === 200) {
                        navigation.navigate('ManagerCategory');
                    }

                } catch (error) {
                    console.error('Error creating category:', error);
                    Alert.alert('Error', 'Có lỗi xảy ra khi tạo danh mục!');
                }
            } else {
                Alert.alert('Error', 'Không thể tạo danh mục!');
            }

            setCategory({
                name: '',
                url: '',
            });
        }
    };

    const toggleDialog = () => {
        setVisible(!visible);
    };

    return (
        <View style={styles.container}>
            {/* Name Input */}
            <TextInput
                placeholder="Category Name"
                value={category.name}
                onChangeText={(text) => setCategory({ ...category, name: text })}
                style={styles.input}
            />
            {/* URL Input */}
            {(category.name || selectedImage) && (
                <Card containerStyle={{}} wrapperStyle={{}}>
                    <Card.Title>Thông tin danh mục</Card.Title>
                    <Card.Divider />
                    <View
                    >
                        <Image
                            style={{ width: "100%", height: 100 }}
                            resizeMode="contain"
                            source={{ uri: `${selectedImage}` }}
                        />
                        <Text>Tên danh mục: {category.name}</Text>
                    </View>
                </Card>
            )}

            <Dialog
                isVisible={visible}
                onBackdropPress={toggleDialog}
            >
                <Dialog.Title title="Chọn ảnh từ" />
                <Dialog.Actions>
                    <Dialog.Button title="Galery" onPress={openImagePicker}>
                        <View style={styles.dialogBorder}>
                            <Icon name="image" type="font-awesome" color='#FFC47E' />
                            <Text style={{ color: '#666666' }}>Galery</Text>
                        </View>
                    </Dialog.Button>
                    <Dialog.Button title="Camera" onPress={handleCameraLaunch} >
                        <View style={styles.dialogBorder}>
                            <Icon name="camera" type="font-awesome" color='#FFC47E' />
                            <Text style={{ color: '#666666' }}>Camera</Text>
                        </View>
                    </Dialog.Button>
                </Dialog.Actions>
            </Dialog>

            <View style={{ margin: 20 }}>
                <Button
                    title="Chọn ảnh"
                    onPress={toggleDialog}
                />
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={handleCreateCategory}>
                    <Text style={styles.buttonText}>Save</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default AddCategory;