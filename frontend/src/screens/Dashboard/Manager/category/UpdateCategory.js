import React, { useState, useLayoutEffect } from "react";
import { View, TouchableOpacity, ToastAndroid, TextInput } from "react-native";
import BackButton from "../../../../components/BackButton/BackButton";
import { updateCategories, uploadImage } from "../../../../data/dataArrays";
import { Card, Dialog, Icon, Image, Text } from '@rneui/themed';
import Enviroment from "../../../../../env/Enviroment";
import styles from "./styles";
import * as ImagePicker from 'expo-image-picker';

export default UpdateCategory = (props) => {
  const { navigation, route } = props;
  const item = route.params?.item;
  const [visible, setVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  const [category, setCategory] = useState({
    name: item.name,  // Set initial values based on the item
    url: item.url,
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTransparent: true,
      headerLeft: () => (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      headerRight: () => <View />,
    });
  }, []);

  const handleUpdate = async () => {
    // Perform validation
    if (!category.name || !selectedImage) {
      Alert.alert('Error', 'Vui lòng nhập đầy đủ thông tin!');
      return;
    }
    else {
      var filename = selectedImage.substring(selectedImage.lastIndexOf('/') + 1, selectedImage.length);
      // setCategory({ ...category, url: filename })
      category.url = filename

      const updatedItem = { ...item, name: category.name, url: filename };

      const data = await updateCategories(updatedItem);
      console.log(data);
      if (data.status === 'OK') {
        try {
          const formData = new FormData()
          formData.append("image", {
            uri: selectedImage,
            name: `${filename}`,
            type: 'image/png'
          })

          const response = uploadImage(formData)
          if (response.status === 200) {
            ToastAndroid.show(data.message, ToastAndroid.SHORT);
            navigation.navigate("ManagerCategory");
          }
        } catch (error) {
          console.error('Error creating category:', error);
          Alert.alert('Error', 'Có lỗi xảy ra khi tạo danh mục!');
        }
      } else {
        Alert.alert('Error', 'Không thể tạo danh mục!');
      }

      setCategory({
        name: '',
        url: '',
      });
    }
  };

  const toggleDialog = () => {
    setVisible(!visible);
  };

  const openImagePicker = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.canceled) {
      setSelectedImage(result.assets[0].uri);
      setCategory({ ...category, url: result.assets[0].uri });
      setVisible(false)
    }
  };

  const handleCameraLaunch = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.canceled) {
      setSelectedImage(result.assets[0].uri);
      setCategory({ ...category, url: result.assets[0].uri });
      setVisible(false)
    }
  }
  return (
    <View style={styles.container}>
      <Text style={styles.title}></Text>
      {/* Name Input */}
      <TextInput
        placeholder="Tên danh mục"
        value={category.name}
        onChangeText={(text) => setCategory({ ...category, name: text })}
        style={styles.input}
      />
      {/* URL Input */}
      {(category.name || selectedImage || category.url) && (
        <Card containerStyle={{}} wrapperStyle={{}}>
          <Card.Title>Thông tin danh mục</Card.Title>
          <Card.Divider />
          <View
          >
            <Image
              style={{ width: "100%", height: 100 }}
              resizeMode="contain"
              source={{ uri: !selectedImage ? `${Enviroment.URL_SERVER_API}/images/products/logo/${category.url}` : selectedImage }}
            />
            <Text>Tên danh mục: {category.name}</Text>
          </View>
        </Card>
      )}

      <Dialog
        isVisible={visible}
        onBackdropPress={toggleDialog}
      >
        <Dialog.Title title="Chọn ảnh từ" />
        <Dialog.Actions>
          <Dialog.Button title="Galery" onPress={openImagePicker}>
            <View style={styles.dialogBorder}>
              <Icon name="image" type="font-awesome" color='#FFC47E' />
              <Text style={{ color: '#666666' }}>Galery</Text>
            </View>
          </Dialog.Button>
          <Dialog.Button title="Camera" onPress={handleCameraLaunch} >
            <View style={styles.dialogBorder}>
              <Icon name="camera" type="font-awesome" color='#FFC47E' />
              <Text style={{ color: '#666666' }}>Camera</Text>
            </View>
          </Dialog.Button>
        </Dialog.Actions>
      </Dialog>

      <View style={{ margin: 20 }}>
        <TouchableOpacity
          style={styles.button}
          onPress={toggleDialog}
        >
          <Text style={styles.buttonText}>Chọn ảnh</Text>
        </TouchableOpacity>

      </View>
      <View>
        <TouchableOpacity
          style={styles.button}
          onPress={handleUpdate}
        >
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
