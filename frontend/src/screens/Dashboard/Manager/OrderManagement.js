// OrderManagement.js
import React, {useState, useEffect} from "react";
import {ScrollView, Button, Text, View, StyleSheet, Image, TextInput, Modal, TouchableOpacity} from "react-native";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import {CheckBox, Card} from 'react-native-elements';
import axios from "axios";
import Enviroment from "../../../../env/Enviroment";
import CartScreen from "../../Cart/CartScreen";

const Tab = createMaterialTopTabNavigator();

const OrderManagement = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Chờ xác nhận" component={UnconfirmedOrdersScreen}/>
            <Tab.Screen name="Đang giao" component={ConfirmedOrdersScreen}/>
            <Tab.Screen name="Đã giao" component={DaGiaoOrdersScreen}/>
            <Tab.Screen name="Đã nhận" component={DaNhanOrdersScreen}/>
            <Tab.Screen name="Đã hủy" component={CancelOrdersScreen}/>
        </Tab.Navigator>

    );
};

const UnconfirmedOrdersScreen = () => {
    const [unconfirmedOrders, setUnconfirmedOrders] = useState([]);
    const [listOrderDetails, seListOrderDetails] = useState([]);
    // State để quản lý việc hiển thị Modal và thông tin chi tiết đơn hàng
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);

    useEffect(() => {
        // Load unconfirmed orders from the Spring Boot application
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/unconfirmed")
            .then(response => setUnconfirmedOrders(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    }, []);


    const handleConfirmOrder = async (orderId, status) => {
        await axios.put(Enviroment.URL_SERVER_API + `/api/orders/updateStatus/${orderId}`, {status: status})
            .then(response => {
                console.log("Order confirmed successfully:", response.data);
                handleReloadOrders();
            })
            .catch(error => console.error("Error confirming order:", error));

    };

    const handleCancelOrder = async (orderId, status) => {
        await axios.put(Enviroment.URL_SERVER_API + `/api/orders/updateStatus/${orderId}`, {status: status})
            .then(response => {
                console.log("Order confirmed successfully:", response.data);
                handleReloadOrders();
            })
            .catch(error => console.error("Error confirming order:", error));
    };
    const handleReloadOrders = () => {
        // Reload unconfirmed orders after confirming
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/unconfirmed")
            .then(response => setUnconfirmedOrders(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    };
    const handleLoadListOrdersByOrderId = (orderId) => {
        console.log(orderId)
        axios.get(Enviroment.URL_SERVER_API + `/api/orderDetails/loadListOrders/${orderId}`)
            .then(response => seListOrderDetails(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    };

    // Function để mở Modal và set thông tin chi tiết đơn hàng được chọn
    const handleOpenModal = (order) => {
        setSelectedOrder(order);
        setModalVisible(true);
    };

    // Function để đóng Modal
    const handleCloseModal = () => {
        setModalVisible(false);
        setSelectedOrder(null);
    };

    return (
        <View>
            <ScrollView>
                {unconfirmedOrders.map(order => (
                    <TouchableOpacity
                        key={order.id}
                        onPress={() => {
                            handleOpenModal(order);
                            handleLoadListOrdersByOrderId(order.id);
                        }}
                    >
                        <Card key={order.id} style={styles.orderContainer}>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>ID Đơn Hàng :</Text>
                                <Text style={styles.value}>{order.id}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tên khách hàng :</Text>
                                <Text style={styles.value}>{order.buyerName}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>SDT :</Text>
                                <Text style={styles.value}>{order.phoneNumber}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Địa chỉ :</Text>
                                <Text style={styles.value}>{order.address}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Phương thức thanh toán :</Text>
                                <Text
                                    style={styles.value}>{order.paymentMethod === '0' ? 'Thanh toán khi nhận hàng' : 'Thanh toán qua VNPay'}</Text>

                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tình trạng :</Text>
                                <Text style={styles.value}>Chưa Thanh toán</Text>
                            </View>
                            {/* Buttons for each order */}
                            <View style={styles.buttonContainer}>
                                <Button title="Hủy" onPress={() => handleCancelOrder(order.id, -1)}
                                        buttonStyle={styles.cancelButton}/>
                                <Button title="Xác nhận" onPress={() => handleConfirmOrder(order.id, 1)}
                                        buttonStyle={styles.confirmButton}/>
                            </View>
                        </Card>
                    </TouchableOpacity>
                ))}
            </ScrollView>
            {/* Modal */}
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={handleCloseModal}
            >
                <View>
                    {/* Hiển thị thông tin chi tiết đơn hàng và danh sách sản phẩm ở đây */}
                    {/* Ví dụ: */}

                    <View>
                        <ScrollView>
                            {listOrderDetails.map((list) => (
                                <Card key={list.id}>
                                    {/*<Text>{list.id}</Text>*/}
                                    {/*<Text>{list.orderId}</Text>*/}
                                    {/*<Text>{list.idProduct}</Text>*/}
                                    {/*<Text>{list.quantity}</Text>*/}
                                    {/* Displaying product details */}
                                    <Image source={{uri: list.product.photoURL}} style={{width: 100, height: 100}}/>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10,
                                        marginTop: 10
                                    }}>
                                        <Text>{list.product.name}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>Giá:</Text>
                                        <Text> {list.product.price}đ</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>SL:</Text>
                                        <Text> {list.product.quantity}</Text>
                                    </View>
                                </Card>
                            ))}
                        </ScrollView>
                        {/* Button để đóng Modal */}
                        <TouchableOpacity onPress={handleCloseModal} style={{
                            backgroundColor: 'blue',
                            padding: 10,
                            borderRadius: 5,
                            marginTop: 10,
                        }}>
                            <Text style={{color: 'white', textAlign: 'center'}}>Đóng</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        </View>
    );
};

const ConfirmedOrdersScreen = () => {
    const [confirmedOrders, setConfirmedOrders] = useState([]);
    const [listOrderDetails, seListOrderDetails] = useState([]);
    // State để quản lý việc hiển thị Modal và thông tin chi tiết đơn hàng
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);
    useEffect(() => {
        // Load unconfirmed orders from the Spring Boot application
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/confirmed")
            .then(response => setConfirmedOrders(response.data))
            .catch(error => console.error("Error fetching confirmed orders:", error));
    }, []);
    const handleLoadListOrdersByOrderId = (orderId) => {
        console.log(orderId)
        axios.get(Enviroment.URL_SERVER_API + `/api/orderDetails/loadListOrders/${orderId}`)
            .then(response => seListOrderDetails(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    };

    // Function để mở Modal và set thông tin chi tiết đơn hàng được chọn
    const handleOpenModal = (order) => {
        setSelectedOrder(order);
        setModalVisible(true);
    };

    // Function để đóng Modal
    const handleCloseModal = () => {
        setModalVisible(false);
        setSelectedOrder(null);
    };

    return (
        <View>
            <ScrollView>
                {confirmedOrders.map(order => (
                    <TouchableOpacity
                        key={order.id}
                        onPress={() => {
                            handleOpenModal(order);
                            handleLoadListOrdersByOrderId(order.id);
                        }}
                    >
                        <Card key={order.id} style={styles.orderContainer}>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>ID Đơn Hàng :</Text>
                                <Text style={styles.value}>{order.id}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tên khách hàng :</Text>
                                <Text style={styles.value}>{order.buyerName}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>SDT :</Text>
                                <Text style={styles.value}>{order.phoneNumber}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Địa chỉ :</Text>
                                <Text style={styles.value}>{order.address}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Phương thức thanh toán :</Text>
                                <Text
                                    style={styles.value}>{order.paymentMethod === '0' ? 'Thanh toán khi nhận hàng' : 'Thanh toán qua VNPay'}</Text>

                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tình trạng :</Text>
                                <Text style={styles.value}>Chưa Thanh toán</Text>
                            </View>

                        </Card>
                    </TouchableOpacity>
                ))}
            </ScrollView>
            {/* Modal */}
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={handleCloseModal}
            >
                <View>
                    {/* Hiển thị thông tin chi tiết đơn hàng và danh sách sản phẩm ở đây */}
                    {/* Ví dụ: */}

                    <View>
                        <ScrollView>
                            {listOrderDetails.map((list) => (
                                <Card key={list.id}>
                                    {/*<Text>{list.id}</Text>*/}
                                    {/*<Text>{list.orderId}</Text>*/}
                                    {/*<Text>{list.idProduct}</Text>*/}
                                    {/*<Text>{list.quantity}</Text>*/}
                                    {/* Displaying product details */}
                                    <Image source={{uri: list.product.photoURL}} style={{width: 100, height: 100}}/>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10,
                                        marginTop: 10
                                    }}>
                                        <Text>{list.product.name}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>Giá:</Text>
                                        <Text> {list.product.price}đ</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>SL:</Text>
                                        <Text> {list.product.quantity}</Text>
                                    </View>
                                </Card>
                            ))}
                        </ScrollView>
                        {/* Button để đóng Modal */}
                        <TouchableOpacity onPress={handleCloseModal} style={{
                            backgroundColor: 'blue',
                            padding: 10,
                            borderRadius: 5,
                            marginTop: 10,
                        }}>
                            <Text style={{color: 'white', textAlign: 'center'}}>Đóng</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        </View>
    );
};

const DaGiaoOrdersScreen = () => {
    const [daGiaoOrders, setPaidOrders] = useState([]);

    useEffect(() => {
        // Load unconfirmed orders from the Spring Boot application
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/dagiao")
            .then(response => setPaidOrders(response.data))
            .catch(error => console.error("Error fetching paid orders:", error));
    }, []);
    return (
        <View style={styles.container}>
            {/* Render the list of unconfirmed orders */}
            {/*<Text>Chưa xác nhận</Text>*/}
            {/* Render the unconfirmedOrders list here */}
            {daGiaoOrders.map(order => (
                <Text key={order.id}>{order.id}</Text>
                // Replace "order.id" and "order.orderName" with your actual data properties
            ))}
        </View>
    );
};
const DaNhanOrdersScreen = () => {
    const [daNhanOrders, setDaNhanOrders] = useState([]);
    const [listOrderDetails, seListOrderDetails] = useState([]);
    // State để quản lý việc hiển thị Modal và thông tin chi tiết đơn hàng
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);
    useEffect(() => {
        // Load unconfirmed orders from the Spring Boot application
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/danhan")
            .then(response => setDaNhanOrders(response.data))
            .catch(error => console.error("Error fetching paid orders:", error));
    }, []);
    const handleLoadListOrdersByOrderId = (orderId) => {
        console.log(orderId)
        axios.get(Enviroment.URL_SERVER_API + `/api/orderDetails/loadListOrders/${orderId}`)
            .then(response => seListOrderDetails(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    };
    // Function để mở Modal và set thông tin chi tiết đơn hàng được chọn
    const handleOpenModal = (order) => {
        setSelectedOrder(order);
        setModalVisible(true);
    };

    // Function để đóng Modal
    const handleCloseModal = () => {
        setModalVisible(false);
        setSelectedOrder(null);
    };

    return (
        <View>
            <ScrollView>
                {daNhanOrders.map(order => (
                    <TouchableOpacity
                        key={order.id}
                        onPress={() => {
                            handleOpenModal(order);
                            handleLoadListOrdersByOrderId(order.id);
                        }}
                    >
                        <Card key={order.id} style={styles.orderContainer}>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>ID Đơn Hàng :</Text>
                                <Text style={styles.value}>{order.id}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tên khách hàng :</Text>
                                <Text style={styles.value}>{order.buyerName}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>SDT :</Text>
                                <Text style={styles.value}>{order.phoneNumber}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Địa chỉ :</Text>
                                <Text style={styles.value}>{order.address}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Phương thức thanh toán :</Text>
                                <Text
                                    style={styles.value}>{order.paymentMethod === '0' ? 'Thanh toán khi nhận hàng' : 'Thanh toán qua VNPay'}</Text>

                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tình trạng :</Text>
                                <Text style={styles.value}>Chưa Thanh toán</Text>
                            </View>

                        </Card>
                    </TouchableOpacity>
                ))}
            </ScrollView>
            {/* Modal */}
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={handleCloseModal}
            >
                <View>
                    {/* Hiển thị thông tin chi tiết đơn hàng và danh sách sản phẩm ở đây */}
                    {/* Ví dụ: */}

                    <View>
                        <ScrollView>
                            {listOrderDetails.map((list) => (
                                <Card key={list.id}>
                                    {/*<Text>{list.id}</Text>*/}
                                    {/*<Text>{list.orderId}</Text>*/}
                                    {/*<Text>{list.idProduct}</Text>*/}
                                    {/*<Text>{list.quantity}</Text>*/}
                                    {/* Displaying product details */}
                                    <Image source={{uri: list.product.photoURL}} style={{width: 100, height: 100}}/>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10,
                                        marginTop: 10
                                    }}>
                                        <Text>{list.product.name}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>Giá:</Text>
                                        <Text> {list.product.price}đ</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>SL:</Text>
                                        <Text> {list.product.quantity}</Text>
                                    </View>
                                </Card>
                            ))}
                        </ScrollView>
                        {/* Button để đóng Modal */}
                        <TouchableOpacity onPress={handleCloseModal} style={{
                            backgroundColor: 'blue',
                            padding: 10,
                            borderRadius: 5,
                            marginTop: 10,
                        }}>
                            <Text style={{color: 'white', textAlign: 'center'}}>Đóng</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        </View>
    );
};
const CancelOrdersScreen = () => {
    const [cancelOrders, setCancelOrders] = useState([]);
    const [listOrderDetails, seListOrderDetails] = useState([]);
    // State để quản lý việc hiển thị Modal và thông tin chi tiết đơn hàng
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedOrder, setSelectedOrder] = useState(null);
    useEffect(() => {
        // Load unconfirmed orders from the Spring Boot application
        axios.get(Enviroment.URL_SERVER_API + "/api/orders/dahuy")
            .then(response => setCancelOrders(response.data))
            .catch(error => console.error("Error fetching paid orders:", error));
    }, []);

    const handleLoadListOrdersByOrderId = (orderId) => {
        console.log(orderId)
        axios.get(Enviroment.URL_SERVER_API + `/api/orderDetails/loadListOrders/${orderId}`)
            .then(response => seListOrderDetails(response.data))
            .catch(error => console.error("Error fetching unconfirmed orders:", error));

    };
    // Function để mở Modal và set thông tin chi tiết đơn hàng được chọn
    const handleOpenModal = (order) => {
        setSelectedOrder(order);
        setModalVisible(true);
    };

    // Function để đóng Modal
    const handleCloseModal = () => {
        setModalVisible(false);
        setSelectedOrder(null);
    };

    return (
        <View>
            <ScrollView>
                {cancelOrders.map(order => (
                    <TouchableOpacity
                        key={order.id}
                        onPress={() => {
                            handleOpenModal(order);
                            handleLoadListOrdersByOrderId(order.id);
                        }}
                    >
                        <Card key={order.id} style={styles.orderContainer}>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>ID Đơn Hàng :</Text>
                                <Text style={styles.value}>{order.id}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tên khách hàng :</Text>
                                <Text style={styles.value}>{order.buyerName}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>SDT :</Text>
                                <Text style={styles.value}>{order.phoneNumber}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Địa chỉ :</Text>
                                <Text style={styles.value}>{order.address}</Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Phương thức thanh toán :</Text>
                                <Text
                                    style={styles.value}>{order.paymentMethod === '0' ? 'Thanh toán khi nhận hàng' : 'Thanh toán qua VNPay'}</Text>

                            </View>
                            <View style={styles.rowContainer}>
                                <Text style={styles.label}>Tình trạng :</Text>
                                <Text style={styles.value}>Chưa Thanh toán</Text>
                            </View>
                     
                        </Card>
                    </TouchableOpacity>
                ))}
            </ScrollView>
            {/* Modal */}
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={handleCloseModal}
            >
                <View>
                    {/* Hiển thị thông tin chi tiết đơn hàng và danh sách sản phẩm ở đây */}
                    {/* Ví dụ: */}

                    <View>
                        <ScrollView>
                            {listOrderDetails.map((list) => (
                                <Card key={list.id}>
                                    {/*<Text>{list.id}</Text>*/}
                                    {/*<Text>{list.orderId}</Text>*/}
                                    {/*<Text>{list.idProduct}</Text>*/}
                                    {/*<Text>{list.quantity}</Text>*/}
                                    {/* Displaying product details */}
                                    <Image source={{uri: list.product.photoURL}} style={{width: 100, height: 100}}/>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10,
                                        marginTop: 10
                                    }}>
                                        <Text>{list.product.name}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>Giá:</Text>
                                        <Text> {list.product.price}đ</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <Text>SL:</Text>
                                        <Text> {list.product.quantity}</Text>
                                    </View>
                                </Card>
                            ))}
                        </ScrollView>
                        {/* Button để đóng Modal */}
                        <TouchableOpacity onPress={handleCloseModal} style={{
                            backgroundColor: 'blue',
                            padding: 10,
                            borderRadius: 5,
                            marginTop: 10,
                        }}>
                            <Text style={{color: 'white', textAlign: 'center'}}>Đóng</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: 100,
        height: 100,
    },
    box: {
        padding: 20,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: "white",
        flexDirection: "row",
    },
    boxContent: {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start",
        marginLeft: 10,
    },
    title: {
        fontSize: 18,
        color: "#151515",
    },
    description: {
        fontSize: 15,
        color: "#646464",
    },
    buttons: {
        flexDirection: "row",
    },
    button: {
        height: 35,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        width: 50,
        marginRight: 5,
        marginTop: 5,
    },
    icon: {
        width: 20,
        height: 20,
    },
    view: {
        backgroundColor: "#eee",
    },
    profile: {
        backgroundColor: "#1E90FF",
    },
    danger: {
        backgroundColor: "rgba(214, 61, 57, 1)",
    },
    lock: {
        backgroundColor: "red",
    },

    lock_open: {
        backgroundColor: "#228B22",
    },
    // container: {
    //     flex: 1,
    //     backgroundColor: "white",
    // },
    formContent: {
        flexDirection: "row",
        // marginTop: 10,
    },
    inputContainer: {
        borderBottomColor: "#000",
        backgroundColor: "#FFFFFF",
        borderRadius: 30,
        borderWidth: 1,
        height: 45,
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        margin: 10,
    },
    iconBtnSearch: {
        alignSelf: "center",
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        flex: 1,
        borderColor: "#000",
    },
    inputIcon: {
        marginLeft: 15,
        justifyContent: "center",
    },

    textId: {
        justifyContent: "center",
        alignItems: "center",
        fontSize: 20,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10,
    },
    cardContainer: {
        // Your existing card styles
    },

    cancelButton: {
        backgroundColor: 'red', // Change to your desired color
        width: 100, // Adjust the width as needed
    },
    confirmButton: {
        backgroundColor: 'green', // Change to your desired color
        width: 100, // Adjust the width as needed
    },
    orderContainer: {
        flexDirection: 'column',
        padding: 10,
        marginBottom: 10,
        backgroundColor: 'white', // optional background color
        borderRadius: 10,
    },
    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    label: {
        fontWeight: 'bold',
    },
    value: {
        marginLeft: 5,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10,
    },
});


export default OrderManagement;