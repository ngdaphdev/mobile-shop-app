import React, { useState, useLayoutEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import BackButton from "../../../components/BackButton/BackButton";

const ProfileDetail = (props) => {
  const { navigation, route } = props;

  const item = route.params?.item;

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTransparent: "true",
      headerLeft: () => (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      headerRight: () => <View />,
    });
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.avatarContainer}>
          <Text style={styles.avatar}>{item.userName.slice(0, 1)}</Text>
        </View>
        <View style={styles.nameContainer}>
          <Text style={styles.name}>{item.userName}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Họ và tên:</Text>
          <Text style={styles.infoText}>{item.name}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Email:</Text>
          <Text style={styles.infoText}>{item.email}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Địa Chỉ:</Text>
          <Text style={styles.infoText}>{item.address}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>SĐT:</Text>
          <Text style={styles.infoText}>{item.phone}</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoLabel}>Trạng Thái:</Text>
          {item.status == 1 && (
            <Text style={[styles.infoText, styles.enable]}>Còn hoạt động</Text>
          )}

          {item.status == 0 && (
            <Text style={[styles.infoText, styles.disable]}>Đã bị khóa</Text>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ECF0F3",
  },
  body: {
    marginTop: 120,
    alignItems: "center",
    justifyContent: "center",
  },
  avatarContainer: {
    width: 140,
    height: 140,
    borderRadius: 70,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 6,
    shadowOpacity: 0.16,
  },
  avatar: {
    fontSize: 72,
    fontWeight: "700",
  },
  nameContainer: {
    marginTop: 24,
    alignItems: "center",
  },
  name: {
    fontSize: 24,
    fontWeight: "600",
  },
  infoContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 12,
  },
  infoLabel: {
    fontSize: 16,
    fontWeight: "600",
    color: "#666666",
    marginRight: 8,
  },
  infoText: {
    fontSize: 16,
  },
  enable: {
    backgroundColor: "green",
    color: "white",
    padding: 3,
  },
  disable: {
    backgroundColor: "red",
    color: "white",
    padding: 3,
  },
});

export default ProfileDetail;
