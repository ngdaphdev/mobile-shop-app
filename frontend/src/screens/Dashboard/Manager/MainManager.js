import React from "react";
import {Text, View, ScrollView, StyleSheet} from "react-native";
import {MenuButton,Button, Icon} from "@rneui/themed";
import {useNavigation} from "@react-navigation/native";
import OrderManagement from "./OrderManagement";
import PropTypes from "prop-types";
import DrawerContainer from "../../DrawerContainer/DrawerContainer";

export default function MainManager(props) {
    const {navigation} = props;
    const onPressManagerOrder = () => {
        // Navigate to the Order Management screen
        console.log("aa")
        navigation.navigate('OrderManagement'); // Replace 'OrderManagement' with the actual name of your Order Management screen
    };

    return (
        <ScrollView>
            <Text style={styles.subHeader}>Trang Quản Lí</Text>
            <View>
                <Button
                    title={<CustomTitle icon={"user"} title={"Quản lí User"}/>}
                    titleStyle={{fontWeight: "bold", fontSize: 18}}
                    buttonStyle={[styles.buttonsContainer, styles.btnOrange]}
                    onPress={() => {
                        onPressManagerUser();
                        console.log(props);
                    }}
                />

                <Button
                    title={<CustomTitle icon={"cart-plus"} title={"Quản lí Sản Phẩm"}/>}
                    titleStyle={{fontWeight: "bold", fontSize: 18}}
                    buttonStyle={[styles.buttonsContainer, styles.btnGreen]}
                />

                <MenuButton
                    title={
                        <CustomTitle icon={"product-hunt"} title={"Quản lí Đơn Hàng"}/>
                    }
                    titleStyle={{fontWeight: "bold", fontSize: 18}}
                    buttonStyle={[styles.buttonsContainer, styles.btnBlueLight]}
                    // onPress={onPressManagerOrder}

                    onPress={() => {

                        console.log("Quản lý đơn hàng")
                        // Chuyển hướng đến trang nhập thông tin
                        navigation.navigate("OrderManagement");
                        navigation.closeDrawer();
                    }}
                />
            </View>
        </ScrollView>
    );
}
MainManager.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }),
};
const CustomTitle = ({icon, title}) => {
    return (
        <View style={{flexDirection: "column"}}>
            <Icon name={icon} type="font-awesome" color="white"/>
            <Text
                style={{
                    fontWeight: "bold",
                    fontSize: 18,
                    color: "white",
                    marginTop: 10,
                }}
            >
                {title}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    buttonsContainer: {
        flex: 1,
        marginVertical: 20,
        marginHorizontal: 20,
        color: "white",
        padding: 30,
        borderRadius: 10,
    },
    container: {
        flex: 1,
    },
    icon: {
        flex: 1,
    },
    textTitle: {
        color: "white",
        flex: 1,
    },
    subHeader: {
        backgroundColor: "#2089dc",
        color: "white",
        textAlign: "center",
        paddingVertical: 5,
        marginBottom: 10,
        fontSize: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 0.5,
    },
    btnGreen: {
        backgroundColor: "#01EE1C",
    },
    btnOrange: {
        backgroundColor: "#EF5A00",
    },
    btnBlueLight: {
        backgroundColor: "#03CAED",
    },
});
