// import React, { useState } from "react";
// import { View, StyleSheet, Image, FlatList } from "react-native";
// import {
//   Text,
//   ListItem,
//   Avatar,
//   Icon,
//   Badge,
//   ListItemProps,
//   Button,
//   Switch,
//   lightColors,
// } from "@rneui/themed";

import React, { useState, useEffect, useLayoutEffect } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Alert,
  Image,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
} from "react-native";

import { Icon } from "@rneui/themed";
import Enviroment from "../../../../env/Enviroment";

let ManagerUser;
export default ManagerUser = (props) => {
  // const data = [
  //   { id: 1, image: "https://bootdey.com/img/Content/avatar/avatar1.png" },
  //   { id: 2, image: "https://bootdey.com/img/Content/avatar/avatar6.png" },
  //   { id: 3, image: "https://bootdey.com/img/Content/avatar/avatar2.png" },
  //   { id: 4, image: "https://bootdey.com/img/Content/avatar/avatar3.png" },
  //   { id: 5, image: "https://bootdey.com/img/Content/avatar/avatar4.png" },
  //   { id: 6, image: "https://bootdey.com/img/Content/avatar/avatar5.png" },
  //   { id: 7, image: "https://bootdey.com/img/Content/avatar/avatar7.png" },
  // ];

  const [users, setUsers] = useState({});
  const [query, setQuery] = useState("");

  const [isLoading, setLoading] = useState(true);

  const { navigation, route } = props;

  // useLayoutEffect(() => {
  //   navigation.setOptions({
  //     title: route.params?.name,
  //   });
  // }, []);

  // let showAlert;
  // showAlert = () => Alert.alert("Alert", "Button pressed ");

  const getUsers = async () => {
    try {
      const response = await fetch(
        `${Enviroment.URL_SERVER_API}/user/all?role=1`
      );
      const json = await response.json();
      setUsers(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);

  if (isLoading) {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator />
        <ActivityIndicator size="large" />
        {/* <ActivityIndicator size="small" color="#0000ff" />
        <ActivityIndicator size="large" color="#00ff00" /> */}
      </View>
    );
  }

  const onPressUserDetail = (item) => {
    navigation.navigate("UserDetail", { item });
  };

  const onPressProfileDetail = (item) => {
    navigation.navigate("ProfileDetail", { item });
  };
  const item = route.params?.item;
  console.log("Item:" + item);
  if (item != undefined) {
    const newData = users.map((obj) => {
      if (item.id === obj.id) {
        // Thay thế đối tượng nếu đúng điều kiện
        return item;
      }
      return obj;
    });
    setUsers(newData);
  }

  // const update = (item) => {
  //   var status = item.status == 0 ? 1 : 0;
  //   item.status = status;
  //   fetch("http://192.168.1.6:8080/user/update/" + item.id, {
  //     method: "PUT",
  //     headers: {
  //       Accept: "application/json",
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       password: item.password,
  //       name: item.name,
  //       email: item.email,
  //       address: item.address,
  //       phone: item.phone,
  //       role: item.role,
  //       status: item.status,
  //     }),
  //   });
  //   if (status == 0) {
  //     ToastAndroid.show("Khóa Thành Công !", ToastAndroid.SHORT);
  //   } else {
  //     ToastAndroid.show("Mở  Thành Công !", ToastAndroid.SHORT);
  //   }

  //   const newData = users.map((obj) => {
  //     if (item.id === obj.id) {
  //       // Thay thế đối tượng nếu đúng điều kiện
  //       return item;
  //     }
  //     return obj;
  //   });
  //   setUsers(users);
  // };

  return (
    <View style={styles.container}>
      <View style={styles.formContent}>
        <View style={styles.inputContainer}>
          <Image
            style={[styles.icon, styles.inputIcon]}
            source={{
              uri: "https://img.icons8.com/color/70/000000/search.png",
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Search..."
            underlineColorAndroid="transparent"
            onChangeText={(name_address) => {
              setQuery(name_address);
            }}
          />
        </View>
      </View>

      <FlatList
        enableEmptySections={true}
        // data={users}
        data={
          users == {}
            ? {}
            : users.filter(
                (obj) =>
                  obj.userName.toLowerCase().includes(query.toLowerCase()) ||
                  obj.email.toLowerCase().includes(query.toLowerCase()) ||
                  obj.phone.toLowerCase().includes(query.toLowerCase()) ||
                  obj.name.toLowerCase().includes(query.toLowerCase())
              )
        }
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <View style={styles.box}>
              <View>
                <Text style={styles.textId}>{item.id}</Text>
              </View>
              <View style={styles.boxContent}>
                <Text style={styles.title}>{item.userName}</Text>
                <Text style={styles.description}>Họ Tên:{item.name}</Text>
                <Text style={styles.description}>
                  SDT: {item.phone == "" ? "Unknow" : item.phone}
                </Text>
                <View style={styles.buttons}>
                  <TouchableOpacity
                    style={[styles.button, styles.profile]}
                    onPress={() => onPressUserDetail(item)}
                  >
                    <Icon name="user" type="font-awesome" color="white" />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[styles.button, styles.eye]}
                    onPress={() => onPressProfileDetail(item)}
                  >
                    <Icon name="eye" type="font-awesome" color="white" />
                  </TouchableOpacity>

                  {/* <TouchableOpacity
                    style={[styles.button]}
                    onPress={update(item)}
                  >
                    {item.status == 1 && (
                      <Icon
                        name="lock"
                        type="font-awesome"
                        color="white"
                        style={styles.lock}
                      />
                    )}

                    {item.status == 0 && (
                      <Icon
                        name="unlock"
                        type="font-awesome"
                        color="white"
                        style={styles.lock_open}
                      />
                    )}
                  </TouchableOpacity> */}

                  {/* {item.status == 0 && (
                    <TouchableOpacity
                      style={[styles.button, styles.lock_open]}
                      onPress={() => {
                        update(item);
                      }}
                    >
                      <Icon name="unlock" type="font-awesome" color="white" />
                    </TouchableOpacity>
                  )} */}
                </View>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 100,
    height: 100,
  },
  box: {
    padding: 20,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: "white",
    flexDirection: "row",
  },
  boxContent: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    marginLeft: 10,
  },
  title: {
    fontSize: 18,
    color: "#151515",
  },
  description: {
    fontSize: 15,
    color: "#646464",
  },
  buttons: {
    flexDirection: "row",
  },
  button: {
    height: 35,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    width: 50,
    marginRight: 5,
    marginTop: 5,
  },
  icon: {
    width: 20,
    height: 20,
  },
  view: {
    backgroundColor: "#eee",
  },
  profile: {
    backgroundColor: "#1E90FF",
  },
  eye: {
    backgroundColor: "#5BC7DE",
  },
  lock: {
    backgroundColor: "red",
  },

  lock_open: {
    backgroundColor: "#228B22",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  formContent: {
    flexDirection: "row",
    // marginTop: 10,
  },
  inputContainer: {
    borderBottomColor: "#000",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderWidth: 1,
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    margin: 10,
  },
  iconBtnSearch: {
    alignSelf: "center",
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    flex: 1,
    borderColor: "#000",
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: "center",
  },

  textId: {
    justifyContent: "center",
    alignItems: "center",
    fontSize: 20,
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
});
