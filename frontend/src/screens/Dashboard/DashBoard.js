import React, { useEffect, useState } from "react";
import { Text, View, ScrollView, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import {
  Card,
  lightColors,
  Button,
  TabView,
  Icon,
  withBadge,
  Tab,
} from "@rneui/themed";


import Profile from "./Manager/Profile";

const BadgedIcon = withBadge(15)(Icon);

export default DashBoard = (props) => {
  const [index, setIndex] = useState(0);
  const { navigation } = props;
  // const [isLoading, setLoading] = useState(true);
  // const [data,setData] = useState({})

  // const getProducts = async () => {
  //   try {
  //     const response = await fetch('http://192.168.1.17:8080/products/all');
  //     const json = await response.json();
  //     setData(json.data);
  //   } catch (error) {
  //     console.error(error);
  //   } finally {
  //     setLoading(false);
  //   }
  // };

  // useEffect(() => {
  //   getProducts();
  // }, []);

  // console.log(data)

  const CustomTitle = ({ icon, title }) => {
    return (
      <View style={{ flexDirection: "column" }}>
        <Icon name={icon} type="font-awesome" color="white" />
        <Text
          style={{
            fontWeight: "bold",
            fontSize: 18,
            color: "white",
            marginTop: 10,
          }}
        >
          {title}
        </Text>
      </View>
    );
  };

  return (
    <>
      <TabView value={index} onChange={setIndex} animationType="spring">
        {/*Giao dien quan li*/}
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <ScrollView>
            <View>
              <Button
                title={<CustomTitle icon={"user"} title={"Quản Lý Người Dùng"} />}
                titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                buttonStyle={[styles.buttonsContainer, styles.btnOrange]}
                onPress={() => {
                  navigation.navigate("ManagerUser");
                }}
              />

              <Button
                title={
                  <CustomTitle icon={"cart-plus"} title={"Quản Lý Kho và Sản Phẩm"} />
                }
                titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                buttonStyle={[styles.buttonsContainer, styles.btnGreen]}
                onPress={() => {
                  navigation.navigate("ManagerProduct");
                }}
              />

              <Button
                title={
                  <CustomTitle
                    icon={"product-hunt"}
                    title={"Quản Lý Danh Mục"}
                  />
                }
                titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                buttonStyle={[styles.buttonsContainer, styles.btnBlackLight]}
                onPress={() => {
                  navigation.navigate("ManagerCategory");
                }}
              />

              <Button
                title={
                  <CustomTitle
                    icon={"product-hunt"}
                    title={"Quản Lý Đơn Hàng"}
                  />
                }
                titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                buttonStyle={[styles.buttonsContainer, styles.btnBlueLight]}
                onPress={() => {
                  navigation.navigate("OrderManagement");
                }}
              />
            </View>
          </ScrollView>
        </TabView.Item>
        {/*End giao dien quan li*/}

        <TabView.Item
          style={{ backgroundColor: "blue", width: "100%" }}
        ></TabView.Item>
        <TabView.Item style={{ width: "100%" }}>
          <Profile></Profile>
        </TabView.Item>
      </TabView>

      <Tab
        value={index}
        onChange={(e) => setIndex(e)}
        indicatorStyle={{
          backgroundColor: "white",
          height: 3,
        }}
        variant="primary"
      >
        <Tab.Item
          title="Quản Lý"
          titleStyle={{ fontSize: 12 }}
          icon={{ name: "timer", type: "ionicon", color: "white" }}
        />
        <Tab.Item
          title="Thống Kê"
          titleStyle={{ fontSize: 12 }}
          icon={{ name: "reader-outline", type: "ionicon", color: "white" }}
        />
        <Tab.Item
          title="Tài Khoản"
          titleStyle={{ fontSize: 12 }}
          icon={{
            name: "body-outline",
            type: "ionicon",
            color: "white",
          }}
        />
      </Tab>
    </>
  );
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flex: 1,
    marginVertical: 20,
    marginHorizontal: 20,
    color: "white",
    padding: 30,
    borderRadius: 10,
  },
  container: {
    flex: 1,
  },
  icon: {
    flex: 1,
  },
  textTitle: {
    color: "white",
    flex: 1,
  },
  subHeader: {
    backgroundColor: "#2089dc",
    color: "white",
    textAlign: "center",
    paddingVertical: 5,
    marginBottom: 10,
    fontSize: 25,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.5,
  },
  btnGreen: {
    backgroundColor: "#01EE1C",
  },
  btnOrange: {
    backgroundColor: "#EF5A00",
  },
  btnBlueLight: {
    backgroundColor: "#03CAED",
  }, btnBlackLight: {
    backgroundColor: "#283048",
  },
});

DashBoard.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};
