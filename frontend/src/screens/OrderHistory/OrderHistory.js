import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import OrderHistoryItem from './OrderHistoryItem';
import { Text } from '@rneui/themed';
import Button from '../../components/login/Button';


const OrderHistory = (props) => {
    const { navigation } = props;
    const orders = [
        {
            id: 1,
            date: '2023-01-15',
            total: 150,
            status: 'Đã giao hàng',
            items: [
                { productId: 101, productName: 'Điện thoại Iphone 15', quantity: 2, price: 100 },
                { productId: 102, productName: 'Quần Jean', quantity: 1, price: 50 },
            ],
        },
        {
            id: 2,
            date: '2023-01-10',
            total: 200,
            status: 'Đã nhận hàng',
            items: [
                { productId: 201, productName: 'Giày Sneaker', quantity: 1, price: 120 },
                { productId: 202, productName: 'Áo Thun', quantity: 1, price: 80 },
            ],
        },
    ];

    const renderOrderItem = ({ item }) => (
        <OrderHistoryItem order={item} />
    );

    return (
        <View>
            {
                orders.length > 0 ? (
                    <FlatList
                        data={orders}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={renderOrderItem}
                    />
                ) : (
                    <View>
                        <Text style={styles.noOrdersText}>Không có đơn hàng nào.</Text>
                        <Button onPress={() => {
                            navigation.navigate("Home");
                        }}>Mua sắm ngay!</Button>
                    </View>
                )}

        </View>
    );
};

export default OrderHistory;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    noOrdersText: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 20,
    },
});