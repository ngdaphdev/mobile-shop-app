import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const OrderHistoryItem = ({ order }) => {
  const [showDetails, setShowDetails] = useState(false);
  const navigation = useNavigation();

  const renderItem = ({ item }) => (
    <View style={styles.productItem}>
      <Text style={styles.productName}>{item.productName}</Text>
      <Text style={styles.productDetail}>Số lượng: {item.quantity}</Text>
      <Text style={styles.productDetail}>Giá: ${item.price}</Text>
    </View>
  );

  const navigateToOrderDetail = () => {
    navigation.navigate('DetailOrderHistory', { order });
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => setShowDetails(!showDetails)}>
        <Text style={styles.date}>{order.date}</Text>
        <Text style={styles.total}>Tổng: ${order.total}</Text>
        <Text style={styles.status}>{order.status}</Text>
      </TouchableOpacity>
      
      {showDetails && (
        <FlatList
          data={order.items}
          renderItem={renderItem}
          keyExtractor={(item) => `${item.productId}`}
        />
      )}

      <TouchableOpacity style={styles.detailButton} onPress={navigateToOrderDetail}>
        <Text style={styles.detailButtonText}>Xem chi tiết</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 16,
    marginBottom: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  date: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  total: {
    fontSize: 16,
    marginBottom: 8,
  },
  status: {
    fontSize: 16,
    color: 'green',
    marginBottom: 16,
  },
  productItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    paddingVertical: 8,
  },
  productName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  productDetail: {
    fontSize: 14,
    color: '#555',
  },
  detailButton: {
    backgroundColor: '#3498db',
    padding: 8,
    borderRadius: 4,
    alignItems: 'center',
    marginTop: 16,
  },
  detailButtonText: {
    color: '#fff',
    fontSize: 16,
  },
});

export default OrderHistoryItem;
