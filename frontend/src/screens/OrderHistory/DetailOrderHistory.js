import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import QRCodeComponent from '../../components/QRCode/QRCodeComponent';

const OrderHistoryItem = (props) => {

    const { navigation, route } = props;
    const order = route?.params?.order;

    const renderItem = ({ item }) => (
        <View style={styles.productItem}>
            <Text style={styles.productName}>{item.productName}</Text>
            <Text style={styles.productDetail}>Số lượng: {item.quantity}</Text>
            <Text style={styles.productDetail}>Giá: ${item.price}</Text>
        </View>
    );

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Chi tiết đơn hàng #{order.id}</Text>
            <Text style={styles.date}>{order.date}</Text>
            <Text style={styles.total}>Tổng: ${order.total}</Text>
            <Text style={styles.status}>{order.status}</Text>
            <FlatList
                data={order.items}
                renderItem={renderItem}
                keyExtractor={(item) => `${item.productId}`}
            />
            <QRCodeComponent order={order} />

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 16,
        marginBottom: 16,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    date: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    total: {
        fontSize: 16,
        marginBottom: 8,
    },
    status: {
        fontSize: 16,
        color: 'green',
        marginBottom: 16,
    },
    productItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        paddingVertical: 8,
    },
    productName: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    productDetail: {
        fontSize: 14,
        color: '#555',
    },
});

export default OrderHistoryItem;