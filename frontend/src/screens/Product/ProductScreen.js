import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import {
  ScrollView,
  Text,
  View,
  Image,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from "react-native";
import { LogBox } from "react-native";

// Disable the warning about VirtualizedLists inside a ScrollView
LogBox.ignoreLogs(["VirtualizedLists should never be nested"]);
import styles from "../Product/style";
import Carousel, { Pagination } from "react-native-snap-carousel";
import {
  getIngredientName,
  getCategoryName,
  getCategoryById,
} from "../../data/MockDataAPI";
import BackButton from "../../components/BackButton/BackButton";
import ViewIngredientsButton from "../../components/ViewIngredientsButton/ViewIngredientsButton";
import Comment from "../../components/Comment/Comment";
import Enviroment from "../../../env/Enviroment";
import { TextInput } from "react-native-gesture-handler";
import BuyButton from "../../components/BuyButton/BuyButton";
import SpecificationItem from "../../components/Specifications/SpecificationItem";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import { ProgressBar, MD3Colors } from "react-native-paper";
import { fetchDataPhotoArrayProductAPI } from "../../data/dataArrays";
import numeral from "numeral";
import axios from "axios";
import ReviewComponent from "../Rating/Reviews";
import AddCartButton from "../../components/AddCartButton/AddCartButton";

import {useFocusEffect, useNavigation} from "@react-navigation/native";
import InputInfoScreen from "../Checkout/InputInfoScreen";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";
import { _retrieveData } from "../../data/dataArrays";

const { width: viewportWidth } = Dimensions.get("window");

export default function ProductScreen(props) {
  const navigation2 = useNavigation();
  const { navigation, route } = props;
  const [cartItems, setCartItems] = useState([]);
  const item = route.params?.item;
  const id = item.id;
  const categoryId = item.categoryId;
  const name = item.name;
  const photoURL = item.photoURL;
  const price = numeral(item.price).format("0,0").replace(/,/g, ".");
  const description = item.description;
  const [activeSlide, setActiveSlide] = useState(0);
  const slider1Ref = useRef();
  const [photoURLArray, setPhotoURLArray] = useState([]);

  let fetchDataPhotoProduct = async () => {
    try {
      const response = await axios.get(`${Enviroment.URL_SERVER_API}/photoarrayurls/getPhotoURLByProductId?productId=${id}`)
      setPhotoURLArray(response.data.length > 0 ? response.data: [item.photoURL]);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }
  useEffect(() => {
    fetchDataPhotoProduct()
  }, [])

  const [selectedItem, setSelectedItem] = useState(null);

  // Specifications
  const [specifications, setSpecifications] = useState([]);

  const [user, setUser] = useState({});
  let fetchDataSpecificationsProduct = async () => {
    try {
      const response = await axios.get(
        `${Enviroment.URL_SERVER_API}/specifications/getSpecificationsByProductId?productId=${id}`
      );
      setSpecifications(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    fetchDataSpecificationsProduct();
  }, [id]);

  //Rating
  var ratingReturn = route.params?.ratings;
  const [comment, setComment] = useState("");

  const handleCommentSubmit = () => {
    if (comment.trim() !== "") {
      onCommentSubmit(comment);
      setComment("");
    }
  };
  const category = item.categoryId;
  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Quay lại",
      headerLeft: () => (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      headerRight: () => <View />,
    });

    // Gọi hàm lấy giỏ hàng từ AsyncStorage khi mở màn hình
    getCartItemsFromAsyncStorage();
  }, []);
  // Hàm để lấy giỏ hàng từ AsyncStorage khi mở màn hình
  const getCartItemsFromAsyncStorage = async () => {
    try {
      const storedCartItems = await AsyncStorage.getItem("cartItems");
      if (storedCartItems) {
        setCartItems(JSON.parse(storedCartItems));
      }
    } catch (error) {
      console.error("Error getting cart items:", error);
    }
  };

  // Hàm để lưu giỏ hàng vào AsyncStorage
  const saveToAsyncStorage = async (cartItems) => {
    try {
      await AsyncStorage.setItem("cartItems", JSON.stringify(cartItems));
    } catch (error) {
      console.error("Error saving cart items:", error);
    }
  };

  const handleAddToCart = () => {
    const existingItemIndex = cartItems.findIndex(
      (cartItem) => cartItem.id === item.id
    );

    if (existingItemIndex !== -1) {
      const updatedCartItems = [...cartItems];
      updatedCartItems[existingItemIndex].quantity += 1;
      updatedCartItems[existingItemIndex].totalPrice =
        updatedCartItems[existingItemIndex].quantity * item.price;

      setCartItems(updatedCartItems);
      saveToAsyncStorage(updatedCartItems);
    } else {
      const updatedCartItems = [
        ...cartItems,
        { ...item, quantity: 1, totalPrice: item.price },
      ];
      setCartItems(updatedCartItems);
      saveToAsyncStorage(updatedCartItems);
    }

    navigation.navigate("Cart", { cartItems });

    // Hiển thị thông báo khi thêm vào giỏ hàng thành công
    Toast.show({
      type: "success",
      position: "bottom",
      text1: "Sản phẩm đã được thêm vào giỏ hàng",
      visibilityTime: 3000, // Hiển thị trong 3 giây
      autoHide: true,
      topOffset: 30,
    });
  };
  // Hàm xử lý khi bấm "Mua ngay"
  const handleBuyNow = () => {
    // Đảm bảo selectedItem đã được định nghĩa hoặc có giá trị
    if (selectedItem) {
      navigation.navigate("InputInfoScreen", {
        item: selectedItem, // Thông tin sản phẩm đã chọn
        selectedCartItems: cartItems, // Danh sách sản phẩm trong giỏ hàng
      });
    } else {
      // Xử lý khi selectedItem không tồn tại
      console.error("selectedItem is not defined.");
    }
  };

  const renderImage = ({ item }) => (
    <TouchableHighlight>
      <View style={{ backgroundColor: "#fff" }}>
        <Image
          style={{ width: "100%", height: 260, resizeMode: "contain" }}
          source={{ uri: item }}
        />
      </View>
    </TouchableHighlight>
  );

  const handleLikePress = (liked) => {
    console.log(liked ? "Liked!" : "Unliked!");
  };

  const handleReplyPress = () => {
    console.log("Reply pressed!");
  };

  const checkRate = async () => {
    const userTemp = await _retrieveData();
    console.log("UserTemp:" + userTemp);
    setUser(userTemp);
    if (userTemp != undefined && user != {}) {
      navigation.navigate("RatingProduct", { item });
    } else {
      navigation.navigate("LoginScreen");
    }
  };

  const renderItem = ({ item, index }) => (
    <SpecificationItem item={item} index={index} />
  );

  return (
    <ScrollView>
      <View>
        <View>
          <Carousel
            ref={slider1Ref}
            data={photoURLArray}
            renderItem={renderImage}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
            firstItem={0}
            loop={false}
            autoplay={false}
            autoplayDelay={500}
            autoplayInterval={3000}
            onSnapToItem={(index) => setActiveSlide(index)}
          />
          <Pagination
            dotsLength={photoURLArray != null ? photoURLArray.length : 1}
            activeDotIndex={activeSlide}
            containerStyle={styles.paginationContainer}
            dotColor="rgba(255, 0, 0, 1)"
            dotStyle={styles.paginationDot}
            inactiveDotColor="white"
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={slider1Ref.current}
            tappableDots={!!slider1Ref.current}
          />
        </View>
      </View>
      <View style={styles.infoRecipeContainer}>
        <Text style={styles.infoRecipeName}>{name}</Text>
        <View>
          <Text style={styles.price}>{price}đ</Text>
        </View>
        <View style={styles.infoContainer}>
          {item.quantity > 0 ? (
            <BuyButton
              onPress={() => {
                let title = "Điền thông tin mua hàng";
                // console.log(item)
                // Chuyển hướng đến trang nhập thông tin
                navigation2.navigate("InputInfoScreen", { item, title });
              }}
            />
          ) : (
            <View style={{ borderRadius: 40, marginTop: 20 }}>
              <View
                style={{
                  flex: 1,
                  paddingTop: 15,
                  paddingBottom: 15,
                  paddingLeft: 80,
                  paddingRight: 80,
                  borderRadius: 40,
                  borderColor: "#737373",
                  borderWidth: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 14, color: "#000" }}>Hết hàng</Text>
              </View>
            </View>
          )}

          {/*<AddCartButton*/}
          {/*    onPress={() => {*/}
          {/*        let ingredients = '';*/}
          {/*        let title = "Ingredients for " + '';*/}
          {/*        console.log("AddCartButton Click")*/}
          {/*        // navigation.navigate("IngredientsDetails", { ingredients, title });*/}
          {/*    }}*/}
          {/*/>*/}
          <AddCartButton onPress={handleAddToCart} />
        </View>
      </View>
      <View style={styles.boxContent}>
        <Text style={styles.titleText}>Thông tin sản phẩm</Text>
        <Text style={styles.contentText}>{description}</Text>
      </View>
      <View style={styles.boxContent}>
        <Text style={styles.titleText}>Thông số kỹ thuật</Text>
        <FlatList
          style={{ marginBottom: 20 }}
          data={specifications}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
      {/* <View style={[styles.boxContent, { paddingBottom: 20 }]}>
        <Text style={styles.titleText}>Đánh giá sản phẩm</Text>
        <View style={styles.boxReview}>
          <View
            style={[styles.boxReviewText, { flexBasis: "40%", margin: 10 }]}
          >
            <Text
              style={[
                styles.boxReviewText,
                {
                  color: "#fb6d2c",
                  fontSize: 25,
                  fontWeight: "bold",
                },
              ]}
            >
              4.5
            </Text>
            <Text style={styles.boxReviewText}>
              <FontAwesomeIcon name="star" color="#fb6d2c" />
              <FontAwesomeIcon name="star" color="#fb6d2c" />
              <FontAwesomeIcon name="star" color="#fb6d2c" />
              <FontAwesomeIcon name="star" color="#fb6d2c" />
              <FontAwesomeIcon name="star" color="#fb6d2c" />
            </Text>
            <Text style={styles.boxReviewText}>200 người đánh giá</Text>
          </View>
          <View
            style={[
              styles.boxReviewText,
              { flexBasis: "60%", bordeLeftColor: "black", borderLeftWidth: 1 },
            ]}
          >
            <View style={{ padding: 10 }}>
              <View
                style={{
                  flexDirection: "row",
                  alignContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <Text>
                  5 <FontAwesomeIcon name="star" color="#fb6d2c" />
                </Text>
                <ProgressBar
                  style={{ width: 100, marginLeft: 8, marginRight: 8 }}
                  progress={0.6}
                  color={MD3Colors.error50}
                />
                <Text>60%</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <Text>
                  4 <FontAwesomeIcon name="star" color="#fb6d2c" />
                </Text>
                <ProgressBar
                  style={{ width: 100, marginLeft: 8, marginRight: 8 }}
                  progress={0.2}
                  color={MD3Colors.error50}
                />
                <Text>20%</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <Text>
                  3 <FontAwesomeIcon name="star" color="#fb6d2c" />
                </Text>
                <ProgressBar
                  style={{ width: 100, marginLeft: 8, marginRight: 8 }}
                  progress={0.1}
                  color={MD3Colors.error50}
                />
                <Text>10%</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <Text>
                  2 <FontAwesomeIcon name="star" color="#fb6d2c" />
                </Text>
                <ProgressBar
                  style={{ width: 100, marginLeft: 8, marginRight: 8 }}
                  progress={0.07}
                  color={MD3Colors.error50}
                />
                <Text>7%</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <Text>
                  1 <FontAwesomeIcon name="star" color="#fb6d2c" />
                </Text>
                <ProgressBar
                  style={{ width: 100, marginLeft: 8, marginRight: 8 }}
                  progress={0.03}
                  color={MD3Colors.error50}
                />
                <Text>3%</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={[styles.containerComment, { marginTop: 20 }]}>
          <TextInput
            style={styles.input}
            placeholder="Viết đánh giá..."
            value={comment}
            onChangeText={(text) => setComment(text)}
          />
          <TouchableOpacity style={styles.button} onPress={handleCommentSubmit}>
            <Text style={styles.buttonText}>Gửi</Text>
          </TouchableOpacity>
        </View>
        <View style={[{ textAlign: "center" }]}>
          <Comment
            name={`Truc`}
            content={`Sản phẩm này có tốt không`}
            likes={100}
            profilePicture={`iphone.png`}
            timestamp={`12/12/2023`}
            onLikePress={handleLikePress}
            onReplyPress={handleReplyPress}
          />
        </View>
      </View> */}
      <View
        style={{
          flex: 2,
          flexDirection: "row",
          marginHorizontal: 30,
          justifyContent: "flex-end",
          marginBottom: 10,
        }}
      >
        <TouchableOpacity
          onPress={() => {
            checkRate();
          }}
          style={{
            width: "48%",
            height: 50,
            borderRadius: 10,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#4267B2",
          }}
        >
          <Text style={{ color: "#fff", fontWeight: "bold" }}>Đánh giá</Text>
        </TouchableOpacity>
      </View>
      <ReviewComponent
        newRating={ratingReturn}
        item={item}
        navigate={navigation}
        route={route}
      ></ReviewComponent>
      <View style={styles.boxContent}></View>
    </ScrollView>
  );
}
