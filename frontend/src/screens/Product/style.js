import { StyleSheet, Dimensions } from 'react-native';

const { width: viewportWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        zIndex: -100
    },
    carouselContainer: {

    },
    price: {
        fontSize: 25,
        color: 'red',
        fontWeight: 'bold'
    },
    carousel: {},

    image: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: 250
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        width: viewportWidth,
        height: 250
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'flex-end',
        paddingVertical: 8,
        marginTop: 200
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 0
    },
    infoRecipeContainer: {
        flex: 1,
        margin: 25,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    infoPhoto: {
        height: 20,
        width: 20,
        marginRight: 0
    },
    infoRecipe: {
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 5,
    },
    category: {
        fontSize: 14,
        fontWeight: 'bold',
        margin: 10,
        color: '#2cd18a'
    },
    infoDescriptionRecipe: {
        textAlign: 'left',
        fontSize: 16,
        marginTop: 30,
        margin: 15
    },
    infoRecipeName: {
        fontSize: 26,
        margin: 10,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center'
    },
    containerComment: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingBottom: 16,
    },
    input: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 20,
        paddingHorizontal: 16,
    },
    button: {
        marginLeft: 10,
        padding: 10,
        backgroundColor: '#007bff',
        borderRadius: 20,
    },
    buttonText: {
        color: '#fff',
    },
    titleText: {
        paddingHorizontal: 16,
        paddingVertical: 12,
        fontSize: 20,
        fontWeight: "bold"
    },
    contentText: {
        paddingHorizontal: 16,
        paddingVertical: 12,
        fontSize: 16,
        paddingTop:0,
    },
    boxContent: {
        backgroundColor: '#fff',
        margin: 10,
    },
    boxReview:{
        flexDirection: 'row',
        flex: 1,
        marginBottom: 20
    },
    boxReviewItem: {
        paddingHorizontal: 16,
        paddingVertical: 12,
        fontSize: 16,

    },
    boxReviewText: {
        textAlign: 'center',
    },
    productColor: {
        width: 70,
        height: 33,
        borderWidth: 1,
        textAlign: 'center',
        lineHeight: 33,
        borderColor: '#c7c7c7',
        fontSize: 16,
        margin: 3
    },
    productColorActive: {
        borderColor: '#0080ff',
        color: '#0080ff'
    },
    radioButtonContainer: {
        marginTop: 10,
    },

    radioButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },

});

export default styles;
