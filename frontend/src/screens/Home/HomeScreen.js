import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import {
  FlatList,
  Text,
  View,
  TouchableHighlight,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
} from "react-native";
import styles from "./styles";
import productStyle from "./ProductStyle";
import MenuImage from "../../components/MenuImage/MenuImage";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";
import { ButtonGroup, Icon } from "@rneui/themed";
import { BottomSheet, Button, ListItem } from "@rneui/themed";
import { StyleSheet } from "react-native";
import numeral from "numeral";
import axios from "axios";
import Enviroment from "../../../env/Enviroment";
import { TouchableWithoutFeedback } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Dimensions } from "react-native";
const { width: viewportWidth } = Dimensions.get("window");
import { AntDesign } from "@expo/vector-icons";
import { useFocusEffect } from "@react-navigation/native";
const { width, height } = Dimensions.get("window");
import { EvilIcons } from "@expo/vector-icons";
import { set } from "react-native-reanimated";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { _retrieveData } from "../../data/dataArrays";
import { Rating } from "@kolking/react-native-rating";
export default function HomeScreen(props) {
  const { navigation, route } = props;
  const slider1Ref = useRef();
  const [activeSlide, setActiveSlide] = useState(0);
  const [products, setProducts] = useState([]);
  const [filterProducts, setFilterProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [categoriesDataFilter, setCategoriesDataFilter] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [user, setUser] = useState({});

  let fetchDataProduct = async () => {
    try {
      const response = await axios.get(
        `${Enviroment.URL_SERVER_API}/products/all`
      );
      setProducts(response.data);
      setFilterProducts(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchDataProduct();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      fetchDataProduct();
    }, [])
  );

  const handleSearchProduct = () => {
    navigation.navigate("Search");
  };
  let fetchDataCategories = async () => {
    try {
      const response = await axios.get(
        `${Enviroment.URL_SERVER_API}/categories/all`
      );
      setCategories(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchDataCategories().then(() => {});
  }, []);

  useEffect(() => {
    setCategoriesDataFilter(categories.map((category) => category.name));
  }, [categories]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <MenuImage
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ),
      headerTitle: () => (
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            width: width * (70 / 100),
          }}
        >
          <View>
            <Text>Home</Text>
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <TouchableOpacity onPress={handleSearchProduct}>
              <Image
                style={{ width: 20, height: 20, padding: 5, marginBottom: 2 }}
                source={require("../../../assets/icons/search.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setModalVisible(true)}
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-end",
                padding: 5,
              }}
            >
              <AntDesign
                style={{ marginBottom: 4 }}
                name="filter"
                size={20}
                color="black"
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("ScanQRCode")}>
              <View>
                <FontAwesomeIcon
                  style={styles.qrCode}
                  name="qrcode"
                  color="#fb6d2c"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      ),
      headerRight: () => <View></View>,
    });
  }, []);

  const onPressProduct = (item) => {
    navigation.navigate("Product", { item });
  };

  const aveger = (list) => {
    if (list.length === 0) {
      return 0;
    }

    const sum = list.reduce((acc, currentValue) => acc + currentValue.rate, 0);
    return sum / list.length;
  };

  const renderProducts = ({ item }) => (
    <TouchableHighlight
      underlayColor="#f1f1f1"
      onPress={() => onPressProduct(item)}
    >
      <View style={productStyle.container}>
        {item.photoURL.includes("http") ? (
          <Image style={productStyle.photo} source={{ uri: item.photoURL }} />
        ) : (
          <Image
            style={productStyle.photo}
            source={{
              uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
            }}
          />
        )}
        <Text style={productStyle.name} numberOfLines={2} ellipsizeMode="tail">
          {item.name}
        </Text>
        {item.quantity > 0 ? (
          <Text style={productStyle.price}>
            {numeral(item.price).format("0,0").replace(/,/g, ".")}₫
          </Text>
        ) : (
          <Text style={{ fontSize: 16, color: "#838383", margin: 10 }}>
            Tạm thời hết hàng
          </Text>
        )}

        {/* <Text> */}
        {/* <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" /> */}
        <View style={{ flexDirection: "row" }}>
          <Rating
            size={10}
            isDisabled={true}
            rating={isNaN(aveger(item.ratings)) ? 5 : aveger(item.ratings)}
            style={{}}
          />
          <Text style={{ fontSize: 12 }}> {item.ratings.length}</Text>
        </View>
        {/* </Text> */}
      </View>
    </TouchableHighlight>
  );

  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [state, setState] = useState(false);

  const sortData = (products) => {
    if (selectedIndex == -1) {
      return products;
    } else if (selectedIndex == 0) {
      if (state) {
        return products.sort((a, b) =>
          a.name == b.name ? 0 : a.name > b.name ? 1 : -1
        );
      } else {
        return products.sort((a, b) =>
          a.name == b.name ? 0 : a.name < b.name ? 1 : -1
        );
      }
    } else if (selectedIndex == 1) {
      if (state) {
        return products.sort((a, b) => {
          var a = parseInt(a.price, 10);
          var b = parseInt(b.price, 10);
          return a - b;
        });
      } else {
        return products.sort((a, b) => {
          var a = parseInt(a.price, 10);
          var b = parseInt(b.price, 10);
          return b - a;
        });
      }
    }
  };

  const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(null);
  const [selectedPriceIndex, setSelectedPriceIndex] = useState(null);
  const handleFilterPrice = (value) => {
    if (selectedPriceIndex === value) {
      setSelectedPriceIndex(null);
    } else {
      setSelectedPriceIndex(value);
    }
  };

  const handleFilterCategory = (value) => {
    if (selectedCategoryIndex === value) {
      setSelectedCategoryIndex(null);
    } else {
      setSelectedCategoryIndex(value);
    }
  };

  const handleFilter = async () => {
    setUser(await _retrieveData());

    let filteredData = [...filterProducts];

    switch (selectedPriceIndex) {
      case 0:
        filteredData = filteredData.filter(
          (product) => product.price < 5000000
        );
        break;
      case 1:
        filteredData = filteredData.filter(
          (product) => product.price > 5000000 && product.price < 10000000
        );
        break;
      case 2:
        filteredData = filteredData.filter(
          (product) => product.price > 10000000 && product.price < 20000000
        );
        break;
      case 3:
        filteredData = filteredData.filter(
          (product) => product.price > 20000000
        );
        break;
      default:
        break;
    }

    const categorySelected =
      selectedCategoryIndex != null
        ? categories[selectedCategoryIndex].id
        : null;
    if (selectedCategoryIndex != null) {
      filteredData = filteredData.filter(
        (product) => product.categoryId === categorySelected
      );
    }

    setProducts(filteredData);
    setModalVisible(false);
  };

  const renderImage = ({ item }) => (
    <TouchableHighlight>
      <View style={{ backgroundColor: "#fff" }}>
        <Image
          style={{ width: "100%", height: 260, resizeMode: "contain" }}
          source={{ uri: item }}
        />
      </View>
    </TouchableHighlight>
  );

  const photoURLArray = [
    "https://cdn-v2.didongviet.vn/files/banners/2024/0/4/1/1704338329336_tragop_824x400_copy.jpg",
    "https://cdn-v2.didongviet.vn/files/banners/2024/0/5/1/1704396759187_824x400.jpg",
    "https://cdn-v2.didongviet.vn/files/banners/2024/0/3/1/1704247707139_ip13_pro_max_824x400.jpg",
    "https://cdn-v2.didongviet.vn/files/banners/2024/0/4/1/1704345539946_teaaaat_nheaa_viaa_824x400.jpg",
  ];

  return (
    <View style={{ marginBottom: "100%" }}>
      <View>
        <Carousel
          ref={slider1Ref}
          data={photoURLArray != null ? photoURLArray : []}
          renderItem={renderImage}
          sliderWidth={viewportWidth}
          itemWidth={viewportWidth}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          firstItem={0}
          loop={true}
          autoplay={true}
          autoplayDelay={500}
          autoplayInterval={3000}
          onSnapToItem={(index) => setActiveSlide(index)}
        />
        <Pagination
          dotsLength={photoURLArray != null ? photoURLArray.length : 1}
          activeDotIndex={activeSlide}
          containerStyle={styles.paginationContainer}
          dotColor="rgba(255, 0, 0, 1)"
          dotStyle={styles.paginationDot}
          inactiveDotColor="black"
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          carouselRef={slider1Ref.current}
          tappableDots={!!slider1Ref.current}
        />
      </View>
      <View
        style={{
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          flexDirection: "row",
        }}
      >
        <Text style={{ marginRight: 10 }}>Sắp Xếp:</Text>
        <Text>
          <ButtonGroup
            buttonStyle={{ padding: 10 }}
            selectedButtonStyle={{ backgroundColor: "#e2e2e2" }}
            selectedTextStyle={{ color: "#000" }}
            selectedIndex={selectedIndex}
            onPress={(value) => {
              if (value == selectedIndex) {
                setState(!state);
              } else {
                setState(false);
              }
              setSelectedIndex(value);
            }}
            buttons={[
              <Text>
                Theo Tên{"  "}
                {selectedIndex == 0 && !state && (
                  <FontAwesomeIcon name="arrow-up" color="#000" />
                )}
                {selectedIndex == 0 && state && (
                  <FontAwesomeIcon name="arrow-down" color="#000" />
                )}
              </Text>,
              <Text>
                Theo Giá{"  "}
                {selectedIndex == 1 && !state && (
                  <FontAwesomeIcon name="arrow-up" color="#000" />
                )}
                {selectedIndex == 1 && state && (
                  <FontAwesomeIcon name="arrow-down" color="#000" />
                )}
              </Text>,
            ]}
          />
        </Text>
      </View>
      <FlatList
        style={{height: height*(55/100)}}
        vertical
        showsVerticalScrollIndicator={false}
        numColumns={2}
        data={sortData(products)}
        renderItem={renderProducts}
        keyExtractor={(item) => `${item.id}`}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={homeStyles.centeredView}>
          <View style={homeStyles.modalView}>
            <View style={homeStyles.box}>
              <Text style={homeStyles.title}>Thương hiệu</Text>
              <ButtonGroup
                vertical={false}
                buttons={categoriesDataFilter}
                selectedIndex={selectedCategoryIndex}
                onPress={handleFilterCategory}
                containerStyle={{ marginBottom: 20, color: "#000" }}
                buttonStyle={{ textAlign: "left", margin: 2 }}
                textStyle={{ color: "#000", textAlign: "left", fontSize: 14 }}
              />
            </View>
            <View style={homeStyles.box}>
              <Text style={homeStyles.title}>Giá</Text>
              <ButtonGroup
                vertical={false}
                buttons={[
                  "Dưới 5 triệu",
                  "5 - 10 triệu",
                  "10 - 20 triệu",
                  "Trên 20 triệu",
                ]}
                selectedIndex={selectedPriceIndex}
                onPress={handleFilterPrice}
                containerStyle={{ marginBottom: 20, color: "#000" }}
                buttonStyle={{ textAlign: "left", margin: 2 }}
                textStyle={{ color: "#000", textAlign: "left", fontSize: 13 }}
              />
            </View>
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                width: width * (85 / 100),
                marginTop: 20,
              }}
            >
              <TouchableOpacity
                style={[
                  {
                    width: width * (40 / 100),
                    padding: 10,
                    borderRadius: 10,
                    marginRight: 5,
                    backgroundColor: "#fff",
                    borderWidth: 1,
                    color: "#000",
                  },
                ]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={[homeStyles.textStyle, { color: "#000" }]}>
                  Huỷ
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  { width: width * (40 / 100), padding: 10, borderRadius: 10 },
                  homeStyles.buttonClose,
                ]}
                onPress={handleFilter}
              >
                <Text style={homeStyles.textStyle}>Lọc</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const homeStyles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: width * (95 / 100),
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  box: {
    backgroundColor: "white",
    width: width * (90 / 100),
  },
  row: {
    lineHeight: 40,
    height: 40,
    textAlign: "left",
    fontSize: 15,
    maxHeight: 40,
    marginTop: 10,
  },
  title: {
    marginLeft: 10,
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 5,
  },
});
