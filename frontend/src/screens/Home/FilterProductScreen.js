import React, { useEffect, useLayoutEffect, useState } from "react";
import { FlatList, Text, View, TouchableHighlight, Image } from "react-native";
import productStyle from "./ProductStyle";
import MenuImage from "../../components/MenuImage/MenuImage";
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";

import { ButtonGroup, Icon } from "@rneui/themed";
import numeral from "numeral";
import axios from "axios";
import Enviroment from "../../../env/Enviroment";
import { getProductByCategory } from "../../data/dataArrays";

export default function FilterProductScreen(props) {
  const { navigation, route } = props;
  const item = route?.params?.category;

  const [products, setProducts] = useState([])

  useEffect(() => {
    if (item?.id) {
      async function fetchData() {
        const response = await getProductByCategory(item);
        if (response.status === "OK") {
          setProducts(response.data);
        }
      }

      fetchData();
    }
  }, [])


  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <MenuImage
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ),
      headerRight: () => <View />,
    });
  }, []);

  const onPressProduct = (item) => {
    navigation.navigate("Product", { item });
  };

  const renderProducts = ({ item }) => (
    <TouchableHighlight underlayColor="rgba(84, 23, 137, 0.8)" onPress={() => onPressProduct(item)}>
      <View style={productStyle.container}>
        <Image style={productStyle.photo} source={{ uri: item.photoURL }} />
        <Text style={productStyle.name} numberOfLines={2} ellipsizeMode="tail">{item.name}</Text>
        <Text style={productStyle.price}>{numeral(item.price).format('0,0').replace(/,/g, '.')}₫</Text>
        <Text>
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <FontAwesomeIcon name="star" color="#fb6d2c" />
          <Text style={{ fontSize: 12 }}> 50</Text>
        </Text>
      </View>
    </TouchableHighlight>
  );

  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [state, setState] = useState(false);

  const sortData = (products) => {
    if (selectedIndex == -1) {
      return products;
    } else if (selectedIndex == 0) {
      if (state) {
        return products.sort((a, b) =>
          a.name == b.name ? 0 : a.name > b.name ? 1 : -1
        );
      } else {
        return products.sort((a, b) =>
          a.name == b.name ? 0 : a.name < b.name ? 1 : -1
        );
      }
    } else if (selectedIndex == 1) {
      if (state) {
        return products.sort((a, b) => {
          var a = parseInt(a.price, 10);
          var b = parseInt(b.price, 10);
          return a - b;
        });
      } else {
        return products.sort((a, b) => {
          var a = parseInt(a.price, 10);
          var b = parseInt(b.price, 10);
          return b - a;
        });
      }
    }
  };

  return (
    <View style={{ marginBottom: "20%" }}>
      <View
        style={{
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          flexDirection: "row",
        }}
      >
        <Text style={{ marginRight: 10 }}>Sắp Xếp:</Text>
        <Text>
          <ButtonGroup
            buttonStyle={{ padding: 10 }}
            selectedButtonStyle={{ backgroundColor: "#e2e2e2" }}
            selectedTextStyle={{ color: "#000" }}
            selectedIndex={selectedIndex}
            onPress={(value) => {
              if (value == selectedIndex) {
                setState(!state);
              } else {
                setState(false);
              }
              setSelectedIndex(value);
            }}
            buttons={[
              <Text>
                Theo Tên{"  "}
                {selectedIndex == 0 && !state && (
                  <FontAwesomeIcon name="arrow-up" color="#000" />
                )}
                {selectedIndex == 0 && state && (
                  <FontAwesomeIcon name="arrow-down" color="#000" />
                )}
              </Text>,
              <Text>
                Theo Giá{"  "}
                {selectedIndex == 1 && !state && (
                  <FontAwesomeIcon name="arrow-up" color="#000" />
                )}
                {selectedIndex == 1 && state && (
                  <FontAwesomeIcon name="arrow-down" color="#000" />
                )}
              </Text>,
            ]}
          />
        </Text>
      </View>
      <View>
        {products.length > 0 ? (
          <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            numColumns={2}
            data={sortData(products)}
            renderItem={renderProducts}
            keyExtractor={(item) => `${item.id}`}
          />
        ) : (
          <View style={styles.centeredContainer}>
            <Text>Không tìm thấy sản phẩm</Text>
          </View>
        )}
      </View>
    </View>
  );
}
