import { StyleSheet, Dimensions } from 'react-native';
// screen sizing
const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const recipeNumColums = 2;
// item size
const RECIPE_ITEM_HEIGHT = 150;
const RECIPE_ITEM_MARGIN = 20;
const ProductStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 20,
        width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
        height: RECIPE_ITEM_HEIGHT + 130,
        borderColor: '#cccccc',
        borderWidth: 0.5,
        borderRadius: 15,
        backgroundColor:'#ffffff',
        overflow: 'hidden',
    },
    photo: {
        width: 200,
        maxWidth: 200,
        height: RECIPE_ITEM_HEIGHT ,
        maxHeight: 150,
        minHeight: 150,
        resizeMode: "contain",
        borderRadius: 0,
        marginBottom:10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    name:{
        fontSize: 14,
        textAlign: 'center',
        marginBottom: 10,
    },
    price:{
        color: '#ff0000',
        fontSize: 16,
        marginBottom:10
    }
});

export default ProductStyle;