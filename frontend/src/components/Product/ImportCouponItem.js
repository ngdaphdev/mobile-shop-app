import {View, Text, StyleSheet, Image, Dimensions, TouchableHighlight} from "react-native";
const { width, height } = Dimensions.get('window');
import React from "react";
import { FontAwesome5 } from '@expo/vector-icons';
import numeral from "numeral";

export function ImportCouponItem({ item, index }){


    const date = new Date(item.dateTime);
    const formattedDateTime =
        `${date.getDate().toString().padStart(2, '0')}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getFullYear()} ` +
        `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'flex-start'}}>
                    <FontAwesome5 style={{paddingBottom: 25, paddingLeft: 15, paddingRight: 10}} name="money-bill-alt" size={24} color="black" />
                    <View style={styles.text}>
                        <Text style={styles.price}>{numeral(item.totalAmount).format('0,0').replace(/,/g, '.')}₫</Text>
                        <Text style={styles.id}>Mã phiếu: {item.id}</Text>
                        <Text style={styles.id}>Người lập: {item.importer}</Text>
                    </View>
                </View>
                <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 10}}>
                    <Text>{formattedDateTime}</Text>
                    <Text></Text>
                </View>
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    photo: {
        width: width*(15/100),
        height: width*(15/100),
        resizeMode: "contain",
        margin: 10
    },
    text:{
        margin: 10,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    price:{
        color: '#000',
        fontSize: 16,
        marginBottom: 5
    },
    id: {
        color: '#656565',
        fontSize: 13
    }
})