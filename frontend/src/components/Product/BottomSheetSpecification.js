import React, { useCallback, useMemo, useRef } from 'react';
import { StyleSheet,View,Text } from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';

const BottomSheetSpecification = () => {


    return (
        <View></View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        backgroundColor: 'red',
    },
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'blue'
    },
});

export default BottomSheetSpecification;
