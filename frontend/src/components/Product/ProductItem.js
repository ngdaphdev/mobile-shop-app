import {View, Text, StyleSheet, Image, Dimensions} from "react-native";
const { width, height } = Dimensions.get('window');
import React from "react";
import numeral from "numeral";
import productStyle from "../../screens/Home/ProductStyle";
import Enviroment from "../../../env/Enviroment";

export function ProductItem({ item, index }){
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                {item.photoURL.includes("http") ? (
                    <Image style={styles.photo} source={{ uri: item.photoURL }} />
                ) : (
                    <Image
                        style={styles.photo}
                        source={{
                            uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${item.photoURL}`,
                        }}
                    />
                )}
                <View style={styles.text}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>{item.name}</Text>
                    <Text style={styles.id}>Mã: {item.id}</Text>
                    <Text>{numeral(item.price).format('0,0').replace(/,/g, '.')}đ</Text>
                </View>
            </View>
            <View style={{display: 'flex',flexDirection: 'column',textAlign: 'center', alignItems: 'space-around', justifyContent: 'space-around', marginRight: 20}}>
                <Text></Text>
                <Text>Trong kho: {item.quantity}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width*(100/100),
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#a2a2a2',
        borderStyle: 'solid',
        justifyContent: 'space-between'
    },
    photo: {
        width: width*(15/100),
        height: width*(15/100),
        resizeMode: "contain",
        margin: 10
    },
    text:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    name:{
        color: '#000',
        fontSize: 15,
        width: width*(45/100)
    },
    id: {
        color: '#656565',
        fontSize: 13
    }
})