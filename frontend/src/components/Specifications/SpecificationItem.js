import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const SpecificationItem = ({ item, index }) => {
    const rowStyle = index % 2 === 0 ? styles.evenRow : styles.oddRow;

    return (
        <View style={[styles.specRow, rowStyle]}>
            <Text style={styles.specTitle}>{item.title}:</Text>
            <Text style={styles.specValue}>{item.value}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    specRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: '#CCCCCC',
    },
    specTitle: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#333333',
    },
    specValue: {
        fontSize: 16,
        color: '#666666',
    },
    evenRow: {
        backgroundColor: '#f5f5f5', // Màu xám
    },
    oddRow: {
        backgroundColor: '#FFFFFF', // Màu trắng
    },
});

export default SpecificationItem;