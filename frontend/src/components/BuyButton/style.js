import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:15,
        paddingBottom: 15,
        paddingLeft:40,
        paddingRight:40,
        borderRadius: 40,
        borderColor: '#2cd18a',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
        // backgroundColor: '#2cd18a'
    },
    text: {
        fontSize: 14,
        color: '#2cd18a'
    }
});

export default styles;
