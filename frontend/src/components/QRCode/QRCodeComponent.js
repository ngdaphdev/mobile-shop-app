import React from 'react';
import { StyleSheet, View } from 'react-native';
import QRCode from "react-qr-code";

const QRCodeComponent = ({ order }) => {
  const qrData = `${order.id}`;

   return (
    <View style={styles.container}>
      <QRCode value={qrData} size={200} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 20,
  },
});

export default QRCodeComponent;
