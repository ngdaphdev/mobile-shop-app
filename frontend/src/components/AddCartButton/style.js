import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:15,
        paddingBottom: 15,
        paddingLeft:40,
        paddingRight:40,
        borderRadius: 40,
        borderColor: '#d1ad2c',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
        // backgroundColor: '#2cd18a'
    },
    text: {
        fontSize: 14,
        color: '#d1ad2c'
    }
});

export default styles;
