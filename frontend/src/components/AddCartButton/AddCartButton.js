import React from 'react';
import { TouchableHighlight, Image, Text, View } from 'react-native';
import styles from './style';

export default function AddCartButton (props) {
    return (
        <TouchableHighlight style={{borderRadius:40, marginTop: 20}} underlayColor='rgba(73,182,77,0.9)' onPress={props.onPress}>
            <View style={styles.container}>
                <Text style={styles.text}>THÊM VÀO GIỎ HÀNG</Text>
            </View>
        </TouchableHighlight>
    );
}