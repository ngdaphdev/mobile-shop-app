import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Enviroment from '../../../env/Enviroment';
import styles from './styles';
import FontAwesomeIcon from "@expo/vector-icons/FontAwesome";

const Comment = ({ name, profilePicture, content, likes, timestamp, onLikePress, onReplyPress }) => {
  return (
    <View style={styles.container}>
      <View style={[styles.commentContainer, { flexDirection: 'row', alignItems:"center"}]}>
        <Image source={{ uri: `${Enviroment.URL_SERVER_API}/images/products/logo/${profilePicture}` }} style={[styles.profilePicture ,{flexBasis: '20%'}]} />
        <View style={{flexBasis: '80%', paddingRight: 10}}>
          <View style={styles.headerContainer}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.timestamp}>{timestamp}</Text>
          </View>
          <Text style={styles.content}>{content}</Text>
          <View style={styles.actionsContainer}>
            <TouchableOpacity onPress={onLikePress}>
              <Text style={styles.likeText}>{likes > 0 ? `${likes} Likes` : 'Like'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={onReplyPress}>
              <FontAwesomeIcon  style={{fontSize: 16, marginRight:3}} name="thumbs-up"/>
              <Text style={styles.replyText}>Hữu ích</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Comment;
