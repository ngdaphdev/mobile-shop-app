import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
  },
  profilePicture: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  commentContainer: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 10,
    padding: 10,
    backgroundColor: '#fff',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  timestamp: {
    color: '#555',
    fontSize: 12,
  },
  content: {
    fontSize: 14,
    marginBottom: 10,
  },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  likeText: {
    color: '#3498db',
    fontWeight: 'bold',
  },
  replyText: {
    color: '#555',
  },
});